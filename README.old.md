# Simi Project [WIP]

There are 3 crates in this repo:
* simi: The framework
* simi-html-tags: Html tags supported by `Simi`, for use internally by `simi` and `simi-macros` crates.
* simi-macros: Provide a custom attribute: `#[simi_app]`, and two macros: `application!` and `component!`.

> * simi-cli: (This crate has been move out of this repo, into [its own repo](https://gitlab.com/limira-rs/simi-cli)).
>     * This tool also compile (by using [rsass](https://github.com/kaj/rsass)) `style.scss` to `style.css` for you.
## Notable features of the framework

* No webpack, no nodejs
* When updating the `View`, `Simi` only check for what can be changed. You can also specify what to be `no update` at compile time.
* Component
* UbuComponent (update by user component)
* Sub apps
* Auto `style.scss` to `style.css` (helps by `simi-cli`, thanks to [rsass](https://github.com/kaj/rsass))

See `simi-project/simi/README.md` for information about the framework.

## Give a look at `Simi`'s examples!

You must [install Rust](https://www.rust-lang.org/en-US/) to use `Simi`.

Then:
```
# Simi requires nightly
rustup install nightly
rustup default nightly

# Because we build for the web
rustup target add wasm32-unknown-unknown

# `Simi` build on top of `wasm-bindgen`
cargo +nightly install wasm-bindgen-cli

# Install simi-cli from crates.io
cargo install simi-cli

# If you want to install simi-cli from git source: https://gitlab.com/limira-rs/simi-cli

# Now, clone simi-project source code and try its examples
git clone https://gitlab.com/limira-rs/simi-project.git
cd ../simi/examples/a-counter
simi serve
```
Now, open your browser and visit http://localhost:8000/!

## Contribution
### Help wanted

When making changes, please add them in `CHANGELOG.md`

#### TODO:
- [ ] Benchmarks
- [ ] Better API for Component?
- [ ] What is the best way to handle Error?
- [ ] release build vs debug build?: debug_assert?

### Test your changes
`simi-macros` crate does not have tests, it is tested indirectly in `simi` crate.
If there is no test that covers your changes, please add tests for them. Currently,
there are only tests that require a browser in `simi-project/simi/tests`.

To run these tests, make sure that:
* `simi-project/simi-cli` installed
* chromium browser installed
* [chromedriver](http://chromedriver.chromium.org/downloads) downloaded and unpacked to `"webdrivers/*"` where `webdrivers` is a folder at the same level of `simi-project`
```
parent-folder/
    |--- simi-project
            |--- simi 
            |--- ...
    |--- webdrivers
            |--- chromedriver
```
then in  `simi-project/`, run:
```
# run every tests
./ci_test_local.sh
```
or if you don't want to install simi-cli, then, in `simi-project/simi`
```
CHROMEDRIVER=path/to/chromedriver cargo +nightly test --target wasm32-unknown-unknown
```
### Test in docker
The docker test can build `simi-project/simi/examples`, but unable to run headless browser test for now (Admittedly, I don't know why! :blush: ).

### Output of the macros
Sometimes, you need to have a look at the generated code of `application!` or `component!`. You just simply add `@debug` to the
beginning of the macro content like this:
```rust
application!{
    @debug // <== to output generated code
    div () {
        ...
    }
}
```
then when the project is building, it will output the generated code to the terminal (if there is no error when generating it).
