FROM rustlang/rust:nightly

RUN apt-get update -yq
RUN apt-get install -y unzip chromium
RUN rustup target add wasm32-unknown-unknown
RUN cargo install wasm-bindgen-cli
RUN curl --retry 5 -LO https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip

ADD . ./simi
RUN chmod +x ./simi/test-in-docker.sh

CMD [ "./simi/test-in-docker.sh" ]