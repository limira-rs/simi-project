#![recursion_limit = "256"]
#![feature(proc_macro_hygiene)]
#![feature(proc_macro_diagnostic)]

extern crate proc_macro;
extern crate proc_macro2;
#[macro_use]
extern crate quote;
extern crate fnv;
extern crate heck;

extern crate simi_html_tags;

use proc_macro2::*;

mod helper;
mod render;

#[proc_macro_attribute]
pub fn simi_app(
    element_id: proc_macro::TokenStream,
    original_struct: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let element_id = helper::PushBackStream::new(element_id.into()).take_element_id();
    let struct_name =
        helper::PushBackStream::new(original_struct.clone().into()).take_struct_or_enum_name();

    let original_struct: TokenStream = original_struct.into();
    let start_in = element_id.map_or(quote!{self.in_body();}, |id| {
        quote!{self.in_element_id(#id);}
    });
    let rs = quote!{
        #original_struct

        #[wasm_bindgen]
        pub struct AppHandle {
            main: RcMain<#struct_name>,
        }

        impl SimiHandle<#struct_name> for AppHandle {
            fn main(&self) -> RcMain<#struct_name> {
                self.main.clone()
            }
        }

        #[wasm_bindgen]
        impl AppHandle {
            #[wasm_bindgen(constructor)]
            pub fn new() -> Self {
                Self {
                    main: SimiMain::new_rc_main(),
                }
            }

            pub fn start(&mut self) {
                #start_in
            }
        }
    };
    //println!("{}", rs);
    rs.into()
}

#[proc_macro]
pub fn application(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    render_with_option(input, render::Options::default())
}

#[proc_macro]
pub fn component(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    render_with_option(input, render::Options::for_component())
}

fn render_with_option(
    input: proc_macro::TokenStream,
    mut options: render::Options,
) -> proc_macro::TokenStream {
    use helper::PushBackStream;
    use render::{raw_dom, Config, CurrentParents};

    if input.is_empty() {
        panic!("empty content is not allowed");
    }
    let mut input = PushBackStream::new(input.into());
    input.get_options(&mut options);
    let config = Config::new(options, quote!{context}, "props");
    let mut raw_list = raw_dom::NodeList::parse(&config, input);
    config.panic_on_error();
    raw_list.inspect_no_update();

    let mut closures = Vec::new();
    raw_list.generate_closures(&config, &mut closures);

    // The binding to a simi::simi_dom::NodeList use to store generated simi's objects
    let root_list = quote!{root_list_from_the_context_};
    let real_parent = quote!{context.real_element};
    let parents = CurrentParents::new_root(root_list.clone(), real_parent.clone());
    let node_count = raw_list.nodes.len();
    let inner = raw_list.render(&config, &parents);
    let log = &config.log_error;
    let result = quote!{{
        use simi;
        use simi::simi_dom::SimiCreator;
        use wasm_bindgen::JsCast;
        #(#closures)*
        let #root_list = context.take_root_list();
        #root_list.new_capacity(#node_count);
        #inner
        if #root_list.count() != #node_count {
            #log("Simi bug: number of nodes at the root is not as expected.");
        }
    }};
    if config.options.is_debug {
        println!("{}", result);
    }
    result.into()
}

fn get_span(token: &TokenTree) -> Span {
    match token {
        TokenTree::Group(group) => group.span(),
        TokenTree::Ident(iden) => iden.span(),
        TokenTree::Literal(lit) => lit.span(),
        TokenTree::Punct(p) => p.span(),
    }
}

struct Error {
    error_span: Option<Span>,
    error_message: String,
    help_span: Option<Span>,
    help_message: Option<String>,
}

impl Error {
    fn with_span(span: Span, message: &str) -> Self {
        Self {
            error_span: Some(span),
            error_message: message.to_string(),
            help_span: None,
            help_message: None,
        }
    }

    fn with_token(token: &TokenTree, message: &str) -> Self {
        Self {
            error_span: Some(get_span(&token)),
            error_message: message.to_string(),
            help_span: None,
            help_message: None,
        }
    }

    fn help_at_span(mut self, span: Span, message: &str) -> Self {
        self.help_span = Some(span);
        self.help_message = Some(message.to_string());
        self
    }

    fn help_at_token(mut self, token: &TokenTree, message: &str) -> Self {
        self.help_span = Some(get_span(token));
        self.help_message = Some(message.to_string());
        self
    }

    fn emit(self, config: Option<&render::Config>) {
        let Error {
            error_span,
            error_message,
            help_span,
            help_message,
        } = self;
        let span = match error_span {
            Some(span) => span,
            None => {
                panic!("`Simi` bug: It tries to emit an error with no associated span or token");
            }
        };
        span.unstable().error(error_message).emit();
        if let Some(help) = help_message {
            let span = help_span.unwrap_or(span);
            span.unstable().help(help).emit();
        }
        if let Some(config) = config {
            config.set_error();
        }
    }

    fn warn(self) {
        let Error {
            error_span,
            error_message,
            help_span,
            help_message,
        } = self;
        let span = match error_span {
            Some(span) => span,
            None => {
                panic!("`Simi` bug: It tries to emit an error with no associated span or token");
            }
        };
        span.unstable().warning(error_message).emit();
        if let Some(help) = help_message {
            let span = help_span.unwrap_or(span);
            span.unstable().help(help).emit();
        }
    }

    fn panic(self) -> ! {
        self.emit(None);
        panic!("Macro stop because of previous error");
    }
}
