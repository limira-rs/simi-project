use proc_macro2::token_stream::IntoIter;
use proc_macro2::*;
use render::{NameMode, Options};
use std::iter::FromIterator;
use Error;

pub mod build_stream {
    use proc_macro2::*;
    pub fn from_streams(streams: Vec<TokenStream>) -> TokenStream {
        let mut rs = TokenStream::new();
        rs.extend(streams);
        rs
    }
}

/// This function will check if it is a simple exprssion or not
/// These are NOT allowed: `{...}`
pub fn is_valid_simi_expression(tokens: &[TokenTree]) {
    tokens.iter().for_each(|token| match token {
        TokenTree::Group(group) if group.delimiter() == Delimiter::Brace => Error::with_span(
            group.span(),
            "Do not put complex code into the macro content",
        )
        .panic(),
        _ => {}
    });
}

pub fn to_first_uppercase(tag: &str) -> String {
    let (first, remaining) = tag.split_at(1);
    let mut first = first.to_uppercase();
    first.push_str(remaining);
    first
}

// Currently, this just check that the first is UpperCase, and if there is a lowercase or not
pub fn is_camel_case(name: &str) -> bool {
    let mut name = name.chars();
    if !name.next().unwrap().is_uppercase() {
        return false;
    }
    name.any(|c| c.is_lowercase())
}

//  * "literal string" is presented as it is
//  * others will be converted `to_string` and an `&` will be prepend if `reference`
pub fn get_str_html_value_attribute(value: &TokenStream, reference: bool) -> TokenStream {
    let literal_string = value.to_string().starts_with('\"');
    if literal_string {
        quote!{#value}
    } else if reference {
        quote!{&#value.to_string()}
    } else {
        quote!{#value.to_string()}
    }
}

pub struct PunctedPortion {
    pub punct_found: bool,
    pub tokens: Vec<TokenTree>,
}

impl PunctedPortion {
    fn new() -> Self {
        Self {
            punct_found: false,
            tokens: Vec::new(),
        }
    }
}
pub struct PushBackStream {
    input_iter: IntoIter,
    pending_tokens: Vec<TokenTree>,
    last_discarded_token: Option<TokenTree>,
}

impl PushBackStream {
    pub fn new(input: TokenStream) -> Self {
        Self {
            input_iter: input.into_iter(),
            pending_tokens: Vec::new(),
            last_discarded_token: None,
        }
    }

    pub fn last_discarded_token(&self) -> &TokenTree {
        self.last_discarded_token
            .as_ref()
            .expect("Simi bug: Unwrap empty `last_discarded_token`")
    }

    pub fn next(&mut self) -> Option<TokenTree> {
        match self.pending_tokens.pop() {
            Some(token) => Some(token),
            None => self.input_iter.next(),
        }
    }

    pub fn push_back(&mut self, token: TokenTree) {
        self.pending_tokens.push(token);
    }

    fn push_some_back(&mut self, token: Option<TokenTree>) {
        if let Some(token) = token {
            self.push_back(token);
        }
    }

    // at = @
    pub fn get_at(&mut self) -> Option<TokenTree> {
        let token = self.next();
        let yes = match token {
            Some(TokenTree::Punct(ref p)) if p.as_char() == '@' => true,
            _ => false,
        };
        if yes {
            token
        } else {
            self.push_some_back(token);
            None
        }
    }

    pub fn get_options(&mut self, options: &mut Options) {
        const SUPPORTS: &str = "Supported parameter/option: `@debug`, `@app_type=SomeType`";
        const WANTED_APP_TYPE :&str = "Expect something like: `@app_type=YourAppType` or `@app_type=A` (where A is the generic type name)";
        while let Some(at) = self.get_at() {
            match self.next() {
                Some(TokenTree::Ident(ident)) => match ident.to_string().as_str() {
                    "debug" => options.is_debug = true,
                    "app_type" => {
                        if let Some(span) = self.next_is_single_punct('=') {
                            match self.next() {
                                Some(TokenTree::Ident(ident)) => options.app_type = Some(ident),
                                Some(token) => Error::with_token(&token, WANTED_APP_TYPE).panic(),
                                None => Error::with_span(span, WANTED_APP_TYPE).panic(),
                            }
                        } else {
                            Error::with_span(ident.span(), WANTED_APP_TYPE).panic()
                        }
                    }
                    _ => Error::with_span(ident.span(), &format!("Unknown option. {}", SUPPORTS))
                        .panic(),
                },
                Some(other) => {
                    Error::with_token(&other, &format!("Unexpected token. {}", SUPPORTS)).panic()
                }
                None => Error::with_token(
                    &at,
                    &format!("Expected an identifier after this. {}", SUPPORTS),
                )
                .panic(),
            }
        }
    }

    pub fn has_no_update(&mut self) -> bool {
        let token = self.next();
        let dont_update = match token {
            Some(TokenTree::Punct(ref p)) if p.as_char() == ::render::NO_UPDATE_PREFIX => true,
            _ => false,
        };
        if !dont_update {
            self.push_some_back(token);
        }
        dont_update
    }

    pub fn next_is_single_punct(&mut self, expected_punct: char) -> Option<Span> {
        let token = self.next();
        let rs = match token {
            Some(TokenTree::Punct(ref punct)) => {
                if punct.as_char() == expected_punct && punct.spacing() == Spacing::Alone {
                    Some(punct.span())
                } else {
                    None
                }
            }
            _ => None,
        };
        if rs.is_none() {
            self.push_some_back(token);
        }
        rs
    }

    pub fn next_is_dot_or_double_colon(&mut self) -> bool {
        let token = self.next();
        let rs = match token {
            None => false,
            Some(TokenTree::Punct(ref punct)) => match punct.as_char() {
                '.' => true,
                ':' if punct.spacing() == Spacing::Joint => true,
                _ => false,
            },
            _ => false,
        };
        self.push_some_back(token);
        rs
    }

    pub fn take_until_single_punct(&mut self, punct: char) -> Option<PunctedPortion> {
        let mut last_punct: Option<char> = None;
        let mut pp = PunctedPortion::new();
        while let Some(token) = self.next() {
            let mut join_punct = false;
            if let TokenTree::Punct(ref p) = token {
                let c = p.as_char();
                if c == punct {
                    if let Some(last) = last_punct {
                        if last == '?' {
                            pp.punct_found = true;
                        }
                    }
                    if p.spacing() == Spacing::Joint {
                        join_punct = true;
                    } else {
                        pp.punct_found = true;
                    }
                } else {
                    last_punct = Some(c);
                }
            } else {
                last_punct = None;
            }
            if pp.punct_found {
                break;
            }
            if join_punct {
                let next = match self.next() {
                    Some(next) => next,
                    None => Error::with_token(
                        &token,
                        &format!("`Simi` bug: Expect a second `{}` next to this", punct),
                    )
                    .panic(),
                };
                if next.to_string() != punct.to_string() {
                    self.push_back(next);
                    pp.punct_found = true;
                    break;
                }
                pp.tokens.push(token);
                pp.tokens.push(next);
            } else {
                pp.tokens.push(token);
            }
        }
        if pp.tokens.is_empty() {
            None
        } else {
            Some(pp)
        }
    }

    /// Return values: (no_update, value, next_name)
    pub fn take_value_and_next_name(
        &mut self,
        config: &::render::Config,
        punct: char,
        mode: &NameMode,
    ) -> Option<(bool, Vec<TokenTree>, Vec<TokenTree>)> {
        fn check_valid_tokens(
            tokens: &[TokenTree],
            error_token: &TokenTree,
            error_message: &'static str,
        ) {
            if tokens.is_empty() {
                Error::with_token(error_token, error_message).panic();
            }
        }

        // Take all up to the next single_punct=punct
        let PunctedPortion {
            punct_found,
            mut tokens,
        } = match self.take_until_single_punct(punct) {
            Some(rs) => rs,
            None => return None,
        };
        // Check if the first token is an `#`?
        let no_update = {
            match tokens.first() {
                Some(TokenTree::Punct(p)) if p.as_char() == ::render::NO_UPDATE_PREFIX => true,
                _ => false,
            }
        };
        // If it is an `#` then, remove it
        if no_update {
            let punct_no_update = tokens.remove(0);
            check_valid_tokens(
                &tokens,
                &punct_no_update,
                "Expect a value/expression after this",
            );
        }
        if !punct_found {
            // This is the last expression value of the last attribute
            return Some((no_update, tokens, Vec::new()));
        }
        // tokens is ensure that it contains at least 1 token

        match mode {
            NameMode::RustIdentifer => {
                // A component id must be a single ident
                let last = tokens.pop().unwrap();
                check_valid_tokens(
                    &tokens,
                    &last,
                    "Expect a value/expression before this, or, an identifier after this",
                );
                Some((no_update, tokens, vec![last]))
            }
            NameMode::HtmlElementAttributeName => {
                // Take the last token
                // Treat it as an attribute name
                let last = tokens.pop().unwrap();
                let mut next_name = vec![last];
                let mut question_mark = false;
                let done = match next_name[0] {
                    TokenTree::Literal(ref _l) => true, // TODO: What if users input a `12345=is_something_true()`???
                    TokenTree::Punct(ref p) if p.as_char() == '?' => {
                        question_mark = true;
                        false
                    }
                    _ => false,
                };
                if done {
                    // It is a literal, it will be the conditional class name
                    check_valid_tokens(
                        &tokens,
                        next_name.first().unwrap(),
                        "Expect a value/expression before this, or, an identifier after this",
                    );
                    return Some((no_update, tokens, next_name));
                }

                // It is not a literal, we need to check if it is a custom attribute:
                //      data-my-custom-attr (attribute name form by serie of identifiers join by hyphens)
                // or if it is a conditional class name by `self.class_name?`

                let expected_punct = if question_mark {
                    match tokens.pop() {
                        Some(token) => next_name.push(token),
                        None => Error::with_token(next_name.first().unwrap(), "Expect an expression (will be used as a conditional class) before this").emit(Some(config))
                    }
                    '.'
                } else {
                    '-'
                };

                while let Some(token) = tokens.pop() {
                    let more = match token {
                        TokenTree::Punct(ref p) if p.as_char() == expected_punct => true,
                        _ => false,
                    };
                    if !more {
                        // The token is not belong to next_name.
                        // It belongs to the value of the last name. Push it back.
                        tokens.push(token);
                        break;
                    }
                    // There is more ident

                    let prev_ident = if let Some(token) = tokens.pop() {
                        match token {
                            TokenTree::Ident(ref _ident) => {}
                            _ => Error::with_token(
                                &token,
                                "Expect an identifier here instead of this",
                            )
                            .panic(),
                        }
                        token
                    } else {
                        Error::with_token(&token, "Expect an identifier before this").panic()
                    };
                    next_name.push(token);
                    next_name.push(prev_ident);
                }
                next_name.reverse();
                check_valid_tokens(
                    &tokens,
                    next_name.first().unwrap(),
                    "Expect a value/expression before this, or, an identifier after this",
                );
                Some((no_update, tokens, next_name))
            }
        }
    }

    pub fn take_group(&mut self, delimiter: Delimiter) -> Option<Group> {
        let may_be_pending_token = self.next();
        if let Some(token) = may_be_pending_token.clone() {
            if let TokenTree::Group(group) = token {
                if group.delimiter() == delimiter {
                    return Some(group);
                }
            }
            self.push_some_back(may_be_pending_token);
        }
        None
    }

    pub fn take_expression(&mut self, first_token: TokenTree) -> TokenStream {
        let mut expect_ident = false;
        let mut vec = vec![first_token];
        while let Some(token) = self.next() {
            match token {
                TokenTree::Ident(ref _p) => {
                    if expect_ident {
                        expect_ident = false;
                    } else {
                        // End of expression. Current token is the next content
                        self.push_back(token.clone());
                        break;
                    }
                }
                TokenTree::Group(ref g) => {
                    if g.delimiter() == Delimiter::Brace {
                        Error::with_span(g.span(), "Unexpect code block here. Do not put complex expression into macro content. You should move it outside.")
                            .help_at_token(vec.first().unwrap(), "Maybe this is a misspelled html tag?")
                            .panic()
                    }
                    expect_ident = false;
                }
                TokenTree::Literal(ref _l) => {
                    // I can not think of a syntax where a literal appear indepently in an expression
                    // So, I disallow it now.
                    self.push_back(token.clone());
                    break;
                }
                _ => expect_ident = true,
            }
            vec.push(token);
        }
        TokenStream::from_iter(vec)
    }

    pub fn take_self_field(&mut self) -> TokenStream {
        let mut vec = Vec::new();
        while let Some(token) = self.next() {
            let ok = match token {
                TokenTree::Ident(_) if vec.len() % 2 == 0 => true,
                TokenTree::Punct(ref p) if p.as_char() != '.' => false,
                TokenTree::Punct(_) if vec.len() % 2 == 1 => true,
                _ => false,
            };
            if ok {
                vec.push(token);
            } else {
                self.push_back(token);
                break;
            }
        }
        // TODO: make sure that the first is `self`
        TokenStream::from_iter(vec)
    }

    pub fn take_until_punct(&mut self, punct: char) -> Option<Vec<TokenTree>> {
        let mut vec = Vec::new();
        while let Some(token) = self.next() {
            match token {
                TokenTree::Punct(ref p) if p.as_char() == punct => break,
                _ => {}
            }
            vec.push(token);
        }
        if vec.is_empty() {
            None
        } else {
            Some(vec)
        }
    }

    pub fn take_until_equal_gt(&mut self) -> Option<Vec<TokenTree>> {
        self.last_discarded_token = None;
        let mut vec = Vec::new();
        while let Some(token) = self.next() {
            match token {
                TokenTree::Punct(ref p) if p.as_char() == '=' && p.spacing() == Spacing::Joint => {
                    match self.next() {
                        Some(token) => match token {
                            TokenTree::Punct(ref p2) if p2.as_char() == '>' => {
                                self.last_discarded_token = Some(token.clone());
                                break;
                            }
                            token => self.push_back(token),
                        },
                        None => Error::with_span(p.span(), "Unexpected end of input").panic(),
                    }
                }
                _ => {}
            }
            vec.push(token);
        }
        if vec.is_empty() {
            if let Some(ref gt) = self.last_discarded_token {
                Error::with_token(gt, "Expect a pattern before this `=>`").panic()
            } else {
                None
            }
        } else {
            Some(vec)
        }
    }

    pub fn take_expression_and_brace_group(&mut self) -> (Vec<TokenTree>, Option<Group>) {
        let mut expr = Vec::new();
        while let Some(token) = self.next() {
            match token {
                TokenTree::Group(ref g) if g.delimiter() == Delimiter::Brace => {
                    return (expr, Some(g.clone()));
                }
                _ => {}
            }
            expr.push(token);
        }
        (expr, None)
    }

    pub fn take_element_id(&mut self) -> Option<TokenStream> {
        const WANTED: &str = "Expected one of: #[simi_app] or #[simi_app(element-id)] or #[simi_app(\"element-id\")]";
        match self.next() {
            None => None,
            Some(token) => {
                match token {
                    TokenTree::Literal(ref lit) => {
                        if let Some(token) = self.next() {
                            Error::with_token(&token, WANTED).panic();
                        }
                        return Some(quote!{#lit});
                    }
                    TokenTree::Ident(_) => {}
                    _ => Error::with_token(&token, WANTED).panic(),
                }
                let mut id = String::new();
                id.push_str(&token.to_string());
                while let Some(token) = self.next() {
                    id.push_str(&token.to_string());
                }
                let id = Literal::string(&id);
                Some(quote!{#id})
            }
        }
    }

    pub fn take_struct_or_enum_name(&mut self) -> TokenStream {
        const MESSAGE: &str =
            "#[simi_app] expects to be applied to a struct/enum data state of a SimiApp";
        match self.next() {
            None => panic!(MESSAGE),
            Some(TokenTree::Ident(ref ident)) if ident == "pub" => self.take_struct_or_enum_name(),
            Some(TokenTree::Ident(ref ident)) if ident == "struct" || ident == "enum" => {
                match self.next() {
                    None => panic!(MESSAGE),
                    Some(token) => return quote!{#token},
                }
            }
            Some(TokenTree::Punct(ref p)) if p.as_char() == '#' => {
                self.take_group(Delimiter::Bracket);
                self.take_struct_or_enum_name()
            }
            Some(token) => Error::with_token(&token, MESSAGE).panic(),
        }
    }

    pub fn take_input_from_brace(&mut self, brace_for_ident: &Ident) -> PushBackStream {
        let g = match self.take_group(Delimiter::Brace) {
            Some(g) => g,
            None => Error::with_span(brace_for_ident.span(), "Expect {...} for this").panic(),
        };
        PushBackStream::new(g.stream())
    }

    pub fn take_all_to_end(&mut self) -> TokenStream {
        let mut all = Vec::new();
        while let Some(token) = self.next() {
            all.push(token);
        }
        TokenStream::from_iter(all)
    }

    pub fn expect_keyword_colon(
        &mut self,
        keyword: &str,
        parent_ident: &Ident,
        message: &str,
    ) -> Ident {
        match self.next() {
            Some(TokenTree::Ident(ref ident)) if ident == keyword => {
                if self.next_is_single_punct(':').is_none() {
                    Error::with_span(ident.span(), message).panic()
                }
                ident.clone()
            }
            Some(token) => Error::with_token(&token, message).panic(),
            None => Error::with_span(
                parent_ident.span(),
                &format!("Unexpect end of content. {}", message),
            )
            .panic(),
        }
    }

    pub fn expect_ident(&mut self, preceded_span: Span, message: &str) -> Ident {
        match self.next() {
            Some(TokenTree::Ident(ident)) => ident,
            Some(token) => {
                Error::with_token(&token, &format!("{} instead of this", message)).panic()
            }
            None => Error::with_span(
                preceded_span,
                &format!("Unexpect end of content. {}", message),
            )
            .panic(),
        }
    }
}
