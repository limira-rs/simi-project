use super::raw_dom::*;
use super::{Config, CurrentParents};
use helper::{self, build_stream};
use proc_macro2::*;
use std::iter::FromIterator;

impl NodeList {
    pub(crate) fn create(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let vec: Vec<TokenStream> = self
            .nodes
            .iter()
            .map(|node| node.create(config, parents))
            .collect();
        helper::build_stream::from_streams(vec)
    }
}

impl Node {
    pub fn create(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        match self {
            Node::Element(e) => e.create(config, parents),
            Node::Component(s) => s.create(config, parents),
            Node::ChildPlaceholder(sc) => {
                // A component child is an instance of ::simi::app::ComponentChild
                let (shadowing, effective_list, effective_real) = parents.rebinding();
                let comp_child = quote!{comp_child_local_name_};
                let log = &config.log_error;
                quote!{
                    {
                        #shadowing
                        let #comp_child = #sc.replace(None).expect("`Simi` bug: A component child is expected in create phase");
                        match #comp_child.upgrade(){
                            Some(component) => {
                                let mut component = component.borrow_mut();
                                component.set_real_parent(#effective_real);
                                component.add_place_holder();
                            },
                            None => #log("Unable upgrade a component child from Weak to Rc"),
                        }
                        // Add the weak link to the simi node list
                        #effective_list.append_child(#comp_child.into());
                    }
                }
            }
            Node::Expression(x) => {
                // This is from an expression and will be render as a text node.
                // `application!/component!` is not designed to use outside of an app's render method or
                // a component's render method.
                if config.need_unwrap(&x.expression) {
                    // We are inside a component, and this expression is not a props
                    // The expression is a component's struct field
                    let expression = &x.expression;
                    let create_text_node = parents.create_text_node(config, quote!{text});
                    quote!{
                        if let Some(ref text) = #expression {
                            #create_text_node
                        }
                    }
                } else {
                    let x = &x.expression;
                    parents.create_text_node(config, quote!{ &#x })
                }
            }
            Node::Literal(literal) => {
                // Literals will always be a text node.
                parents.create_text_node(config, literal)
            }
            Node::IfElse(ifelse) => ifelse.create_by_single_closure(config, parents),
            Node::Match(m) => m.create_by_single_closure(config, parents),
            Node::For(f) => f.create(config, parents),
        }
    }
}

impl Element {
    fn create(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let tag = self.tag.to_string();
        let first_uppercase_tag = Ident::new(&helper::to_first_uppercase(&tag), Span::call_site());
        let html_tag_path = config.html_tag_path.clone();

        let simi_dom = &config.simi_dom_path;
        let new_simi = quote!{new_simi_element_for_an_html_element_};
        let new_real = quote!{new_real_element_for_a_simi_element_};
        let new_simi_nodes = quote!{
            #new_simi.nodes_mut()
        };

        // Events is only process in create-phase
        let attach_events = self.attach_events(config, &new_simi);
        // Set attributes for the real element
        // We do not store any of these attribute in simi_dom
        let const_attributes = self
            .const_attributes
            .set_const_attributes(config, &tag, &new_real);
        let post_const_attributes = self
            .const_attributes
            .post_set_const_attributes(config, &tag, &new_real);

        // These attribute need to be tracted in simi_dom
        // And add them into simi element
        let expr_attributes = self.expr_attributes.create(config, &tag, &new_simi);

        // Classes that never changes
        let classes = self.attach_permanent_classes(config, &new_real);
        // Classes that on/of depend on a condition
        let conditional_classes = self.create_conditional_classes(config, &new_simi);

        // Create childs for this element
        // Of course, this element is their parent.
        let sub_parents = CurrentParents::new_sub(new_simi_nodes.clone(), new_real.clone());
        let childs = self.content.create(config, &sub_parents);

        let (shadowing, effective_list, effective_real) = parents.rebinding();
        let append_or_insert = if let Some(before) = parents.insert_before() {
            quote!{ #new_simi.insert_to(&#effective_real, #before); }
        } else if config.need_insert(parents) {
            let context = &config.context;
            quote!{ #new_simi.insert_to(&#effective_real, #context.insert_before_node()); }
        } else {
            quote!{ #new_simi.append_to(&#effective_real); }
        };

        let more = match tag.as_str() {
            "select" => quote!{#simi_dom::Select},
            "input" => quote!{#simi_dom::Input},
            "textarea" => quote!{#simi_dom::TextArea},
            _ => quote!{#simi_dom::NoSpecial},
        };
        quote!{
            {
                // Use via a new binding, to avoid shadowing by the same name.
                #shadowing
                //#[allow(unused_mut)]
                let mut #new_simi = #effective_list.create_element::<#more>(#tag, #html_tag_path::#first_uppercase_tag);
                //#[allow(unused_variables)]
                let #new_real = #new_simi.get_real_element().clone(); // How to avoid cloning?
                let #new_real = &#new_real; // How to avoid cloning?
                #attach_events
                #const_attributes
                #expr_attributes
                #classes
                #conditional_classes
                #append_or_insert
                #childs
                // select.index/value will be setted here,
                // after adding its childs (option elements)
                #post_const_attributes
                #new_simi.set_attributes_to_real_element();
                #effective_list.append_child(#new_simi.into());

            }
        }
    }

    fn attach_events(&self, config: &Config, new_simi: &TokenStream) -> TokenStream {
        if self.events.is_empty() {
            return TokenStream::new();
        }
        let events = quote!{events};
        let all_events = self
            .events
            .iter()
            .map(|e| {
                match e.handler_type {
                    EventHandlerType::MessageVariant | EventHandlerType::ContextMethod => {
                        let event = e.create_element_event(config);
                        quote!{ #events.push(Box::new(#event));}
                    }
                    EventHandlerType::ComponentField => {
                        // We are in a component, and the event is passed here from the caller
                        let handler = TokenStream::from_iter(e.handler.clone());
                        quote! { #events.push(#handler.take().expect("`Simi` bug: Event handler is None. Expect: it must be available in create phase")); }
                    }
                }
            })
            .collect();
        let all_events = build_stream::from_streams(all_events);
        let main = config.main_clone();
        quote!{
            {
                use simi::element_events::Events;
                let mut #events = Events::new();
                #all_events
                #new_simi.add_real_events(&#main, #events);
            }
        }
    }

    fn attach_permanent_classes(&self, config: &Config, new_real: &TokenStream) -> TokenStream {
        if self.permanent_classes.is_empty() {
            return TokenStream::new();
        }
        // Permanent classes are added directly to the real element in real dom
        let class_list = quote!{class_list};
        let all = self
            .permanent_classes
            .iter()
            .map(|item| match item {
                LiteralOrTokenStream::Stream(ref class_name) => {
                    if config.need_unwrap(class_name) {
                        quote!{
                            if let Some(class) = #class_name {
                                simi::interop::display_error(#class_list.add_1(&class.to_string()));
                            }
                        }
                    } else {
                        quote!{
                            simi::interop::display_error(#class_list.add_1(&#class_name.to_string()));
                        }
                    }
                }
                LiteralOrTokenStream::Literal(ref literal) => {
                    let classes = literal
                        .to_string()
                        .trim_matches('"')
                        .split_whitespace()
                        .map(|class| {
                            quote!{ simi::interop::display_error(#class_list.add_1(#class)); }
                        }).collect();
                    build_stream::from_streams(classes)
                }
            }).collect();
        let all = build_stream::from_streams(all);
        quote!{{
            let #class_list = #new_real.class_list();
            #all
        }}
    }

    fn create_conditional_classes(&self, config: &Config, new_simi: &TokenStream) -> TokenStream {
        if self.conditional_classes.is_empty() {
            return TokenStream::new();
        }
        // Conditional classes will be tracked by virtual dom
        let classes = quote!{classes};
        let expr_classes = quote!{expr_classes};
        let all = self
            .conditional_classes
            .iter()
            .map(|cc| {
                let ConditionalClass { name, value } = cc;
                match name {
                    LiteralOrTokenStream::Literal(lit) => {
                        if config.need_unwrap(value) {
                            quote!{
                                if let Some(ref value) = #value {
                                    #classes.insert(#lit, *value);
                                }
                            }
                        } else {
                            quote!{ #classes.insert(#lit, #value); }
                        }
                    }
                    LiteralOrTokenStream::Stream(s) => {
                        // We don't know the class name at compile time
                        match (config.need_unwrap(s), config.need_unwrap(value)) {
                            (true, true) => quote!{
                                if let (Some(ref class_name), Some(ref value)) = (#s, #value) {
                                    #expr_classes.insert(class_name.to_string(), *value);
                                }
                            },
                            (true, false) => quote!{
                                if let Some(ref class_name) = #s{
                                    #expr_classes.insert(class_name.to_string(), #value);
                                }
                            },
                            (false, true) => quote!{
                                if let Some(ref value)=#value{
                                    #expr_classes.insert(#s.to_string(), *value);
                                }
                            },
                            (false, false) => {
                                quote!{ #expr_classes.insert(#s.to_string(), #value); }
                            }
                        }
                    }
                }
            })
            .collect();
        let all = build_stream::from_streams(all);
        quote!{{
            let (#classes, #expr_classes) = #new_simi.classes_expr_classes_mut();
            #all
        }}
    }

    fn create_as_component(&self, parent_comp: &TokenStream) -> TokenStream {
        let new_comp = quote!{a_new_simi_comp_created_from_element_};
        let local_parent_comp = quote!{a_local_reference_to_the_parent_comp_};
        quote! {
            {
                let #local_parent_comp = &mut #parent_comp;
                let #new_comp = #local_parent_comp.create_component(1);
                #local_parent_comp.add_child_component(#new_comp);
            }
        }
    }

    fn render_as_component(
        &self,
        config: &Config,
        parent_comp: &TokenStream,
        index: usize,
    ) -> TokenStream {
        let log = &config.log_error;
        let new_comp = quote!{a_new_simi_comp_created_from_element_};
        let new_comp_nodes = quote!{mutable_reference_to_new_comp_nodes_};
        let real_parent = quote!{a_real_element_that_is_setted_to_new_comp_at_child_place_holder_};

        let sub_parents = CurrentParents::new_sub(new_comp_nodes.clone(), real_parent.clone());
        let render = self.create(config, &sub_parents);
        quote! {
            {
                match #parent_comp.childs().get(#index){
                    Some(#new_comp) => match #new_comp.try_borrow_mut() {
                        Ok(mut #new_comp) => {
                            let (#new_comp_nodes, #real_parent) = #new_comp.nodes_mut_real_parent();
                            #render
                        }
                        Err(e) => #log(&e.to_string())
                    }
                    None => #log("Simi bug: Element as component child: Must have a child")
                }

            }
        }
    }
}

impl AttributeGroup {
    // Attributes that need to track in virtual dom
    fn create(&self, config: &Config, html_tag: &str, new_simi: &TokenStream) -> TokenStream {
        // Regular attributes
        let regulars = self
            .regulars
            .iter()
            .map(|a| a.create(config, html_tag, new_simi))
            .collect();
        // Custom attributes
        let customs = self
            .customs
            .iter()
            .map(|a| a.create(config, new_simi))
            .collect();
        let regulars = build_stream::from_streams(regulars);
        let customs = build_stream::from_streams(customs);
        quote!{
            #regulars
            #customs
        }
    }

    // Const attributes will be added directly to real element in real dom
    fn set_const_attributes(
        &self,
        config: &Config,
        html_tag: &str,
        new_real: &TokenStream,
    ) -> TokenStream {
        // Regular attributes
        let regulars = self
            .regulars
            .iter()
            .map(|a| a.set_const_attributes(config, html_tag, new_real))
            .collect();
        // Custom attributes
        let customs = self
            .customs
            .iter()
            .map(|a| a.set_const_attributes(config, new_real))
            .collect();
        let regulars = build_stream::from_streams(regulars);
        let customs = build_stream::from_streams(customs);
        quote!{
            #regulars
            #customs
        }
    }

    // 'value' and 'index' of a 'select' depend on its child-elements (option element)
    // so, we must set these after adding its childs
    fn post_set_const_attributes(
        &self,
        config: &Config,
        html_tag: &str,
        new_real: &TokenStream,
    ) -> TokenStream {
        // post-setting is not required for custom attributes
        // Only for regulars attributes
        let regulars = self
            .regulars
            .iter()
            .map(|a| a.post_set_const_attributes(config, html_tag, new_real))
            .collect();
        build_stream::from_streams(regulars)
    }
}

impl RegularAttribute {
    fn set_const_attributes(
        &self,
        config: &Config,
        html_tag: &str,
        new_real: &TokenStream,
    ) -> TokenStream {
        let real_dom = &config.real_dom_path;
        let name = self.name.to_string();
        let value = &self.value;
        match name.as_str() {
            "value" => self.set_const_value(config, html_tag, new_real),
            // index for select will be set after creating its childs
            "index" => TokenStream::new(),
            "checked" => {
                // Checked is for 'input' only (at least for now)
                // So we just use 'set_checked'
                if config.need_unwrap(value) {
                    quote!{
                        if let Some(ref value) = #value {
                            let converted_: &#real_dom::HtmlInputElement = #new_real.unchecked_ref();
                            converted_.set_checked(*value);
                        }
                    }
                } else {
                    quote!{{
                        let converted_: &#real_dom::HtmlInputElement = #new_real.unchecked_ref();
                        converted_.set_checked(#value);
                    }}
                }
            }
            // Other attributes
            _ => {
                if config.need_unwrap(value) {
                    quote!{
                        if let Some(ref value) = #value {
                            #new_real.set_attribute(#name, &value.to_string());
                        }
                    }
                } else {
                    let value = helper::get_str_html_value_attribute(value, true);
                    quote!{
                        #new_real.set_attribute(#name, #value);
                    }
                }
            }
        }
    }
    fn set_const_value(
        &self,
        config: &Config,
        html_tag: &str,
        new_real: &TokenStream,
    ) -> TokenStream {
        let real_dom = &config.real_dom_path;
        let name = self.name.to_string();
        let value = &self.value;
        let (set_value, type_name) = match html_tag {
            "option" => (false, quote!{HtmlOptionElement}),
            "input" => (true, quote!{HtmlInputElement}),
            "textarea" => (true, quote!{HtmlTextAreaElement}),
            "select" => return TokenStream::new(), // value for select will be setted after creating its childs
            _ => (false, TokenStream::new()),
        };
        if set_value {
            if config.need_unwrap(&value) {
                quote!{
                    if let Some(ref value) = #value {
                        let converted_: &#real_dom::#type_name = #new_real.unchecked_ref();
                        converted_.set_value(value);
                    }
                }
            } else {
                let value = helper::get_str_html_value_attribute(value, true);
                quote!{{
                    let converted_: &#real_dom::#type_name = #new_real.unchecked_ref();
                    converted_.set_value(#value);
                }}
            }
        } else if config.need_unwrap(&value) {
            quote!{
                if let Some(ref value) = #value {
                    #new_real.set_attribute(#name, &value.to_string());
                }
            }
        } else {
            let value = helper::get_str_html_value_attribute(value, true);
            quote!{
                #new_real.set_attribute(#name, #value);
            }
        }
    }

    fn post_set_const_attributes(
        &self,
        config: &Config,
        html_tag: &str,
        new_real: &TokenStream,
    ) -> TokenStream {
        if html_tag != "select" {
            return TokenStream::new();
        }
        let real_dom = &config.real_dom_path;
        let value = &self.value;
        let name = self.name.to_string();
        if config.need_unwrap(value) {
            match name.as_str() {
                "value" => quote!{
                    if let Some(ref value) = #value{
                        let converted_: &#real_dom::HtmlSelectElement = #new_real.unchecked_ref();
                        converted_.set_value(value);
                    }
                },
                "index" => quote!{
                    if let Some(ref value) = #value{
                        let converted_: &#real_dom::HtmlSelectElement = #new_real.unchecked_ref();
                        converted_.set_selected_index(value);
                    }
                },
                _ => TokenStream::new(),
            }
        } else {
            match name.as_str() {
                "value" => quote!{{
                    let converted_: &#real_dom::HtmlSelectElement = #new_real.unchecked_ref();
                    converted_.set_value(#value);
                }},
                "index" => quote!{{
                    let converted_: &#real_dom::HtmlSelectElement = #new_real.unchecked_ref();
                    converted_.set_selected_index(#value);
                }},
                _ => TokenStream::new(),
            }
        }
    }

    fn create(&self, config: &Config, html_tag: &str, new_simi: &TokenStream) -> TokenStream {
        let name = self.name.to_string();
        let value = &self.value;
        if config.need_unwrap(value) {
            let create = match name.as_str() {
                "value" => match html_tag {
                    "select" => quote!{#new_simi.more.value = (*value).clone();},
                    "input" | "textarea" => {
                        quote!{ #new_simi.more.value = Some(value.to_string());}
                    }
                    _ => quote!{#new_simi.attributes_mut().insert(#name, value.to_string());},
                },
                "index" => quote!{#new_simi.more.index = **value;},
                "checked" => quote!{#new_simi.more.checked = Some(*value);},
                _ => quote!{#new_simi.attributes_mut().insert(#name, value.to_string());},
            };
            quote! {
                if let Some(ref value) = #value{
                    #create
                }
            }
        } else {
            match name.as_str() {
                "value" => match html_tag {
                    "select" => quote!{#new_simi.more.value = #value.clone();},
                    "input" | "textarea" => {
                        quote!{#new_simi.more.value = Some(#value.to_string());}
                    }
                    _ => quote!{ #new_simi.attributes_mut().insert(#name, #value.to_string()); },
                },
                "index" => quote!{#new_simi.more.index = #value.clone();},
                "checked" => quote!{#new_simi.more.checked = Some(#value);},
                _ => quote!{ #new_simi.attributes_mut().insert(#name, #value.to_string()); },
            }
        }
    }
}

impl CustomAttribute {
    fn set_const_attributes(&self, config: &Config, new_real: &TokenStream) -> TokenStream {
        let value = &self.value;
        let name = &self.name;
        if config.need_unwrap(value) {
            quote!{
                if let Some(ref value) = #value{
                    #new_real.set_attribute(#name, &value.to_string());
                }
            }
        } else {
            let value = helper::get_str_html_value_attribute(value, true);
            quote!{
                #new_real.set_attribute(#name, #value);
            }
        }
    }

    fn create(&self, config: &Config, new_simi: &TokenStream) -> TokenStream {
        let name = &self.name;
        let value = &self.value;
        if config.need_unwrap(value) {
            quote!{
                if let Some(ref value) = #value{
                    #new_simi.attributes_mut().insert(#name, value.to_string());
                }
            }
        } else {
            quote!{
                #new_simi.attributes_mut().insert(#name, #value.to_string());
            }
        }
    }
}

impl ElementContent {
    fn create(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        match self {
            ElementContent::NodeList(list) => list.create(config, parents),
            ElementContent::UbuComponent(comp, props) => {
                ElementContent::create_component(config, parents, comp, props)
            }
            ElementContent::ContextSubApp(sub_app) => {
                ElementContent::create_sub_app(config, parents, sub_app)
            }
            ElementContent::SimiCreatedSubApp(app_type) => {
                ElementContent::create_simi_created_sub_app(config, parents, app_type)
            }
            ElementContent::OneOfSimiCreatedSubApps(oneof) => oneof.create(config, parents),
            ElementContent::None => TokenStream::new(),
        }
    }

    fn create_component(
        config: &Config,
        parents: &CurrentParents,
        field: &TokenStream,
        props: &TokenStream,
    ) -> TokenStream {
        let context = &config.context;
        let simi_dom = &config.simi_dom_path;
        let sub_dom_node = quote!{new_sub_node_for_component_or_sub_app_};
        let (shadowing, effective_list, effective_real) = parents.rebinding();
        quote!{
            {
                #shadowing
                let #sub_dom_node = #effective_list.create_sub_dom(#simi_dom::SubDomType::Component);
                #effective_list.append_child(#sub_dom_node.into());
                #context.app_context.#field.render(#effective_real, #props);
            }
        }
    }

    fn create_sub_app(
        config: &Config,
        parents: &CurrentParents,
        field: &TokenStream,
    ) -> TokenStream {
        let context = &config.context;
        let simi_dom = &config.simi_dom_path;
        let sub_dom_node = quote!{new_sub_node_for_component_or_sub_app_};
        let (shadowing, effective_list, effective_real) = parents.rebinding();
        quote!{
            {
                #shadowing
                let #sub_dom_node = #effective_list.create_sub_dom(#simi_dom::SubDomType::SubApp);
                #effective_list.append_child(#sub_dom_node.into());
                #context.app_context.#field.set_real_parent_element(#effective_real);
            }
        }
    }

    fn create_simi_created_sub_app(
        config: &Config,
        parents: &CurrentParents,
        app_type: &TokenStream,
    ) -> TokenStream {
        let simi_dom = &config.simi_dom_path;
        let (shadowing, effective_list, effective_real) = parents.rebinding();
        let sub_app = quote!{ a_sub_app_created_by_simi };
        quote!{
            {
                #shadowing
                let #sub_app = #simi_dom::SimiCreatedSubApp::new::<#app_type>(#effective_real);
                #effective_list.append_child(#sub_app.into());
            }
        }
    }
}

impl ElementEvent {
    pub fn create_element_event(&self, config: &Config) -> TokenStream {
        let event_name = self.event.to_string().replacen("on", "", 1);
        let event_type = config.get_js_event_argument_name(&self.event.to_string());
        let simi_event_path = &config.simi_events_path;
        let real_dom = &config.real_dom_path;
        let mut handler = self.handler.clone();
        let parameter = if self.with_question_mark {
            let name = quote!{ element_event_argument_name_ };
            handler.push(TokenTree::Group(Group::new(
                Delimiter::Parenthesis,
                name.clone(),
            )));
            name
        } else {
            quote!{ _ }
        };
        let handler = TokenStream::from_iter(handler);
        match self.handler_type {
            EventHandlerType::MessageVariant => {
                let event_type = Ident::new(&event_type, Span::call_site());
                quote!{
                    #simi_event_path::#event_type::new(#event_name, move |#parameter: #real_dom::#event_type| #handler)
                }
            }
            EventHandlerType::ComponentField => {
                ::Error::with_span(
                    self.event.span(),
                    "`Simi` bug: create_element_event can only apply to an enum variant such as Msg::DoSomething",
                ).panic()
            }
            EventHandlerType::ContextMethod => {
                let event_type = format!("Context{}", event_type);
                let event_type = Ident::new(&event_type, Span::call_site());
                quote!{
                    #simi_event_path::#event_type::new(#event_name, move |app_context: &mut Self::Context| {
                        app_context.#handler;
                    })
                }
            }
        }
    }
}

impl Component {
    // This create a component that directly attach to the caller vDOM
    // For creation of components that will be used as a nested component (inside another component)
    // please see: Component::create_child_component
    fn create(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let main = &config.main_clone();
        let (shadowing, effective_list, effective_real) = parents.rebinding();
        let props = self
            .properties
            .as_ref()
            .map_or(quote!(()), |props| props.clone());

        let simi_comp = quote!{a_new_simi_comp_object_};
        let create_childs_of_simi_comp = self.create_all_childs(config, &simi_comp);
        let render_childs_of_simi_comp = self.render_all_childs(config, &simi_comp);

        let component_childs = quote!{a_reference_to_simi_comp_childs_list_};
        let init_component_fields = self.init_component_fields(config, &component_childs);

        let component_fields = self.component_fields();

        let user_comp_name = &self.name;
        let user_component = quote!{temporary_user_comp_instance_};
        let child_count = self.childs.len();
        let component_nodes = quote!{mutable_reference_to_simi_comp_nodes_};
        let context = quote!{a_context_for_rendering_user_comp_};
        quote!{
            {
                #shadowing
                let mut #simi_comp = #effective_list.create_component(#child_count);
                {
                    // This is currently for solve bug for top component that never
                    // have something like ChildPlaceholder
                    #simi_comp.set_real_parent(#effective_real);

                    // Create all child components and add to its (#new_comp)
                    // child list (simi_dom::component::Component::childs)
                    // All the childs are not renderd yet, they will be rendered
                    // after #new_comp is rendered. At that point, the real_parent
                    // is already setted.
                    #create_childs_of_simi_comp
                }
                {
                    let (#component_nodes, #component_childs) = #simi_comp.nodes_childs_mut();
                    let #user_component = {
                        #init_component_fields
                        #user_comp_name {
                            #component_fields
                        }
                    };
                    let #context = CompRenderContext::new(#component_nodes, #effective_real, #main, None);
                    #user_component.render(&#props, #context);
                }
                {
                    #render_childs_of_simi_comp
                }
                #effective_list.append_child(#simi_comp.into());
            }
        }
    }

    fn create_all_childs(&self, config: &Config, parent_comp: &TokenStream) -> TokenStream {
        let create_child_components = self
            .childs
            .iter()
            .map(|c| c.create(config, &parent_comp))
            .collect();
        build_stream::from_streams(create_child_components)
    }

    fn render_all_childs(&self, config: &Config, parent_comp: &TokenStream) -> TokenStream {
        let render_child_components = self
            .childs
            .iter()
            .enumerate()
            .map(|(i, c)| c.render(config, &parent_comp, i))
            .collect();
        build_stream::from_streams(render_child_components)
    }

    fn init_component_fields(
        &self,
        config: &Config,
        component_childs: &TokenStream,
    ) -> TokenStream {
        let mut init_component_fields = TokenStream::new();
        init_component_fields.extend(
            self.events
                .iter()
                .map(|e| e.init_component_field_for_create(config)),
        );
        init_component_fields.extend(
            self.expressions
                .iter()
                .map(|e| e.init_component_field_for_create(config)),
        );
        init_component_fields.extend(
            self.literals
                .iter()
                .map(|l| l.init_component_field_for_create(config)),
        );

        let iter = quote!{component_childs_iter_for_weak_component_};
        init_component_fields.extend(quote!{let mut #iter = #component_childs.iter();});
        init_component_fields.extend(self.childs.iter().map(|c| {
            let id = &c.id;
            quote!{
                let #id = ::std::cell::Cell::new(Some(::std::rc::Rc::downgrade(#iter.next().unwrap())));
            }
        }));
        init_component_fields
    }

    fn create_child_component(&self, config: &Config, parent_comp: &TokenStream) -> TokenStream {
        let child_component = quote!{a_new_child_of_the_parent_component_};
        let create_childs_of_child_component = self.create_all_childs(config, &child_component);

        let child_count = self.childs.len();
        let local_parent_comp = quote!{a_local_reference_to_the_parent_component_};
        quote!{
            {
                let #local_parent_comp = &mut #parent_comp;
                let mut #child_component = #local_parent_comp.create_component(#child_count);
                {
                    // If #child_component has some childs, they will be created here
                    // and added (see the last line of this `quote!`) to #child_component.childs
                    // in exact order in raw_dom::Component::childs. Then, in #init_component_fields
                    // these childs are borrowed and assign to its id with the same order
                    #create_childs_of_child_component
                }
                #local_parent_comp.add_child_component(#child_component);
            }
        }
    }

    fn render_child_component(
        &self,
        config: &Config,
        parent_comp: &TokenStream,
        index: usize,
    ) -> TokenStream {
        let main = &config.main_clone();
        let props = self
            .properties
            .as_ref()
            .map_or(quote!(()), |props| props.clone());

        let child_component = quote!{a_new_child_of_the_parent_component_};
        let render_childs_of_child_component = self.render_all_childs(config, &child_component);

        let component_childs = quote!{a_reference_to_component_childs_list_};
        let init_component_fields = self.init_component_fields(config, &component_childs);

        let component_fields = self.component_fields();
        let user_comp_name = &self.name;
        let user_component = quote!{temporary_user_component_instance_};
        let component_nodes = quote!{mutable_reference_to_simi_component_nodes_};
        let context = quote!{a_context_for_rendering_user_component_};
        quote!{
            {
                let mut #child_component = #parent_comp.childs().get(#index).expect("Component child: Must have a child").borrow_mut();
                {
                    let insert_before = #child_component.take_place_holder();
                    let (#component_nodes, #component_childs, real_element_as_parent) = #child_component.nodes_childs_mut_real_parent();
                    let #user_component = {
                        #init_component_fields
                        #user_comp_name {
                            #component_fields
                        }
                    };
                    let #context = CompRenderContext::new(#component_nodes, real_element_as_parent, #main, insert_before.clone());
                    #user_component.render(&#props, #context);
                    if let Some(ref ib) = insert_before{
                        let node: &::simi::interop::real_dom::Node = real_element_as_parent.as_ref();
                        node.remove_child(ib.as_ref());
                    }
                }
                #render_childs_of_child_component
            }
        }
    }
}

impl ComponentEvent {
    fn init_component_field_for_create(&self, config: &Config) -> TokenStream {
        let ee = &config.simi_events_path;
        let id = &self.id;
        let event = self.event.create_element_event(config);
        quote!{
            let #id: ::std::cell::Cell<Option<Box<#ee::SimiElementEvent<_>>>>  = ::std::cell::Cell::new(Some(Box::new(#event)));
        }
    }
}

impl ComponentExpression {
    fn init_component_field_for_create(&self, _config: &Config) -> TokenStream {
        let id = &self.id;
        let expression = &self.expression;
        // Arguments for a component are always wrapped in Option<>
        quote!{
            let #id = Some(#expression);
        }
    }
}

impl ComponentLiteral {
    fn init_component_field_for_create(&self, _config: &Config) -> TokenStream {
        let ComponentLiteral { id, value } = self;
        quote!{
            let #id = Some(#value);
        }
    }
}

impl ComponentChild {
    fn create(&self, config: &Config, simi_comp: &TokenStream) -> TokenStream {
        match self.child {
            ComponentChildInfo::Component(ref component) => {
                component.create_child_component(config, simi_comp)
            }
            ComponentChildInfo::Element(ref element) => element.create_as_component(simi_comp),
        }
    }

    fn render(&self, config: &Config, simi_comp: &TokenStream, index: usize) -> TokenStream {
        match self.child {
            ComponentChildInfo::Component(ref component) => {
                component.render_child_component(config, simi_comp, index)
            }
            ComponentChildInfo::Element(ref element) => {
                element.render_as_component(config, simi_comp, index)
            }
        }
    }
}

impl IfElse {
    fn create_by_single_closure(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let context = &config.context;

        let closure_name = self
            .single_closure_name
            .as_ref()
            .expect("Simi bug: Closure must available before generating code");

        let nodes_list = parents.node_list();
        let real_parent = parents.real_parent();

        quote! {
            {
                let mut if_else_arm_node = #nodes_list.create_arm();
                #closure_name(&mut if_else_arm_node, #real_parent, &#context);
                #nodes_list.append_child(if_else_arm_node.into());
            }
        }
    }
}

impl Match {
    fn create_by_single_closure(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let context = &config.context;

        let closure_name = self
            .single_closure_name
            .as_ref()
            .expect("Simi bug: Closure must available before generating code");

        let nodes_list = parents.node_list();
        let real_parent = parents.real_parent();

        quote! {
            {
                let mut match_arm_node = #nodes_list.create_arm();
                #closure_name(&mut match_arm_node, #real_parent, &#context);
                #nodes_list.append_child(match_arm_node.into());
            }
        }
    }
}

impl For {
    fn create(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let simi_dom = &config.simi_dom_path;
        let node_list = parents.node_list();
        let simi_for_loop = quote!{a_new_simi_for_loop_node_};
        let real_parent = parents.real_parent();
        let local_parent = quote!{local_name_for_outer_parent_for_a_for_loop_};
        let sub_parents = CurrentParents::new_root(simi_for_loop.clone(), local_parent.clone());

        let create = self.node.create(config, &sub_parents);

        let iter_expr = &self.iter_expr;
        quote! {
            let mut #simi_for_loop = #node_list.create_node_list();
            {
                let #local_parent = #real_parent;
                for #iter_expr {
                    #create
                }
            }
            #node_list.append_child(#simi_dom::Node::NoneKeyedFor(#simi_for_loop));
        }
    }
}

impl OneOfSimiCreatedSubApps {
    fn create(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let match_ident = &self.match_ident;
        let match_expr = &self.match_expr;

        let (shadowing, effective_list, effective_real) = parents.rebinding();
        let arms = self
            .app_arms
            .iter()
            .map(|arm| arm.create(config, &effective_real));
        let sub_app = quote!{ a_sub_app_created_by_simi };
        quote!{
            {
                #shadowing
                let #sub_app = #match_ident #match_expr {
                    #(#arms)*
                };
                #effective_list.append_child(#sub_app.into());
            }
        }
    }
}

impl AppArm {
    fn create(&self, config: &Config, effective_real: &TokenStream) -> TokenStream {
        let pattern = &self.pattern;
        let app_type = &self.app_type;
        let simi_dom = &config.simi_dom_path;
        quote!{
            #pattern => {
                #simi_dom::SimiCreatedSubApp::new::<#app_type>(#effective_real)
            }
        }
    }
}
