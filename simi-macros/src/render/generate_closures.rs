use super::raw_dom::*;
use super::{Config, CurrentParents};
use proc_macro2::*;

impl NodeList {
    pub fn generate_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        self.nodes
            .iter_mut()
            .for_each(|node| node.generate_closures(config, closures));
    }
}

impl Node {
    fn generate_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        match self {
            Node::Element(ref mut e) => e.generate_closures(config, closures),
            Node::Component(ref mut s) => s.generate_closures(config, closures),
            Node::IfElse(ref mut ie) => ie.generate_closure(config, closures),
            Node::Match(ref mut m) => m.generate_closure(config, closures),
            _ => {}
        }
    }
}

impl Element {
    fn generate_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        self.content.generate_closures(config, closures)
    }
}

impl ElementContent {
    fn generate_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        if let ElementContent::NodeList(ref mut nodes) = self {
            nodes.generate_closures(config, closures)
        }
    }
}

impl Component {
    fn generate_closures(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        self.childs.iter_mut().for_each(|child| {
            if let ComponentChildInfo::Element(ref mut e) = child.child {
                e.generate_closures(config, closures)
            }
        })
    }
}

impl IfElse {
    fn generate_closure(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        let single_closure_name = format!(
            "a_single_generated_closure_number_{}_for_if_else",
            closures.len()
        );
        let single_closure_name = Ident::new(&single_closure_name, Span::call_site());
        let simi_dom = &config.simi_dom_path;
        let app_type = config.get_app_type();
        let context_type = config.get_context_type();
        let simi_dom_arm = quote!{ simi_dom_arm_for_closure_ };
        let real_parent = quote!{ real_parent_for_closure_ };

        let mut arms: Vec<TokenStream> = self
            .arms
            .iter()
            .enumerate()
            .map(|(index, arm)| arm.generate_ifelse_arm(config, &simi_dom_arm, &real_parent, index))
            .collect();
        if self.no_last_else {
            let html_tag_path = &config.html_tag_path;
            let missing_else_index = self.arms.len();

            let missing_else = quote! {
                else {
                    // There is nothing to do when updating the missing else
                    if !#simi_dom_arm.is_same_index(Some(#missing_else_index)) {
                        let next_sibling = if #simi_dom_arm.is_same_index(None) {
                            // This means the arm is just created. There is no arm render yet.
                            None
                        }else{
                            #simi_dom_arm.remove_and_get_next_sibling(#real_parent.as_ref())
                        };
                        let phantom_else = #simi_dom_arm.nodes_mut().create_element::<#simi_dom::NoSpecial>("span", #html_tag_path::Span);
                        simi::interop::display_error(phantom_else.get_real_element().class_list().add_1("simi-empty-span-element-use-in-place-of-omitted-else"));
                        let next_sibling: Option<&simi::interop::real_dom::RealNode> = next_sibling.as_ref().map(|next| next.as_ref());
                        phantom_else.insert_to(#real_parent, next_sibling);
                        #simi_dom_arm.set_index(Some(#missing_else_index));
                        #simi_dom_arm.nodes_mut().append_child(phantom_else.into());
                    }
                }
            };
            arms.push(missing_else);
        }

        let closure = quote!{
            let #single_closure_name = |
                #simi_dom_arm: &mut #simi_dom::Arm<#app_type>,
                #real_parent: &simi::interop::real_dom::RealElement,
                context: &#context_type
            | {
                #(#arms)*
            };
        };
        closures.push(closure);
        self.single_closure_name = Some(single_closure_name);
    }
}

impl Match {
    fn generate_closure(&mut self, config: &Config, closures: &mut Vec<TokenStream>) {
        let single_closure_name = format!(
            "a_single_generated_closure_number_{}_for_match",
            closures.len()
        );
        let single_closure_name = Ident::new(&single_closure_name, Span::call_site());
        let simi_dom = &config.simi_dom_path;
        let app_type = config.get_app_type();
        let context_type = config.get_context_type();
        let simi_dom_arm = quote!{ simi_dom_arm_for_closure_ };
        let real_parent = quote!{ real_parent_for_closure_ };

        let arms: Vec<TokenStream> = self
            .arms
            .iter()
            .enumerate()
            .map(|(index, arm)| arm.generate_match_arm(config, &simi_dom_arm, &real_parent, index))
            .collect();

        let match_ident = &self.match_ident;
        let match_expr = &self.match_expr;

        let closure = quote!{
            let #single_closure_name = |
                #simi_dom_arm: &mut #simi_dom::Arm<#app_type>,
                #real_parent: &simi::interop::real_dom::RealElement,
                context: &#context_type
            | {
                #match_ident #match_expr {
                    #(#arms)*
                }
            };
        };
        closures.push(closure);
        self.single_closure_name = Some(single_closure_name);
    }
}

impl Arm {
    fn generate_ifelse_arm(
        &self,
        config: &Config,
        simi_dom_arm: &TokenStream,
        real_parent: &TokenStream,
        index: usize,
    ) -> TokenStream {
        let arm_starting = self
            .condition
            .as_ref()
            .map_or(quote! { else }, |condition| {
                if index == 0 {
                    quote! { if #condition }
                } else {
                    quote! { else if #condition }
                }
            });

        let body = self.generate_arm_body(config, simi_dom_arm, real_parent, index);

        quote! {
            #arm_starting {
                #body
            }
        }
    }

    fn generate_match_arm(
        &self,
        config: &Config,
        simi_dom_arm: &TokenStream,
        real_parent: &TokenStream,
        index: usize,
    ) -> TokenStream {
        let body = self.generate_arm_body(config, simi_dom_arm, real_parent, index);
        let pattern = self
            .condition
            .as_ref()
            .expect("Simi bug: There must be a pattern for a match arm");
        quote! {
            #pattern => {
                #body
            }
        }
    }

    fn generate_arm_body(
        &self,
        config: &Config,
        simi_dom_arm: &TokenStream,
        real_parent: &TokenStream,
        index: usize,
    ) -> TokenStream {
        let nodes_list = quote! { #simi_dom_arm.nodes_mut() };
        let next_sibling = quote!{ the_real_node_after_this_arm_ };
        let parents_for_creating =
            CurrentParents::new_root(nodes_list.clone(), real_parent.clone())
                .insert_before_node(next_sibling.clone());
        let parents_for_updating =
            CurrentParents::new_root(nodes_list.clone(), real_parent.clone());

        let create_arm = self.nodes.create(config, &parents_for_creating);
        let update_arm = self.nodes.update(config, &parents_for_updating);

        quote! {
            if #simi_dom_arm.is_same_index(Some(#index)) {
                #update_arm
            }else{
                let #next_sibling = if #simi_dom_arm.is_same_index(None) {
                    // This means the arm is just created. There is no arm render yet.
                    None
                }else{
                    #simi_dom_arm.remove_and_get_next_sibling(#real_parent.as_ref())
                };
                let #next_sibling: Option<&simi::interop::real_dom::RealNode> = #next_sibling.as_ref().map(|next| next.as_ref());
                #create_arm
                #simi_dom_arm.set_index(Some(#index));
            }
        }
    }
}
