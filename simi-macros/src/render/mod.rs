use self::raw_dom::NodeList;
use fnv;
use proc_macro2::*;
use quote::ToTokens;
use simi_html_tags;

mod create;
mod generate_closures;
mod parse;
pub mod raw_dom;
mod update;

pub(crate) use self::parse::NO_UPDATE_PREFIX;

impl NodeList {
    pub(crate) fn render(self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let create_new = self.create(config, parents);
        let update_current = self.update(config, parents);
        let root_node_list = parents.node_list.clone();
        quote!{
            if #root_node_list.count() == 0 {
                #create_new
            }else{
                #update_current
            }
        }
    }
}

pub struct Config {
    is_error: ::std::cell::Cell<bool>,
    pub options: Options,
    context: TokenStream,

    simi_dom_path: TokenStream,
    html_tag_path: TokenStream,
    simi_events_path: TokenStream,
    real_dom_path: TokenStream,
    pub log_error: TokenStream,

    tags: fnv::FnvHashSet<&'static str>,
    element_events: fnv::FnvHashSet<&'static str>,
}

impl Config {
    pub fn new(options: Options, context: TokenStream, _props: &str) -> Self {
        Self {
            is_error: ::std::cell::Cell::new(false),
            options,
            context,
            simi_dom_path: quote!{simi::simi_dom},
            html_tag_path: quote!{simi::html_tags::HtmlTag},
            simi_events_path: quote!{simi::element_events},
            //interop_events_path: quote!{simi::interop::real_dom},
            real_dom_path: quote!{simi::interop::real_dom},
            log_error: quote!{simi::interop::log_error},

            tags: simi_html_tags::tags(),
            element_events: vec![
                "onfocus",
                "onblur",
                "onreset",
                "onsubmit",
                "onkeydown",
                "onkeypress",
                "onkeyup",
                "onmouseenter",
                "onmouseover",
                "onmousemove",
                "onmousedown",
                "onmouseup",
                "onauxclick",
                "onclick",
                "ondblclick",
                "oncontextmenu",
                "onwheel",
                "onmouseleave",
                "onmouseout",
                "onselect",
                "onpointerlockchange",
                "onpointerlockerror",
                "oninput",
                "onchange",
            ]
            .into_iter()
            .collect(),
        }
    }

    pub fn get_actual_element_event(&self, name: Ident) -> Option<Ident> {
        if name == "ondoubleclick" {
            return Some(Ident::new("ondblclick", Span::call_site()));
        }
        if self.element_events.contains(name.to_string().as_str()) {
            Some(name)
        } else {
            None
        }
    }

    pub fn get_js_event_argument_name(&self, name: &str) -> &str {
        match name {
            "onblur" | "onfocus" => "FocusEvent",
            "onchange" | "onreset" | "onsubmit" | "onpointerlockchange" | "onpointerlockerror" => {
                "Event"
            }
            "onkeydown" | "onkeypress" | "onkeyup" => "KeyboardEvent",
            "onauxclick" | "onclick" | "ondblclick" | "onmouseenter" | "onmouseover"
            | "onmousemove" | "onmousedown" | "onmouseup" | "onmouseleave" | "onmouseout"
            | "oncontextmenu" => "MouseEvent",
            "onwheel" => "WheelEvent",
            "onselect" => "UiEvent",
            "oninput" => "InputEvent",

            _ => panic!(format!(
                "JS argument name for {} is not coverred yet. Please file a bug",
                name
            )),
        }
    }

    pub fn panic_on_error(&self) {
        if self.is_error.get() {
            panic!("Macro stop because of previous error");
        }
    }

    pub fn set_error(&self) {
        self.is_error.set(true);
    }

    pub fn main_clone(&self) -> TokenStream {
        let context = &self.context;
        quote!{#context.main.clone()}
    }

    pub fn need_unwrap(&self, name: &TokenStream) -> bool {
        match self.options.content_type {
            ContentType::Main => false,
            ContentType::Component => {
                let name = name
                    .clone()
                    .into_iter()
                    .map(|t| t.to_string())
                    .collect::<String>();
                name.starts_with("self.")
            }
        }
    }

    pub fn need_insert(&self, parents: &CurrentParents) -> bool {
        parents.root_of_render && self.options.content_type == ContentType::Component
    }

    pub fn get_app_type(&self) -> Ident {
        if let Some(ref ident) = self.options.app_type {
            ident.clone()
        } else {
            match self.options.content_type {
                ContentType::Main => Ident::new("Self", Span::call_site()),
                ContentType::Component => Ident::new("A", Span::call_site()),
            }
        }
    }

    pub fn get_context_type(&self) -> TokenStream {
        let app_type = self.get_app_type();
        match self.options.content_type {
            ContentType::Main => quote!{ AppRenderContext<#app_type> },
            ContentType::Component => quote!{ CompRenderContext<#app_type> },
        }
    }
}

pub struct CurrentParents {
    root_of_render: bool,
    // Current NodeList that a new Node will be attached to
    node_list: TokenStream,
    // A real parent only available under the first level of real element created by `render!`
    real_parent: TokenStream,
    insert_before_node: Option<TokenStream>,
}

impl CurrentParents {
    pub fn new_root(node_list: TokenStream, real_parent: TokenStream) -> Self {
        Self {
            root_of_render: true,
            node_list,
            real_parent,
            insert_before_node: None,
        }
    }

    pub fn new_sub(node_list: TokenStream, real_parent: TokenStream) -> Self {
        Self {
            root_of_render: false,
            node_list,
            real_parent,
            insert_before_node: None,
        }
    }

    pub fn new_node_list_same_real_parent(&self, node_list: TokenStream) -> Self {
        Self {
            root_of_render: false,
            node_list,
            real_parent: self.real_parent.clone(),
            insert_before_node: None,
        }
    }

    pub fn insert_before_node(mut self, before: TokenStream) -> Self {
        self.insert_before_node = Some(before);
        self
    }

    pub fn create_text_node<T: ToTokens>(&self, config: &Config, text: T) -> TokenStream {
        let node_list = &self.node_list;
        let real_parent = &self.real_parent;
        let simi_text = quote!{new_simi_text_node_};

        let append_or_insert = if let Some(before) = self.insert_before() {
            quote!{ #simi_text.insert_to(&#real_parent, #before); }
        } else if config.need_insert(self) {
            quote!{ #simi_text.insert_to(&#real_parent, context.insert_before.as_ref()); }
        } else {
            quote!{ #simi_text.append_to(&#real_parent); }
        };
        // Only one new binding is created in this. It is `simi_text` and it will not shadow
        // any outer binding because outer binding will never be a #simi_text
        quote!{{
            let mut #simi_text = #node_list.create_text(#text);
            #append_or_insert
            #node_list.append_child(#simi_text.into());
        }}
    }

    pub fn node_list(&self) -> &TokenStream {
        &self.node_list
    }

    pub fn real_parent(&self) -> &TokenStream {
        &self.real_parent
    }

    /// This return (shadowing, effective_list, effective_real)
    pub fn rebinding(&self) -> (TokenStream, TokenStream, TokenStream) {
        if self.root_of_render {
            // In case of this is for the root list, we just use its name directly.
            // We do not need to setup a new binding for `node_list`
            // At root of render, we do not have a real parent
            (
                TokenStream::new(),
                self.node_list.clone(),
                self.real_parent.clone(),
            )
        } else {
            // In case of this is a sublist, we must define new binding to a reference of the list.
            // Because the name of some binding is reuse in every nested scope. If we do not define a new
            // binding, it maybe shadowed away!
            let node_list = &self.node_list;
            let real_parent = &self.real_parent;
            let local_real_parent = quote!{ a_local_reference_to_the_real_parent_ };

            let local_simi_parent = quote! {
                a_local_reference_to_the_simi_parent_
            };
            let setup = quote!{
                let #local_simi_parent = #node_list;
                let #local_real_parent = #real_parent;
            };
            (setup, local_simi_parent, local_real_parent)
        }
    }

    pub fn insert_before(&self) -> Option<&TokenStream> {
        self.insert_before_node.as_ref()
    }
}

pub enum NameMode {
    RustIdentifer,
    HtmlElementAttributeName,
}

#[derive(PartialEq)]
pub enum ContentType {
    Main,
    Component,
}

pub struct Options {
    pub is_debug: bool,
    pub content_type: ContentType,
    pub app_type: Option<Ident>,
}

impl Options {
    pub fn for_component() -> Self {
        Self {
            is_debug: false,
            content_type: ContentType::Component,
            app_type: None,
        }
    }
}

impl Default for Options {
    fn default() -> Self {
        Self {
            is_debug: false,
            content_type: ContentType::Main,
            app_type: None,
        }
    }
}
