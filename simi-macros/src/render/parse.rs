use super::raw_dom::*;
use super::Config;
use helper::{self, PunctedPortion, PushBackStream};
use proc_macro2::*;
use std::iter::FromIterator;
use Error;

pub(crate) const NO_UPDATE_PREFIX: char = '#';
const COMPONENT_CHILD_PREFIX: char = '$';
const ELEMENT_ATTRIBUTE_VALUE_SEPARATOR: char = '=';
const COMPONENT_FIELD_VALUE_SEPARATOR: char = ':';
const COMPONENT_ELEMENT_EVENT_HANDLER_SEPARATOR: char = '=';

const CONTEXT_SUB_APP_ELEMENT: &str = "context_sub_application";
const CONTEXT_UBU_COMPONENT_ELEMENT: &str = "context_ubu_component";
const SUB_APPS_ELEMENT: &str = "sub_application";
const ONE_OF_APPS_ELEMENT: &str = "one_of_sub_applications";

fn is_a_conditional_class(tokens: &mut Vec<TokenTree>) -> bool {
    let last = tokens.pop().unwrap();
    if let TokenTree::Punct(ref p) = last {
        if p.as_char() == '?' {
            return true;
        }
    }
    tokens.push(last);
    false
}

impl NodeList {
    pub(crate) fn parse(config: &Config, mut input: PushBackStream) -> Self {
        const WANTED: &str =
            "Expected things like: div () {} ComponentTypeName () {} if {} else {} for {} $child_id expression \"literal\"";
        let mut list = Vec::new();
        loop {
            let no_update = input.has_no_update();
            let token = if let Some(token) = input.next() {
                token
            } else {
                break;
            };
            match token {
                TokenTree::Ident(ident) => {
                    let name = ident.to_string();
                    if helper::is_camel_case(&name) {
                        // It is a component
                        list.push(Node::Component(Component::parse(
                            config, ident, no_update, &mut input,
                        )))
                    } else if config.tags.contains(name.as_str())
                        && !input.next_is_dot_or_double_colon()
                    {
                        input.push_back(TokenTree::Ident(ident));
                        // It is a supported tag
                        list.push(Node::Element(Box::new(Element::parse(
                            config, true, no_update, &mut input,
                        ))));
                    } else {
                        // It is (the first part of) an expression
                        match name.as_str() {
                            CONTEXT_SUB_APP_ELEMENT => {
                                let mut input = input.take_input_from_brace(&ident);
                                let e = Element::parse_context_sub_app(
                                    config, &ident, no_update, &mut input,
                                );
                                list.push(Node::Element(Box::new(e)));
                            }
                            CONTEXT_UBU_COMPONENT_ELEMENT => {
                                let mut input = input.take_input_from_brace(&ident);
                                let e = Element::parse_context_ubu_comp(
                                    config, &ident, no_update, &mut input,
                                );
                                list.push(Node::Element(Box::new(e)));
                            }
                            SUB_APPS_ELEMENT => {
                                let mut input = input.take_input_from_brace(&ident);
                                let e = Element::parse_simi_created_sub_app(
                                    config, &ident, no_update, &mut input,
                                );
                                list.push(Node::Element(Box::new(e)));
                            }
                            ONE_OF_APPS_ELEMENT => {
                                let mut input = input.take_input_from_brace(&ident);
                                let e = Element::parse_one_of_simi_created_sub_apps(
                                    config, &ident, no_update, &mut input,
                                );
                                list.push(Node::Element(Box::new(e)));
                            }
                            "if" => {
                                let ifelse = IfElse::parse(config, ident, no_update, &mut input);
                                list.push(Node::IfElse(ifelse));
                            }
                            "match" => {
                                let match_ = Match::parse(config, &ident, no_update, &mut input);
                                list.push(Node::Match(match_));
                            }
                            "for" => {
                                let for_ = For::parse(config, &ident, no_update, &mut input);
                                list.push(Node::For(for_));
                            }
                            _ => {
                                // Anything else is now treated as an expression
                                // self.something;
                                // var_name;
                                // function();
                                list.push(Node::Expression(Expression {
                                    no_update,
                                    expression: input.take_expression(TokenTree::Ident(ident)),
                                }));
                            }
                        }
                    }
                }
                TokenTree::Literal(literal) => list.push(Node::Literal(literal)),
                TokenTree::Group(group) => {
                    Error::with_span(group.span(), &format!("Unexpected block here. {}", WANTED))
                        .panic()
                }
                TokenTree::Punct(punct) => match punct.as_char() {
                    COMPONENT_CHILD_PREFIX => {
                        if config.options.content_type == super::ContentType::Main {
                            Error::with_span(
                                punct.span(),
                                "A child placeholder is only supported in a `component!`",
                            )
                            .panic()
                        }
                        let child = input.take_self_field();
                        list.push(Node::ChildPlaceholder(child));
                    }
                    _ => Error::with_span(punct.span(), &format!("Unknown token. {}", WANTED))
                        .panic(),
                },
            }
        }
        Self { nodes: list }
    }
}

impl Element {
    fn parse(
        config: &Config,
        allow_content: bool,
        no_update: bool,
        input: &mut PushBackStream,
    ) -> Self {
        let tag = match input.next() {
            Some(TokenTree::Ident(ident)) => ident,
            Some(token) => Error::with_token(
                &token,
                "simi-macro internal parse bug: Expect html tag identifier instead of this",
            )
            .panic(),
            None => panic!("simi-macro internal parse bug: Unexpected end of macro input"),
        };
        let attributes_stream = input.take_group(Delimiter::Parenthesis);
        let content = Element::parse_content(config, allow_content, input);
        if attributes_stream.is_none() && content.is_none() {
            match tag.to_string().as_str() {
                "br" | "hr" => {}
                _ => {
                    Error::with_span(tag.span(), "Html tags other than 'br' or 'hr' are required to have at least one of attribute-block `(...)` or content-block `{...}`").
                    panic()
                }
            }
        }
        let mut e = Self {
            tag,
            no_update,
            const_attributes: AttributeGroup::new(),
            expr_attributes: AttributeGroup::new(),
            permanent_classes: Vec::new(),
            conditional_classes: Vec::new(),
            events: Vec::new(),
            content: content.unwrap_or(ElementContent::None),
        };
        if let Some(input) = attributes_stream {
            let mut input = PushBackStream::new(input.stream());
            e.parse_attribute(config, &mut input);
            let mut index: Option<Span> = None;
            let mut value: Option<Span> = None;
            if !e
                .const_attributes
                .check_select_index_value(config, &mut index, &mut value)
            {
                e.expr_attributes
                    .check_select_index_value(config, &mut index, &mut value);
            }
        }
        e
    }

    fn parse_container(config: &Config, tag: &Ident, input: &mut PushBackStream) -> Self {
        input.expect_keyword_colon("container", tag, "Expect `container: <html_tag> (...)`");
        Element::parse(config, false, false, input)
    }

    fn parse_context_field(tag: &Ident, input: &mut PushBackStream) -> Ident {
        const WANTED: &str = "Expect `context_field: <a_field_ident>`";
        let kw = input.expect_keyword_colon("context_field", tag, WANTED);
        input.expect_ident(kw.span(), WANTED)
    }

    fn parse_context_sub_app(
        config: &Config,
        tag: &Ident,
        no_update: bool,
        input: &mut PushBackStream,
    ) -> Self {
        if no_update {
            Error::with_span(
                tag.span(),
                "A context-sub-app can not be no_update. Please remove `#` in front of this",
            )
            .emit(Some(config));
        }
        let mut element = Element::parse_container(config, tag, input);
        let context_field = Element::parse_context_field(tag, input);
        element.content = ElementContent::ContextSubApp(quote!{#context_field});
        element
    }

    fn parse_context_ubu_comp(
        config: &Config,
        tag: &Ident,
        no_update: bool,
        input: &mut PushBackStream,
    ) -> Self {
        if no_update {
            Error::with_span(
                tag.span(),
                "An context-ubu-component can not be no_update. Please remove `#` in front of this",
            )
            .emit(Some(config));
        }
        let mut element = Element::parse_container(config, tag, input);
        let context_field = Element::parse_context_field(tag, input);

        const WANTED: &str = "Expect `props: <some_props_value>`";
        input.expect_keyword_colon("props", tag, WANTED);
        let props = input.take_all_to_end();

        element.content = ElementContent::UbuComponent(quote!{#context_field}, props);
        element
    }

    fn parse_simi_created_sub_app(
        config: &Config,
        tag: &Ident,
        no_update: bool,
        input: &mut PushBackStream,
    ) -> Self {
        if no_update {
            Error::with_span(
                tag.span(),
                "An sub application can not be no_update. Please remove `#` in front of this",
            )
            .emit(Some(config));
        }
        let mut element = Element::parse_container(config, tag, input);

        const WANTED: &str = "Expect `application_type: <UserAppType>`";
        let kw = input.expect_keyword_colon("application_type", tag, WANTED);
        let app_type = input.take_all_to_end();
        if app_type.is_empty() {
            Error::with_span(kw.span(), WANTED).panic();
        }

        element.content = ElementContent::SimiCreatedSubApp(app_type);
        element
    }

    fn parse_one_of_simi_created_sub_apps(
        config: &Config,
        tag: &Ident,
        no_update: bool,
        input: &mut PushBackStream,
    ) -> Self {
        if no_update {
            Error::with_span(
                tag.span(),
                "An sub application can not be no_update. Please remove `#` in front of this",
            )
            .emit(Some(config));
        }
        let mut element = Element::parse_container(config, tag, input);
        let kw = input.expect_keyword_colon(
            "apps",
            tag,
            "Expect `apps: match <match_value> { <match_arms> }`",
        );
        let oneof = OneOfSimiCreatedSubApps::parse(&kw, input);
        element.content = ElementContent::OneOfSimiCreatedSubApps(oneof);
        element
    }

    fn parse_attribute(&mut self, config: &Config, input: &mut PushBackStream) {
        let PunctedPortion {
            punct_found,
            tokens: mut name,
        } = match input.take_until_single_punct(ELEMENT_ATTRIBUTE_VALUE_SEPARATOR) {
            Some(rs) => rs,
            None => return,
        };
        if !punct_found {
            Error::with_token(
                name.first().unwrap(),
                &format!(
                    "Attributes must be provided in `name{0}value` pairs like: div (id{0}\"some_id\" class{0}\"some-class\")",
                    ELEMENT_ATTRIBUTE_VALUE_SEPARATOR
                )
            ).
            panic()
        }
        while let Some((no_update, value, next_name)) = input.take_value_and_next_name(
            config,
            ELEMENT_ATTRIBUTE_VALUE_SEPARATOR,
            &super::NameMode::HtmlElementAttributeName,
        ) {
            if name.len() == 1 {
                self.add_attribute(config, name.pop().unwrap(), no_update, value);
            } else if is_a_conditional_class(&mut name) {
                // It is a `some_var?=some_condition`
                // Or a `self.some_var?=some_condition`
                self.conditional_classes.push(ConditionalClass {
                    name: LiteralOrTokenStream::Stream(TokenStream::from_iter(name)),
                    value: TokenStream::from_iter(value),
                });
            } else {
                self.add_custom_attribute(name, no_update, value);
            }

            name = next_name;
            if name.is_empty() {
                if let Some(token) = input.next() {
                    Error::with_token(
                        &token,
                        "`Simi` bug: how name.len()=0 and there is still this?",
                    )
                    .panic();
                }
                break;
            }
        }
        if !name.is_empty() {
            Error::with_token(name.last().unwrap(), "Value for this attribute?").panic();
        }
    }

    fn add_attribute(
        &mut self,
        config: &Config,
        name: TokenTree,
        no_update: bool,
        value: Vec<TokenTree>,
    ) {
        if !self.is_valid_regular_attribute(&name.to_string()) {
            Error::with_token(
                &name,
                &format!(
                    "This attribute is not supported by `{}`",
                    self.tag.to_string()
                ),
            )
            .emit(Some(config));
            return;
        }
        match name {
            TokenTree::Ident(ident) => {
                let name = ident.to_string();
                // It is an event
                if let Some(ident) = config.get_actual_element_event(ident.clone()) {
                    self.events
                        .push(ElementEvent::parse(ident, no_update, value));
                    return;
                }
                if name == "class" {
                    self.add_class(config, value);
                    return;
                }
                // It is not an event nor class
                let is_literal = if value.len() == 1 {
                    match value.first().unwrap() {
                        TokenTree::Literal(_) => true,
                        TokenTree::Ident(ident) => match ident.to_string().as_str() {
                            "true" | "false" => true,
                            _ => false,
                        },
                        _ => false,
                    }
                } else {
                    false
                };
                let a = RegularAttribute {
                    name: ident,
                    value: TokenStream::from_iter(value),
                };
                if is_literal || no_update {
                    self.const_attributes.regulars.push(a);
                } else {
                    self.expr_attributes.regulars.push(a);
                }
            }
            TokenTree::Punct(item) => {
                Error::with_span(item.span(), "Unexpect token in attribute name")
                    .emit(Some(config));
            }
            TokenTree::Literal(item) => {
                if item.to_string().split_whitespace().count() > 1 {
                    Error::with_span(
                        item.span(),
                        "Literal attribute name is used for conditional class. Conditional class name does not allow to contain whitespace",
                    ).emit(Some(config));
                }
                if no_update {
                    Error::with_span(
                        value.first().unwrap().span(),
                        "Constant expression for a conditional class is not supported",
                    )
                    .emit(Some(config));
                } else {
                    self.conditional_classes.push(ConditionalClass {
                        name: LiteralOrTokenStream::Literal(item),
                        value: TokenStream::from_iter(value),
                    });
                }
            }
            TokenTree::Group(item) => {
                Error::with_span(item.span(), "Unexpected group of code").emit(Some(config));
            }
        }
    }

    fn add_class(&mut self, config: &Config, tokens: Vec<TokenTree>) {
        let mut input = PushBackStream::new(TokenStream::from_iter(tokens));
        while let Some(token) = input.next() {
            match token {
                TokenTree::Literal(lit) => self
                    .permanent_classes
                    .push(LiteralOrTokenStream::Literal(lit)),
                TokenTree::Ident(ident) => {
                    let value = input.take_expression(TokenTree::Ident(ident));
                    self.permanent_classes
                        .push(LiteralOrTokenStream::Stream(value))
                }
                token => {
                    Error::with_token(
                        &token,
                        "Unexpected token. Expect class=<space separated list of class names here>",
                    )
                    .emit(Some(config));
                }
            }
        }
    }

    fn add_custom_attribute(
        &mut self,
        name: Vec<TokenTree>,
        no_update: bool,
        value: Vec<TokenTree>,
    ) {
        let is_literal = if value.len() == 1 {
            match value.first().unwrap() {
                TokenTree::Literal(_) => true,
                TokenTree::Ident(ident) => match ident.to_string().as_str() {
                    "true" | "false" => true,
                    _ => false,
                },
                _ => false,
            }
        } else {
            false
        };
        let a = CustomAttribute::new(TokenStream::from_iter(name), TokenStream::from_iter(value));
        if is_literal || no_update {
            self.const_attributes.customs.push(a);
        } else {
            self.expr_attributes.customs.push(a);
        }
    }

    fn parse_content(
        config: &Config,
        allow_content: bool,
        input: &mut PushBackStream,
    ) -> Option<ElementContent> {
        if let Some(input) = input.take_group(Delimiter::Brace) {
            if !allow_content {
                Error::with_span(input.span(), "Unexpected group. Current html element tag is not allow to have a body content, maybe because it is a container for sub app or similar things").panic();
            }
            let mut input = PushBackStream::new(input.stream());
            Some(ElementContent::NodeList(NodeList::parse(config, input)))
        } else if let Some(g) = input.take_group(Delimiter::Bracket) {
            Error::with_span(
                g.span(),
                &format!(
                    "The use of `[..]` for `sub app` or `ubu component` is deprecated. Please use `{}` or `{}` instead. See `simi/README.md` for more information",
                    CONTEXT_SUB_APP_ELEMENT,
                    CONTEXT_UBU_COMPONENT_ELEMENT,
                )
            ).warn();
            Some(Element::parse_component_or_sub_app(config, &g))
        } else {
            return None;
        }
    }

    fn parse_component_or_sub_app(config: &Config, g: &Group) -> ElementContent {
        const WANTED: &str =
            "Expect `[component: context_field  props: some_value]` or `[sub_app: context_field]`";
        let mut input = PushBackStream::new(g.stream());
        match input.take_until_single_punct(':') {
            Some(mut pp) => {
                if !pp.punct_found || pp.tokens.len() != 1 {
                    Error::with_span(g.span(), WANTED).panic();
                }
                let name = pp.tokens.pop().unwrap();
                match name.to_string().as_str() {
                    "component" => match input.take_value_and_next_name(
                        config,
                        ':',
                        &super::NameMode::RustIdentifer,
                    ) {
                        Some((_, value, next_name)) => {
                            if next_name.is_empty() {
                                Error::with_span(
                                    g.span(),
                                    "Expect `props: some_value` for this component",
                                )
                                .panic();
                            } else if next_name.len() > 1 {
                                Error::with_span(
                                    g.span(),
                                    "`Simi` bug: Why this can have more than 1 token?",
                                )
                                .panic();
                            } else if next_name[0].to_string().as_str() != "props" {
                                Error::with_token(
                                    next_name.first().unwrap(),
                                    "Expect `props: some_value` instead of this",
                                )
                                .panic();
                            }
                            let field = TokenStream::from_iter(value);
                            let props = match input.take_value_and_next_name(
                                config,
                                ':',
                                &super::NameMode::RustIdentifer,
                            ) {
                                None => Error::with_token(
                                    next_name.first().unwrap(),
                                    "Expect `props: some_value`",
                                )
                                .panic(),
                                Some((_, value, next_name)) => {
                                    if !next_name.is_empty() {
                                        Error::with_token(
                                            next_name.first().unwrap(),
                                            &format!("Unexpected token. {}", WANTED),
                                        )
                                        .panic();
                                    }
                                    TokenStream::from_iter(value)
                                }
                            };
                            ElementContent::UbuComponent(field, props)
                        }
                        None => Error::with_span(g.span(), WANTED).panic(),
                    },
                    "sub_app" => match input.take_value_and_next_name(
                        config,
                        ':',
                        &super::NameMode::RustIdentifer,
                    ) {
                        None => {
                            Error::with_span(g.span(), "Expect `sub_app: context_field` in this")
                                .panic()
                        }
                        Some((_, value, next_name)) => {
                            if !next_name.is_empty() {
                                Error::with_token(next_name.first().unwrap(), "Unexpect token")
                                    .panic();
                            }
                            ElementContent::ContextSubApp(TokenStream::from_iter(value))
                        }
                    },
                    _ => {
                        Error::with_token(&name, "Expect `component` or `sub_app` instead of this")
                            .panic()
                    }
                }
            }
            _ => Error::with_span(g.span(), WANTED).panic(),
        }
    }
}

impl OneOfSimiCreatedSubApps {
    fn parse(kw: &Ident, input: &mut PushBackStream) -> Self {
        const WANTED: &str = "Expect `match <match_expr>` { ... }";
        let mkw = input.expect_ident(kw.span(), WANTED);
        let (expr, g) = input.take_expression_and_brace_group();
        let mut input = match g {
            None => Error::with_span(mkw.span(), WANTED).panic(),
            Some(g) => PushBackStream::new(g.stream()),
        };
        let mut app_arms = Vec::new();
        while let Some(arm) = AppArm::parse(&mut input) {
            app_arms.push(arm);
        }
        Self {
            match_ident: mkw,
            match_expr: TokenStream::from_iter(expr),
            app_arms,
        }
    }
}

impl AppArm {
    fn parse(input: &mut PushBackStream) -> Option<Self> {
        let expr = match input.take_until_equal_gt() {
            None => return None,
            Some(vec) => TokenStream::from_iter(vec),
        };
        let span = input.last_discarded_token().span();
        let app_type = input.expect_ident(span, "Expect `=> <SimeAppType>`");
        // If there is a `,` next, just discard it
        input.next_is_single_punct(',');
        Some(Self {
            pattern: expr,
            app_type,
        })
    }
}

impl ElementEvent {
    fn parse(event: Ident, no_update: bool, mut handler: Vec<TokenTree>) -> Self {
        const WANTED: &str = "Expect something like `Msg::EnumVariant(?)`, `Msg::EnumVariant(some_value)`, `Msg::EnumVariant` (or `identifier` in case when the event is passed here)";
        helper::is_valid_simi_expression(&handler);
        let last = match handler.pop() {
            Some(last) => last,
            None => Error::with_span(event.span(), "Expect handler for this event").panic(),
        };
        let with_question_mark = if let TokenTree::Group(ref group) = last {
            if group.delimiter() != Delimiter::Parenthesis {
                Error::with_span(group.span(), WANTED).panic()
            }
            group.stream().to_string() == "?"
        } else {
            false
        };
        if !with_question_mark {
            handler.push(last);
        }
        let handler_type = if handler.len() > 2 {
            match handler[0].to_string().as_str() {
                "app_context" => {
                    if handler[1].to_string() == ":" {
                        EventHandlerType::ContextMethod
                    } else {
                        EventHandlerType::MessageVariant
                    }
                }
                "self" => {
                    if handler[1].to_string() == "." {
                        EventHandlerType::ComponentField
                    } else {
                        EventHandlerType::MessageVariant
                    }
                }
                _ => EventHandlerType::MessageVariant,
            }
        } else {
            EventHandlerType::MessageVariant
        };
        let handler = if handler_type == EventHandlerType::ContextMethod {
            handler[2..].to_vec()
        } else {
            handler
        };
        Self {
            no_update,
            handler_type,
            event,
            with_question_mark,
            handler,
        }
    }
}

impl Component {
    fn parse(config: &Config, comp: Ident, no_update: bool, input: &mut PushBackStream) -> Self {
        let mut s = Self {
            name: comp,
            no_update,
            properties: input
                .take_group(Delimiter::Parenthesis)
                .and_then(|g| Some(g.stream())),
            childs: Vec::new(),
            events: Vec::new(),
            expressions: Vec::new(),
            literals: Vec::new(),
        };
        s.parse_arguments(config, input);
        s
    }

    fn parse_arguments(&mut self, config: &Config, input: &mut PushBackStream) {
        const EXPECTED: &str = "Component arguments must be provided in `id: value` pairs like: `up: onclick=Msg::Up` `value: self.value`";
        let mut input = if let Some(g) = input.take_group(Delimiter::Brace) {
            PushBackStream::new(g.stream())
        } else {
            return;
        };

        let PunctedPortion {
            punct_found,
            tokens: mut name,
        } = match input.take_until_single_punct(COMPONENT_FIELD_VALUE_SEPARATOR) {
            Some(rs) => rs,
            None => return,
        };
        if name.is_empty() {
            // This is the case of `Component {}` => empty brace
            return;
        }
        if !punct_found || name.len() > 1 {
            Error::with_token(name.first().unwrap(), EXPECTED).panic()
        }

        let mut name = name.pop().unwrap();
        while let Some((no_update, value, mut next_name)) = input.take_value_and_next_name(
            config,
            COMPONENT_FIELD_VALUE_SEPARATOR,
            &super::NameMode::RustIdentifer,
        ) {
            self.add_argument(config, name, no_update, value);
            match next_name.len() {
                0 => break,
                1 => name = next_name.pop().unwrap(),
                _ => Error::with_token(
                    &next_name[1],
                    "Unexpect token. Argument id must be a single identifier.",
                )
                .panic(),
            }
        }
    }

    fn add_argument(
        &mut self,
        config: &Config,
        id: TokenTree,
        no_update: bool,
        value: Vec<TokenTree>,
    ) {
        if self.add_event(config, &id, no_update, &value) {
            return;
        }
        if self.add_child(config, &id, no_update, &value) {
            return;
        }
        if self.add_literal(&id, &value) {
            return;
        }
        // Treat as expression
        self.expressions.push(ComponentExpression {
            id,
            no_update,
            expression: TokenStream::from_iter(value),
        });
    }

    fn add_literal(&mut self, id: &TokenTree, value: &[TokenTree]) -> bool {
        if value.len() > 1 {
            return false;
        }
        let is_literal = match value.first() {
            Some(TokenTree::Literal(_)) => true,
            _ => false,
        };
        if !is_literal {
            return false;
        }
        self.literals.push(ComponentLiteral {
            id: id.clone(),
            value: value.first().unwrap().clone(),
        });
        true
    }

    fn add_event(
        &mut self,
        config: &Config,
        id: &TokenTree,
        no_update: bool,
        value: &[TokenTree],
    ) -> bool {
        let mut iter = value.iter();
        let event_name = match iter.next() {
            None => return false,
            Some(token) => match token {
                TokenTree::Ident(ident) => {
                    if let Some(ident) = config.get_actual_element_event(ident.clone()) {
                        ident
                    } else {
                        return false;
                    }
                }
                _ => return false,
            },
        };

        match iter.next() {
            None => return false,
            Some(token) => match token {
                TokenTree::Punct(p) if p.as_char() == COMPONENT_ELEMENT_EVENT_HANDLER_SEPARATOR => {
                }
                _ => return false,
            },
        }

        let event_handler: Vec<TokenTree> = iter.cloned().collect();
        if event_handler.is_empty() {
            Error::with_span(event_name.span(), "Expect handler for this event").panic()
        }
        let event = ElementEvent::parse(event_name, no_update, event_handler);
        self.events.push(ComponentEvent {
            no_update,
            id: id.clone(),
            event,
        });
        true
    }

    fn add_child(
        &mut self,
        config: &Config,
        id: &TokenTree,
        no_update: bool,
        value: &[TokenTree],
    ) -> bool {
        let mut iter = value.iter();
        let (ident, comp) = match iter.next() {
            None => return false,
            Some(token) => match token {
                TokenTree::Ident(ident) => {
                    let sident = ident.to_string();
                    if helper::is_camel_case(&sident) {
                        (ident.clone(), true)
                    } else if config.tags.contains(sident.as_str()) {
                        if value.len() < 2 {
                            // No, it is not a child of the snippet
                            return false;
                        }
                        // It is an html element as a snippet-child
                        (ident.clone(), false)
                    } else {
                        return false;
                    }
                }
                _ => return false,
            },
        };
        let input: Vec<TokenTree> = iter.cloned().collect();
        let input = TokenStream::from_iter(input);
        let mut input = PushBackStream::new(input);
        if comp {
            let comp = Component::parse(config, ident, no_update, &mut input);
            self.childs.push(ComponentChild {
                id: id.clone(),
                child: ComponentChildInfo::Component(comp),
            });
        } else {
            input.push_back(TokenTree::Ident(ident));
            let element = Element::parse(config, true, no_update, &mut input);
            self.childs.push(ComponentChild {
                id: id.clone(),
                child: ComponentChildInfo::Element(element),
            });
        }
        true
    }
}

fn check_for_empty_body(g: &Group) {
    if g.stream().is_empty() {
        Error::with_span(
            g.span(),
            "Empty body for a `for`/`if-else`/`match` is not allowed",
        )
        .panic();
    }
}

impl IfElse {
    fn parse(
        config: &Config,
        mut if_ident: Ident,
        no_update: bool,
        input: &mut PushBackStream,
    ) -> Self {
        let mut arms = Vec::new();
        let mut no_last_else = true;
        loop {
            let (condition, group) = input.take_expression_and_brace_group();

            if condition.is_empty() || group.is_none() {
                Error::with_span(if_ident.span(), "Expect `if <bool-expression> {...}`").panic()
            }

            let group = group.unwrap();
            check_for_empty_body(&group);
            arms.push(Arm::new(
                Some(TokenStream::from_iter(condition)),
                NodeList::parse(config, PushBackStream::new(group.stream())),
            ));

            if let Some(token1) = input.next() {
                if token1.to_string().as_str() == "else" {
                    if let Some(token2) = input.next() {
                        match token2 {
                            TokenTree::Ident(ref ident) if ident.to_string().as_str() == "if" => {
                                if_ident = ident.clone();
                                // Continue the loop
                            }
                            TokenTree::Group(ref g) if g.delimiter() == Delimiter::Brace => {
                                check_for_empty_body(g);
                                arms.push(Arm::new(
                                    None,
                                    NodeList::parse(config, PushBackStream::new(g.stream())),
                                ));
                                // End of if else with ELSE
                                no_last_else = false;
                                break;
                            }
                            token => Error::with_token(
                                &token,
                                "Expect `{...}` or `if` (because after an `else`) instead of this",
                            )
                            .panic(),
                        }
                    // Continue the loop to get (condition, nodes)
                    } else {
                        // Have `else` but nothing after it
                        Error::with_token(&token1, "Expect `{...}` after this").panic()
                    }
                } else {
                    // End of if...else if... (no final else)
                    // Next is another content
                    input.push_back(token1);
                    break;
                }
            } else {
                // End of if...else if... (no final else)
                // Also end of the content
                break;
            }
        }
        IfElse {
            no_update,
            single_closure_name: None,
            arms,
            no_last_else,
        }
    }
}

impl Match {
    fn parse(
        config: &Config,
        match_ident: &Ident,
        no_update: bool,
        input: &mut PushBackStream,
    ) -> Self {
        let (expr, group) = input.take_expression_and_brace_group();

        if expr.is_empty() || group.is_none() {
            Error::with_span(match_ident.span(), "Expect `match <bool-expression> {...}`").panic();
        }
        let group = group.unwrap();
        check_for_empty_body(&group);
        let mut arms = Vec::new();
        let mut input = PushBackStream::new(group.stream());
        while let Some(arm) = Arm::parse_next_match_arm(config, &mut input) {
            arms.push(arm);
        }
        Self {
            no_update,
            match_ident: match_ident.clone(),
            match_expr: TokenStream::from_iter(expr),
            arms,
            single_closure_name: None,
        }
    }
}

impl Arm {
    fn new(condition: Option<TokenStream>, nodes: NodeList) -> Self {
        Self {
            create_closure_name: None,
            update_closure_name: None,
            condition,
            nodes,
        }
    }

    fn parse_next_match_arm(config: &Config, input: &mut PushBackStream) -> Option<Self> {
        let pattern = match input.take_until_equal_gt() {
            Some(rs) => rs,
            None => return None,
        };
        let body = match input.take_group(Delimiter::Brace) {
            Some(rs) => {
                check_for_empty_body(&rs);
                rs.stream()
            }
            None => match input.take_until_punct(',') {
                Some(rs) => {
                    if rs.is_empty() {
                        Error::with_token(
                            input.last_discarded_token(),
                            "Empty body for a match arm is not allowed",
                        );
                    }
                    TokenStream::from_iter(rs)
                }
                None => Error::with_token(
                    input.last_discarded_token(),
                    "Expect a `{ simi html template }` after this",
                )
                .panic(),
            },
        };
        Some(Arm::new(
            Some(TokenStream::from_iter(pattern)),
            NodeList::parse(config, PushBackStream::new(body)),
        ))
    }
}

impl For {
    fn parse(
        config: &Config,
        for_ident: &Ident,
        no_update: bool,
        input: &mut PushBackStream,
    ) -> Self {
        let (expr, group) = input.take_expression_and_brace_group();
        if expr.is_empty() || group.is_none() {
            Error::with_span(for_ident.span(), "Expect `for <iterable-expression> {...}`").panic();
        }
        let group = group.unwrap();
        check_for_empty_body(&group);
        let mut nodes = NodeList::parse(config, PushBackStream::new(group.stream()));
        if nodes.nodes.len() != 1 {
            Error::with_span(
                group.span(),
                "Expect exactly 1 item at the root of the content of a `for` loop",
            )
            .emit(Some(config));
        }
        let node = nodes.nodes.pop().unwrap();
        if let Node::ChildPlaceholder(_) = node {
            Error::with_span(
                group.span(),
                "A child component is not allowed inside a `for` loop",
            )
            .emit(Some(config))
        }
        Self {
            no_update,
            iter_expr: TokenStream::from_iter(expr),
            node: Box::new(node),
        }
    }
}

#[test]
fn no_update() {
    let no_update = Punct::new('#', Spacing::Alone);
    let input = quote!{
        section (class="todoapp") {
            header(class="header") {
                h1 { "Simi todos" }
                input (
                    class="new-todo"
                    placeholder="What needs to be done?"
                    autofocus=true
                    onchange=#no_update Msg::New(?)
                )
            }
            section ( class="main" "hidden"=empty_list ) {
                input (
                    id="toggle-all"
                    class="toggle-all"
                    type="checkbox"
                    checked=all_completed
                    onchange=Msg::ToggleAll(!all_completed)
                )
                label ( for="toggle-all" ) { "Mark all as complete" }
                ul ( class="todo-list" ) {
                    for (index,task) in self.data.list().iter().enumerate() {
                        li ( "completed"=task.completed "editing"=self.is_editing(index) ) {
                            div(class="view"){
                                input(
                                    class="toggle"
                                    type="checkbox"
                                    checked=task.completed
                                    onchange=Msg::Toggle(index)
                                )
                                label (ondoubleclick=Msg::StartEditing(index)) { task.title }
                                button ( class="destroy" onclick=Msg::Remove(index) )
                            }
                            input (
                                class="edit"
                                value=task.title
                                onblur=Msg::BlurEditing(?)
                                onkeypress=Msg::EndEditing(?)
                            )
                        }
                    }
                }
            }
            footer ( class="footer" "hidden"=empty_list) {
                span ( class="todo-count" ) {
                    strong {item_left} self.item_left_label(item_left)
                }
                ul ( class="filters" ) {
                    li { a ( class="selected" href="#/" ) {"All"} }
                    li { a ( href="#/active" ) {"Active"} }
                    li { a ( href="#/completed" ) {"Completed"} }
                }
                button (
                    class="clear-completed"
                    "hidden"=completed_count==0
                    onclick=#no_update Msg::ClearCompleted
                ) { "Clear completed" }
            }
        }
        footer ( class="info" ) {
            p { "Double-click to edit a todo" }
            p { "Created by Limira" }
            p { "Part of Simi Project" }
            p { "Part of " a (href="http://todomvc.com") { "TodoMVC" } }
        }
    };
    let config = Config::new(super::Options::default(), TokenStream::new(), "props");
    let mut input = NodeList::parse(&config, PushBackStream::new(input));
    input.inspect_no_update();
    let mut states = Vec::new();
    input.collect_no_update_states(&mut states);
    let expects = vec![
        //========
        ("literal".to_string(), true),
        ("h1".to_string(), true),
        ("input".to_string(), true),
        ("header".to_string(), true),
        //========
        ("input".to_string(), false),
        ("literal".to_string(), true),
        ("label".to_string(), true),
        //>>>>>>>>
        ("input".to_string(), false),
        ("expression".to_string(), false),
        ("label".to_string(), false),
        ("button".to_string(), false),
        ("div".to_string(), false),
        ("input".to_string(), false),
        ("li".to_string(), false),
        ("For".to_string(), false),
        ("ul".to_string(), false),
        ("section".to_string(), false),
        //========
        ("expression".to_string(), false),
        ("strong".to_string(), false),
        ("expression".to_string(), false),
        ("span".to_string(), false),
        ("literal".to_string(), true),
        ("a".to_string(), true),
        ("li".to_string(), true),
        ("literal".to_string(), true),
        ("a".to_string(), true),
        ("li".to_string(), true),
        ("literal".to_string(), true),
        ("a".to_string(), true),
        ("li".to_string(), true),
        ("ul".to_string(), true),
        ("literal".to_string(), true),
        ("button".to_string(), false),
        ("footer".to_string(), false),
        ("section".to_string(), false),
        //========
        ("literal".to_string(), true),
        ("p".to_string(), true),
        ("literal".to_string(), true),
        ("p".to_string(), true),
        ("literal".to_string(), true),
        ("p".to_string(), true),
        ("literal".to_string(), true),
        ("literal".to_string(), true),
        ("a".to_string(), true),
        ("p".to_string(), true),
        ("footer".to_string(), true),
    ];

    //println!("{:#?}\n{:#?}", expects, states);

    assert_eq!(expects, states);
}
