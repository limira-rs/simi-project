use proc_macro2::*;
use Error;

pub struct NodeList {
    pub nodes: Vec<Node>,
}

pub enum Node {
    Element(Box<Element>),
    Component(Component),
    ChildPlaceholder(TokenStream), // A placeholder for component/element that will be passed here from caller
    Expression(Expression),
    Literal(Literal),
    IfElse(IfElse),
    Match(Match),
    For(For),
}

pub struct Element {
    pub no_update: bool,
    pub tag: Ident,
    pub const_attributes: AttributeGroup, // name=#value or name="literal"/true/false
    pub expr_attributes: AttributeGroup,
    pub permanent_classes: Vec<LiteralOrTokenStream>, // class=items: Vec<item>
    pub conditional_classes: Vec<ConditionalClass>, // var_name?=bool_expr, "literal_name"=bool_expr
    pub events: Vec<ElementEvent>,
    pub content: ElementContent,
}

pub struct AttributeGroup {
    pub regulars: Vec<RegularAttribute>,
    pub customs: Vec<CustomAttribute>,
}

pub struct RegularAttribute {
    pub name: Ident,
    pub value: TokenStream,
}

pub struct CustomAttribute {
    pub name: String,
    pub value: TokenStream,
}
#[derive(Debug)]
pub enum LiteralOrTokenStream {
    Literal(Literal),
    Stream(TokenStream),
}

pub struct ConditionalClass {
    pub name: LiteralOrTokenStream,
    pub value: TokenStream,
}

#[derive(PartialEq)]
pub enum EventHandlerType {
    MessageVariant,
    ComponentField,
    ContextMethod,
}

pub struct ElementEvent {
    pub no_update: bool,
    pub handler_type: EventHandlerType,
    pub event: Ident,
    pub with_question_mark: bool,
    pub handler: Vec<TokenTree>,
}

pub enum ElementContent {
    None,
    NodeList(NodeList),
    UbuComponent(TokenStream, TokenStream), // Field, props
    ContextSubApp(TokenStream),
    SimiCreatedSubApp(TokenStream),
    OneOfSimiCreatedSubApps(OneOfSimiCreatedSubApps),
}

pub struct OneOfSimiCreatedSubApps {
    pub match_ident: Ident,
    pub match_expr: TokenStream,
    pub app_arms: Vec<AppArm>,
}

pub struct AppArm {
    pub pattern: TokenStream,
    pub app_type: Ident,
}

pub struct Component {
    pub no_update: bool,
    pub name: Ident,
    pub properties: Option<TokenStream>,
    pub childs: Vec<ComponentChild>,
    pub events: Vec<ComponentEvent>,
    pub expressions: Vec<ComponentExpression>,
    pub literals: Vec<ComponentLiteral>,
}

// Another component as a child or a sub dom tree as a child
pub struct ComponentChild {
    //pub no_update: bool, // no_update is already indicated in self.child
    pub id: TokenTree,
    pub child: ComponentChildInfo,
}

pub enum ComponentChildInfo {
    Component(Component),
    Element(Element),
}

pub struct ComponentEvent {
    pub no_update: bool,
    pub id: TokenTree,
    pub event: ElementEvent,
}

pub struct ComponentExpression {
    pub no_update: bool,
    pub id: TokenTree,
    pub expression: TokenStream,
}

pub struct ComponentLiteral {
    // A literal is always no_update
    pub id: TokenTree,
    pub value: TokenTree,
}

pub struct Expression {
    pub no_update: bool,
    pub expression: TokenStream,
}

pub struct IfElse {
    pub no_update: bool,
    pub arms: Vec<Arm>,
    pub no_last_else: bool,
    pub single_closure_name: Option<Ident>,
}

pub struct Arm {
    pub create_closure_name: Option<Ident>,
    pub update_closure_name: Option<Ident>,
    pub condition: Option<TokenStream>,
    pub nodes: NodeList,
}

pub struct Match {
    pub no_update: bool,
    pub match_ident: Ident,
    pub match_expr: TokenStream,
    pub arms: Vec<Arm>,
    pub single_closure_name: Option<Ident>,
}

pub struct For {
    pub no_update: bool,
    pub iter_expr: TokenStream,
    pub node: Box<Node>,
}

impl NodeList {
    pub fn inspect_no_update(&mut self) -> bool {
        // use `fold` here to make sure inspect no update must run on every node
        let no_update_count = self.nodes.iter_mut().fold(0, |count, n| {
            if n.inspect_no_update() {
                count + 1
            } else {
                count
            }
        });
        no_update_count == self.nodes.len()
    }

    #[cfg(test)]
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        self.nodes
            .iter()
            .for_each(|n| n.collect_no_update_states(states))
    }
}

impl Node {
    fn inspect_no_update(&mut self) -> bool {
        match self {
            Node::Element(item) => item.inspect_no_update(),
            Node::Component(item) => item.no_update,
            Node::ChildPlaceholder(_) => false,
            Node::Expression(item) => item.no_update,
            Node::Literal(_) => true,
            Node::IfElse(item) => item.inspect_no_update(),
            Node::Match(item) => item.inspect_no_update(),
            Node::For(item) => item.inspect_no_update(),
        }
    }

    #[cfg(test)]
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        match self {
            Node::Element(item) => item.collect_no_update_states(states),
            Node::Component(item) => states.push(("compopnent".to_string(), item.no_update)),
            Node::ChildPlaceholder(_) => states.push(("child_placeholder".to_string(), false)),
            Node::Expression(item) => states.push(("expression".to_string(), item.no_update)),
            Node::Literal(_) => states.push(("literal".to_string(), true)),
            Node::IfElse(item) => item.collect_no_update_states(states),
            Node::Match(item) => item.collect_no_update_states(states),
            Node::For(item) => item.collect_no_update_states(states),
        }
    }
}

impl Element {
    pub fn is_valid_regular_attribute(&self, name: &str) -> bool {
        let html_tag = self.tag.to_string();
        match name {
            "value" => match html_tag.as_ref() {
                "select" | "option" | "input" | "textarea" | "button" | "li" | "meter"
                | "progress" | "param" => true,
                _ => false,
            },
            "index" if html_tag == "select" => true,
            "index" => false,
            "checked" => match html_tag.as_ref() {
                "input" => true,
                _ => false,
            },
            _ => true,
        }
    }

    fn inspect_no_update(&mut self) -> bool {
        if self.no_update {
            return true;
        }

        let no_update = self.content.inspect_no_update();
        self.no_update = no_update
            && self.expr_attributes.regulars.is_empty()
            && self.expr_attributes.customs.is_empty()
            && self.conditional_classes.is_empty()
            && self.events.iter().all(|e| e.no_update);
        self.no_update
    }

    #[cfg(test)]
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        self.content.collect_no_update_states(states);
        states.push((self.tag.to_string(), self.no_update));
    }
}

impl AttributeGroup {
    pub fn new() -> Self {
        Self {
            regulars: Vec::new(),
            customs: Vec::new(),
        }
    }
    pub fn check_select_index_value(
        &self,
        config: &super::Config,
        index: &mut Option<Span>,
        value: &mut Option<Span>,
    ) -> bool {
        const WANTED: &str = "a `select` element should only has one of `index` or `value`";
        for ra in self.regulars.iter() {
            if ra.name.to_string().to_ascii_lowercase() == "value" {
                if let Some(index) = index {
                    Error::with_span(ra.name.span(), WANTED)
                        .help_at_span(index.clone(), "`index` is already specified here")
                        .emit(Some(config));
                    return false;
                } else {
                    *value = Some(ra.name.span());
                }
            } else if ra.name.to_string().to_ascii_lowercase() == "index" {
                if let Some(value) = value {
                    Error::with_span(ra.name.span(), WANTED)
                        .help_at_span(value.clone(), "`value` is already specified here")
                        .emit(Some(config));
                    return false;
                } else {
                    *index = Some(ra.name.span());
                }
            }
        }
        true
    }
}

impl CustomAttribute {
    pub fn new(name: TokenStream, value: TokenStream) -> Self {
        // FIXME: Better handling for this?
        Self {
            name: name.into_iter().map(|t| t.to_string()).collect::<String>(),
            value,
        }
    }
}

impl ElementContent {
    fn inspect_no_update(&mut self) -> bool {
        match self {
            ElementContent::NodeList(item) => item.inspect_no_update(),
            ElementContent::OneOfSimiCreatedSubApps(_) => false,
            ElementContent::None
            | ElementContent::ContextSubApp(_)
            | ElementContent::SimiCreatedSubApp(_)
            | ElementContent::UbuComponent(_, _) => true,
        }
    }

    #[cfg(test)]
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        if let ElementContent::NodeList(item) = self {
            item.collect_no_update_states(states);
        }
    }
}

impl Component {
    pub fn component_fields(&self) -> TokenStream {
        let mut component = TokenStream::new();
        component.extend(self.events.iter().map(|e| {
            let id = &e.id;
            quote!{ #id, }
        }));
        component.extend(self.expressions.iter().map(|e| {
            let id = &e.id;
            quote!{ #id, }
        }));
        component.extend(self.literals.iter().map(|e| {
            let id = &e.id;
            quote!{ #id, }
        }));
        component.extend(self.childs.iter().map(|e| {
            let id = &e.id;
            quote!{ #id, }
        }));
        component
    }
}

impl IfElse {
    fn inspect_no_update(&mut self) -> bool {
        if self.no_update {
            return true;
        }
        // Run inspect no update for each arm
        self.arms.iter_mut().for_each(|a| {
            a.nodes.inspect_no_update();
        });
        // But the `IfElse` must alway need update unless user prefix it with `#`
        self.no_update
    }

    #[cfg(test)]
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        self.arms
            .iter()
            .for_each(|a| a.nodes.collect_no_update_states(states));
        states.push(("IfElse".to_string(), self.no_update));
    }
}

impl Match {
    fn inspect_no_update(&mut self) -> bool {
        if self.no_update {
            return true;
        }
        // Run inspect no update for each arm
        self.arms.iter_mut().for_each(|a| {
            a.nodes.inspect_no_update();
        });
        // But the `Match` must alway need update unless user prefix it with `#`
        self.no_update
    }

    #[cfg(test)]
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        self.arms
            .iter()
            .for_each(|a| a.nodes.collect_no_update_states(states));
        states.push(("Match".to_string(), self.no_update));
    }
}

impl For {
    fn inspect_no_update(&mut self) -> bool {
        if self.no_update {
            return true;
        }
        self.node.inspect_no_update();
        // But the `For` must alway need update unless user prefix it with `#`
        self.no_update
    }

    #[cfg(test)]
    pub fn collect_no_update_states(&self, states: &mut Vec<(String, bool)>) {
        self.node.collect_no_update_states(states);
        states.push(("For".to_string(), self.no_update));
    }
}
