use super::raw_dom::*;
use super::{Config, CurrentParents};
use proc_macro2::*;
use std::iter::FromIterator;

use helper::{self, build_stream};

impl NodeList {
    /// This method will both: render new node and update current node
    pub(crate) fn update(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let vec: Vec<TokenStream> = self
            .nodes
            .iter()
            .enumerate()
            .map(|(index, node)| {
                let node_list = parents.node_list();
                let current_node = quote!{ current_simi_node_for_updating_ };
                let update = node.update(config, &current_node, parents);
                quote!{
                    if let Some(ref mut #current_node) = #node_list.get_mut(#index) {
                        #update
                    }
                }
            })
            .collect();
        build_stream::from_streams(vec)
    }
}

impl Node {
    fn update(
        &self,
        config: &Config,
        current_node: &TokenStream,
        parents: &CurrentParents,
    ) -> TokenStream {
        match self {
            Node::Element(e) => e.update(config, current_node),
            Node::Component(s) => s.update(config, current_node),
            Node::ChildPlaceholder(_) => {
                quote!{ #current_node.set_need_rerender(true); }
            }
            Node::Expression(x) => {
                // This is from an expression and will be render as a text node.
                // `application!/component!` is not designed to use outside of an app's render method or
                // a component's render method.

                // The result of the execution of this expression may change over time
                // so we must update this.
                if x.no_update {
                    return TokenStream::new();
                }
                let expression = &x.expression;
                if config.need_unwrap(&x.expression) {
                    quote!{
                        if let Some(ref expression) = #expression {
                            #current_node.update_text(expression.to_string());
                        }
                    }
                } else {
                    quote! { #current_node.update_text(#expression.to_string()); }
                }
            }
            Node::Literal(_) => {
                // We are in update mode.
                // We ignore this while updating. Because it is a literal with the same index and
                // it is not in a conditinal arm.
                // It will not be changed???
                TokenStream::new()
            }
            Node::IfElse(ifelse) => ifelse.update_by_single_closure(config, current_node, parents),
            Node::Match(m) => m.update_by_single_closure(config, current_node, parents),
            Node::For(f) => f.update(config, current_node, parents),
        }
    }
}

impl Element {
    fn update(&self, config: &Config, current_node: &TokenStream) -> TokenStream {
        if self.no_update {
            return TokenStream::new();
        }
        let tag = self.tag.to_string();
        let first_uppercase_tag = Ident::new(&helper::to_first_uppercase(&tag), Span::call_site());
        let html_tag_path = config.html_tag_path.clone();

        let simi_dom = &config.simi_dom_path;
        let simi_element = quote!{a_reference_to_a_simi_element_in_node_list_};
        let real_element = quote!{a_reference_to_a_real_element_in_effect_};
        let simi_element_nodes = quote!{
            #simi_element.nodes_mut()
        };

        // When updating: Simi ignores:
        //      const_expressions
        //      permanent_classes

        let update_events = self.update_events(config, &simi_element);

        let expr_attributes =
            self.expr_attributes
                .update(config, &tag, &simi_element, &real_element);
        let conditional_classes =
            self.update_conditional_classes(config, &simi_element, &real_element);

        let post_update = self
            .expr_attributes
            .regulars
            .iter()
            .map(|ra| ra.post_update(config, &tag, &simi_element, &real_element))
            .collect();
        let post_update = build_stream::from_streams(post_update);

        let sub_parents = CurrentParents::new_sub(simi_element_nodes.clone(), real_element.clone());
        let update_childs = self.content.update(config, &sub_parents);

        let simi_node_variant = match tag.as_str() {
            "select" => quote!{Select},
            "input" => quote!{Input},
            "textarea" => quote!{TextArea},
            _ => quote!{Element},
        };
        let log = &config.log_error;
        let rs = quote!{
            match #current_node {
                #simi_dom::Node::#simi_node_variant(ref mut #simi_element) => {
                    debug_assert_eq!(#html_tag_path::#first_uppercase_tag, #simi_element.etag());
                    // How to avoid cloning the real element here?
                    let #real_element = #simi_element.get_real_element().clone();
                    let #real_element = &#real_element;
                    #update_events
                    #expr_attributes
                    #conditional_classes
                    #update_childs
                    #post_update
                }
                _ => {
                    #log("`Simi` bug: Expected an Node::Element")
                }
            }
        };
        rs
    }

    fn update_events(&self, config: &Config, simi_node: &TokenStream) -> TokenStream {
        if self.events.is_empty() {
            return TokenStream::new();
        }
        let events = quote!{events};
        let all_events:Vec<TokenStream> = self
            .events
            .iter()
            .map(|e| {
                if e.no_update {
                    return TokenStream::new();
                }
                match e.handler_type {
                    EventHandlerType::MessageVariant | EventHandlerType::ContextMethod => {
                        let event = e.create_element_event(config);
                        quote!{ #events.push(Box::new(#event));}
                    },
                    EventHandlerType::ComponentField=> {
                        // We are in a component, and the event is passed here from the caller
                        let handler = TokenStream::from_iter(e.handler.clone());
                        quote! { #events.push(#handler.take().expect("`Simi` bug: Event handler is None. Expect: it must be available in create phase")); }
                    }
                }
            })
            .collect();
        let main = config.main_clone();
        quote!{
            {
                use simi::element_events::Events;
                let mut #events = Events::new();
                #(#all_events)*
                #simi_node.update_real_events(&#main, #events);
            }
        }
    }

    fn update_conditional_classes(
        &self,
        config: &Config,
        simi_element: &TokenStream,
        real_element: &TokenStream,
    ) -> TokenStream {
        if self.conditional_classes.is_empty() {
            return TokenStream::new();
        }
        let class_list = quote!{class_list};
        let all = self
            .conditional_classes
            .iter()
            .map(|cc| {
                let ConditionalClass { name, value } = cc;
                match name {
                    LiteralOrTokenStream::Literal(lit) => {
                        if config.need_unwrap(value) {
                            quote!{
                                if let Some(ref value) = #value {
                                    #simi_element.update_class(#lit, *value, &#class_list);
                                }
                            }
                        }else{
                            quote!{ #simi_element.update_class(#lit, #value, &#class_list); }
                        }
                    }
                    LiteralOrTokenStream::Stream(s) => {
                        match (config.need_unwrap(s), config.need_unwrap(value)) {
                            (true, true) => quote!{
                                if let (Some(ref class_name), Some(ref value)) = (#s, #value) {
                                    #simi_element.update_expr_class(&class_name.to_string(), *value, &#class_list);
                                }
                            },
                            (true, false) => quote!{
                                if let Some(ref class_name) = #s{
                                    #simi_element.update_expr_class(&class_name.to_string(), #value, &#class_list);
                                }
                            },
                            (false, true) => quote!{
                                if let Some(ref value) = #value {
                                    #simi_element.update_expr_class(&#s.to_string(), *value, &#class_list);
                                }
                            },
                            (false,false) => quote!{ #simi_element.update_expr_class(&#s.to_string(), #value, &#class_list); }
                        }
                    }
                }
            })
            .collect();
        let all = helper::build_stream::from_streams(all);
        quote!{{
            let #class_list = #real_element.class_list();
            #all
        }}
    }

    fn update_as_child_component(
        &self,
        config: &Config,
        parent_component: &TokenStream,
        index: usize,
    ) -> TokenStream {
        let simi_component = quote!{a_reference_to_a_simi_component_in_node_list_};
        let simi_component_nodes = quote!{mutable_reference_to_simi_component_nodes_};
        // Provide an empty TokenStream here does not affect the update self.process on next line
        // because it does not use the real_parent field
        let current_node = quote!{a_child_component_created_from_element_};
        let update = self.update(config, &current_node);
        let log = &config.log_error;
        quote! {
            if let Some(#simi_component) = #parent_component.childs().get(#index) {
                if #simi_component.borrow().need_rerender() {
                    let mut #simi_component = #simi_component.borrow_mut();
                    let #simi_component_nodes = #simi_component.nodes_mut();
                    if let Some(#current_node) = #simi_component_nodes.get_mut(0){
                        #update
                    }
                }
            }else{
                #log("No item at the given index");
            }
        }
    }
}

impl AttributeGroup {
    fn update(
        &self,
        config: &Config,
        html_tag: &str,
        simi_element: &TokenStream,
        real_element: &TokenStream,
    ) -> TokenStream {
        let regulars = self
            .regulars
            .iter()
            .map(|a| a.update(config, html_tag, simi_element, real_element))
            .collect();
        let customs = self
            .customs
            .iter()
            .map(|a| a.update(config, simi_element, real_element))
            .collect();
        let regulars = build_stream::from_streams(regulars);
        let customs = build_stream::from_streams(customs);
        quote!{
            #regulars
            #customs
        }
    }
}

impl RegularAttribute {
    fn update(
        &self,
        config: &Config,
        html_tag: &str,
        simi_element: &TokenStream,
        real_element: &TokenStream,
    ) -> TokenStream {
        let name = self.name.to_string();
        let value = &self.value;
        if config.need_unwrap(value) {
            let update = match name.as_str() {
                // value for select element will be in post_update
                "value" => match html_tag {
                    "select" => return TokenStream::new(),
                    "input" | "textarea" => quote!{ #simi_element.more.update_value(value, &#real_element); },
                    _=> quote!{ #simi_element.update_attribute(#name, value.to_string(), &#real_element); }
                },
                // index for select element will be in post_update
                "index" =>return TokenStream::new(),
                "checked" => quote!{ #simi_element.more.update_checked(*value, &#real_element); },
                _ => quote!{ #simi_element.update_attribute(#name, value.to_string(), &#real_element); }
            };
            quote!{
                if let Some(ref value) = #value{
                    #update
                }
            }
        } else {
            match name.as_str() {
                // value for select element will be in post_update
                "value" => match html_tag {
                    "select" => TokenStream::new(),
                    "input" | "textarea" => quote!{#simi_element.more.update_value(&#value, &#real_element);},
                    _=> quote!{ #simi_element.update_attribute(#name, #value.to_string(), &#real_element); }
                },
                // index for select element will be in post_update
                "index" => TokenStream::new(),
                "checked" => quote!{#simi_element.more.update_checked(#value, &#real_element);},
                _ => quote!{ #simi_element.update_attribute(#name, #value.to_string(), &#real_element); }
            }
        }
    }

    // This is apply for select element
    fn post_update(
        &self,
        config: &Config,
        html_tag: &str,
        simi_element: &TokenStream,
        real_element: &TokenStream,
    ) -> TokenStream {
        if html_tag != "select" {
            return TokenStream::new();
        }
        let name = self.name.to_string();
        let value = &self.value;
        if config.need_unwrap(value) {
            match name.as_str() {
                "value" => quote!{
                    if let Some(ref value) = #value {
                        #simi_element.more.update_value(value, &#real_element);
                    }
                },
                "index" => quote!{
                    if let Some(ref index)=#value{
                        #simi_element.more.update_index(index, &#real_element);
                    }
                },
                _ => TokenStream::new(),
            }
        } else {
            match name.as_str() {
                "value" => quote!{ #simi_element.more.update_value(&#value, &#real_element); },
                "index" => quote!{ #simi_element.more.update_index(&#value, &#real_element);},
                _ => TokenStream::new(),
            }
        }
    }
}

impl CustomAttribute {
    fn update(
        &self,
        config: &Config,
        simi_element: &TokenStream,
        real_element: &TokenStream,
    ) -> TokenStream {
        let name = &self.name;
        let value = &self.value;
        if config.need_unwrap(value) {
            quote!{
                if let Some(ref value) = #value{
                    #simi_element.update_attribute(#name, value.to_string(), &#real_element);
                }
            }
        } else {
            quote!{
                #simi_element.update_attribute(#name, #value.to_string(), &#real_element);
            }
        }
    }
}

impl ElementContent {
    fn update(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        match self {
            ElementContent::NodeList(list) => list.update(config, parents),
            ElementContent::OneOfSimiCreatedSubApps(oneof) => oneof.update(config, parents),
            // Component, SubApp will be update by user calls
            _ => TokenStream::new(),
        }
    }
}

impl Component {
    fn update(&self, config: &Config, current_node: &TokenStream) -> TokenStream {
        if self.no_update {
            return TokenStream::new();
        }
        let main = &config.main_clone();
        let simi_dom = &config.simi_dom_path;
        let props = self
            .properties
            .as_ref()
            .map_or(quote!(()), |props| props.clone());

        let init_component_fields = self.init_component_fields_for_update(config);
        let component_fields = self.component_fields();

        let simi_component = quote!{a_reference_to_a_simi_component_in_node_list_};
        let update_requested_child = self.update_requested_child(config, &simi_component);

        let user_component_name = &self.name;
        let user_component = quote!{a_temporary_instance_of_a_user_component_};
        let context = quote!{a_context_for_rendering_user_component_};
        let log = &config.log_error;
        quote!{
            match #current_node {
                #simi_dom::Node::Component(ref mut #simi_component) => {
                    {
                        let (nodes_mut, real_parent) = #simi_component.nodes_mut_real_parent();
                        let #context = CompRenderContext::new(nodes_mut, real_parent, #main, None);
                        let #user_component = {
                            #init_component_fields
                            #user_component_name {
                                #component_fields
                            }
                        };
                        #user_component.render(&#props, #context);
                    }
                    #update_requested_child
                }
                _ => {
                    #log("`Simi` bug: Expected an Node::Component")
                }
            }
        }
    }

    fn init_component_fields_for_update(&self, config: &Config) -> TokenStream {
        let mut init_component_fields = TokenStream::new();
        init_component_fields.extend(
            self.events
                .iter()
                .map(|e| e.init_component_field_for_update(config)),
        );
        init_component_fields.extend(
            self.expressions
                .iter()
                .map(|e| e.init_component_field_for_update(config)),
        );
        init_component_fields.extend(
            self.literals
                .iter()
                .map(|l| l.init_component_field_for_update(config)),
        );
        init_component_fields.extend(
            self.childs
                .iter()
                .map(|c| c.init_component_field_for_update(config)),
        );
        init_component_fields
    }

    fn update_requested_child(
        &self,
        config: &Config,
        parent_component: &TokenStream,
    ) -> TokenStream {
        let update_child_components = self
            .childs
            .iter()
            .enumerate()
            .map(|(index, child)| child.update(config, parent_component, index))
            .collect();
        build_stream::from_streams(update_child_components)
    }

    fn update_child_component(
        &self,
        config: &Config,
        parent_component: &TokenStream,
        index: usize,
    ) -> TokenStream {
        let main = &config.main_clone();
        let props = self
            .properties
            .as_ref()
            .map_or(quote!(()), |props| props.clone());

        let init_component_fields = self.init_component_fields_for_update(config);
        let component_fields = self.component_fields();

        let simi_component = quote!{a_reference_to_a_simi_component_in_node_list_};
        let update_requested_child = self.update_requested_child(config, &simi_component);

        let user_component_name = &self.name;
        let user_component = quote!{a_temporary_instance_of_a_user_component_};
        let context = quote!{a_context_for_rendering_user_component_};
        let log = &config.log_error;
        quote!{
            if let Some(#simi_component) = #parent_component.childs().get(#index) {
                if #simi_component.borrow().need_rerender(){
                    let mut #simi_component = #simi_component.borrow_mut();
                    {
                        let (nodes_mut, real_parent) = #simi_component.nodes_mut_real_parent();
                        let #context = CompRenderContext::new(nodes_mut, real_parent, #main, None);
                        let #user_component = {
                            #init_component_fields
                            #user_component_name {
                                #component_fields
                            }
                        };
                        #user_component.render(&#props, #context);
                    }
                    #update_requested_child
                }
            }else{
                #log("No item at the given index");
            }
        }
    }
}

impl ComponentEvent {
    fn init_component_field_for_update(&self, config: &Config) -> TokenStream {
        let ee = &config.simi_events_path;
        let id = &self.id;
        if self.no_update {
            quote!{
                let #id: ::std::cell::Cell<Option<Box<#ee::SimiElementEvent<_>>>>  = ::std::cell::Cell::new(None);
            }
        } else {
            let event = self.event.create_element_event(config);
            quote!{
                let #id: ::std::cell::Cell<Option<Box<#ee::SimiElementEvent<_>>>>  = ::std::cell::Cell::new(Some(Box::new(#event)));
            }
        }
    }
}

impl ComponentExpression {
    fn init_component_field_for_update(&self, _config: &Config) -> TokenStream {
        let ComponentExpression {
            id,
            no_update,
            expression,
        } = self;
        if *no_update {
            quote!{
                let #id = None;
            }
        } else {
            // It is not mutable, hence, when update, it should be None => no update
            quote!{
                let #id = Some(#expression);
            }
        }
    }
}

impl ComponentLiteral {
    fn init_component_field_for_update(&self, _config: &Config) -> TokenStream {
        // Never update a literal
        let id = &self.id;
        quote!{
            let #id = None;
        }
    }
}

impl ComponentChild {
    fn init_component_field_for_update(&self, _config: &Config) -> TokenStream {
        let id = &self.id;
        quote!{
            let #id = ::std::cell::Cell::new(None);
        }
    }

    fn update(&self, config: &Config, parent_component: &TokenStream, index: usize) -> TokenStream {
        match self.child {
            ComponentChildInfo::Component(ref component) => {
                component.update_child_component(config, parent_component, index)
            }
            ComponentChildInfo::Element(ref element) => {
                element.update_as_child_component(config, parent_component, index)
            }
        }
    }
}

impl IfElse {
    fn update_by_single_closure(
        &self,
        config: &Config,
        current_node: &TokenStream,
        parents: &CurrentParents,
    ) -> TokenStream {
        if self.no_update {
            return TokenStream::new();
        }

        let simi_dom = &config.simi_dom_path;
        let simi_dom_arm = quote!{the_active_arm_of_an_if_else_construct_};
        let real_parent = parents.real_parent();

        let closure_name = self
            .single_closure_name
            .as_ref()
            .expect("Simi bug: Closure must available before generating code");
        let log = &config.log_error;
        let context = &config.context;
        quote! {
            match #current_node {
                #simi_dom::Node::Arm(ref mut #simi_dom_arm) => {
                    #closure_name(#simi_dom_arm, #real_parent, &#context);
                }
                _ => {
                    #log("`Simi` bug: Expected an Node::Arm")
                }
            }
        }
    }
}

impl Match {
    fn update_by_single_closure(
        &self,
        config: &Config,
        current_node: &TokenStream,
        parents: &CurrentParents,
    ) -> TokenStream {
        if self.no_update {
            return TokenStream::new();
        }

        let simi_dom = &config.simi_dom_path;
        let simi_dom_arm = quote!{the_active_arm_of_an_if_else_construct_};
        let real_parent = parents.real_parent();

        let closure_name = self
            .single_closure_name
            .as_ref()
            .expect("Simi bug: Closure must available before generating code");
        let log = &config.log_error;
        let context = &config.context;
        quote! {
            match #current_node {
                #simi_dom::Node::Arm(ref mut #simi_dom_arm) => {
                    #closure_name(#simi_dom_arm, #real_parent, &#context);
                }
                _ => {
                    #log("`Simi` bug: Expected an Node::Arm")
                }
            }
        }
    }
}

impl For {
    fn update(
        &self,
        config: &Config,
        current_node: &TokenStream,
        parents: &CurrentParents,
    ) -> TokenStream {
        let simi_dom = &config.simi_dom_path;
        let simi_for_loop = quote!{ a_reference_to_a_for_loop_ };

        let real_parent = parents.real_parent();
        let local_parent = quote!{local_name_for_outer_parent_for_a_for_loop_};
        let create_sub_parents =
            CurrentParents::new_root(simi_for_loop.clone(), local_parent.clone());
        let create = self.node.create(config, &create_sub_parents);

        let sub_current_node = quote!{a_simi_for_loop_node_};
        let no_simi_parent = quote!{none_exists_simi_parent_node_list_};
        let update_sub_parents = parents.new_node_list_same_real_parent(no_simi_parent);
        let update = self
            .node
            .update(config, &sub_current_node, &update_sub_parents);

        let iter_expr = &self.iter_expr;
        let log = &config.log_error;
        quote!{
            match #current_node {
                #simi_dom::Node::NoneKeyedFor(ref mut #simi_for_loop) => {
                    let #local_parent = #real_parent;
                    let mut current_simi_for_loop_index = 0;
                    for #iter_expr {
                        if current_simi_for_loop_index < #simi_for_loop.count() {
                            // The index is already checked by this `if`,
                            // We do not need to check it again, so use get_unchecked_mut here
                            let #sub_current_node = unsafe { #simi_for_loop.nodes_mut().get_unchecked_mut(current_simi_for_loop_index) };
                            #update
                        }else{
                            #create
                        }
                        current_simi_for_loop_index += 1;
                    }
                    if current_simi_for_loop_index < #simi_for_loop.count(){
                        #simi_for_loop.truncate(current_simi_for_loop_index, #real_parent.as_ref());
                    }
                }
                _ => {
                    #log("`Simi` bug: Expected an Node::NoneKeyedFor")
                }
            }
        }
    }
}

impl OneOfSimiCreatedSubApps {
    fn update(&self, config: &Config, parents: &CurrentParents) -> TokenStream {
        let simi_dom = &config.simi_dom_path;
        let node_list = parents.node_list();
        let current_node = quote!{ current_simi_node_for_updating_ };
        let one_of = quote! { one_of_simi_created_application_node_ };

        let match_ident = &self.match_ident;
        let match_expr = &self.match_expr;

        let arms = self.app_arms.iter().map(|arm| arm.update(&one_of, parents));
        let log = &config.log_error;
        quote!{
            if let Some(ref mut #current_node) = #node_list.get_mut(0) {
                match #current_node {
                    #simi_dom::Node::SimiCreatedSubApp(#one_of) => {
                        #match_ident #match_expr {
                            #(#arms)*
                        }
                    }
                    _ => {
                        #log("`Simi` bug: Expected an Node::SimiCreatedSubApp")
                    }
                }
            }
        }
    }
}

impl AppArm {
    fn update(&self, one_of: &TokenStream, parents: &CurrentParents) -> TokenStream {
        let pattern = &self.pattern;
        let app_type = &self.app_type;
        let real_parent = parents.real_parent();
        quote!{
            #pattern => {
                if !#one_of.is_app_type::<#app_type>() {
                    #one_of.new_app::<#app_type>(#real_parent);
                }
            }
        }
    }
}
