This is the repo of Simi 0.1.x, and it is discontinued because the code-quality is too low.
And a dramatical change in internal design make it extremely difficult to modify the code.
Instead, I did a rewrite from Scratch.

Simi v0.2 is now at: [https://gitlab.com/limira-rs/simi](https://gitlab.com/limira-rs/simi)