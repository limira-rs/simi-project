use std::env;
use std::fs::File;
use std::io::Write;

const TAGS: [&'static str; 106] = [
    // Content sectioning
    "address",
    "article",
    "aside",
    "footer",
    "header",
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "hgroup",
    "nav",
    "section",
    // Text content
    "blockquote",
    "dd",
    "div",
    "dl",
    "dt",
    "figcaption",
    "figure",
    "hr",
    "li",
    "main",
    "ol",
    "p",
    "pre",
    "ul",
    // Inline text semantics
    "a",
    "abbr",
    "b",
    "bdi",
    "bdo",
    "br",
    "cite",
    "code",
    "data",
    "dfn",
    "em",
    "i",
    "kbd",
    "mark",
    "q",
    "rp",
    "rt",
    "rtc",
    "ruby",
    "s",
    "samp",
    "small",
    "span",
    "strong",
    "sub",
    "sup",
    "time",
    "u",
    "var",
    "wbr",
    // Image and multimedia
    "area",
    "audio",
    "img",
    "map",
    "track",
    "video",
    // Embedded content
    "embed",
    "iframe",
    "object",
    "param",
    "picture",
    "source",
    // Scripting
    "canvas",
    "noscript",
    "script",
    // Demarcating edits
    "del",
    "ins",
    // Table content
    "caption",
    "col",
    "colgroup",
    "table",
    "tbody",
    "td",
    "tfoot",
    "th",
    "thead",
    "tr",
    // Forms
    "button",
    "datalist",
    "fieldset",
    "form",
    "input",
    "label",
    "legend",
    "meter",
    "optgroup",
    "option",
    "output",
    "progress",
    "select",
    "textarea",
    // Interactive elements
    "details",
    "dialog",
    "menu",
    "menuitem",
    "summary",
    // Web Components
    "slot",
    "template",
];

const HTML_TAG_START: &'static str = r#"
/// html tags Supported by `simi`
#[derive(Debug,Clone,Copy,PartialEq)]
pub enum HtmlTag {"#;

fn main() {
    println!("cargo:rerun-if-changed=build.rs");

    let dest = env::var("OUT_DIR").expect("Unable to get current dir");
    let dest = ::std::path::Path::new(&dest).join("html_tags.rs");
    let mut out = File::create(dest).expect("Unable to create html_tags.rs");

    // Generate tags
    generate(
        &mut out,
        "pub fn tags()->fnv::FnvHashSet<&'static str>{\n let mut rs = fnv::FnvHashSet::default();",
        "\trs\n}",
        &TAGS,
        generate_hash_set_insert,
    );

    // Generate pub enum HtmlTag {}
    generate(&mut out, HTML_TAG_START, "}", &TAGS, generate_html_tag_enum);
}

fn generate_hash_set_insert(out: &mut File, line: &str) {
    writeln!(out, "\trs.insert(\"{}\");", line).expect("Unable to create html_tags.rs");
}

fn generate_html_tag_enum(out: &mut File, line: &str) {
    let (first, remaining) = line.split_at(1);
    writeln!(out, "\t{}{},", first.to_uppercase(), remaining)
        .expect("Unable to create html_tags.rs");
}

fn generate(
    out: &mut File,
    start: &str,
    end: &str,
    lines: &[&str],
    generator: fn(&mut std::fs::File, &str),
) {
    writeln!(out, "{}", start).expect("Unable to create html_tags.rs");
    lines.iter().for_each(|line| {
        if line.starts_with("//") {
            writeln!(out, "\t{}", line).expect("Unable to create html_tags.rs");
        } else {
            generator(out, line);
        }
    });
    writeln!(out, "{}", end).expect("Unable to create html_tags.rs");
}
