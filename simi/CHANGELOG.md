# `Simi` Change Log

---
## 0.1.4
Released 2018-??-??
Actually, this verion is a breaking change. But I do not want to bump to 0.2 because there is no real major change in Simi.

**Added**

`context_callback!` to create closure for JS to callback into the app context
`closure::with_serde_arg` to create closure for JS to send arbitrary data that can be parse into Rust type using serde

**Changed**
**Breaking** change `UbuComponent::new` now return `UbuComponent` instead of `Result<UbuComponent, SimiError>`

**Fixed**
**Breaking** change because of the new clippy


## 0.1.3
Released 2018-10-22.

### Added

* `context_sub_application`, `context_ubu_component`, `sub_application`, `one_of_sub_applications`
* `simi::fetch::fetch_json` via features `fetch` + `fetch_json`
* `onclick=context:context_method`

### Changed

* SubApp => SimiSubApp

### Deprecated

* `<html_tag> [...]` for ubu-component and sub app is deprecated, replaced by `context_sub_application` and `context_ubu_component`

### Removed

* `interop::console` has been removed, use `console` in `web-sys` (from wasm-bindgen project) instead

## v0.1.2
Released 2018-10-08.

### Fixed

* Rust nightly `#![feature(proc_macro_hygiene)]`

## v0.1.1
Released 2018-10-02.

### Fixed

* Correct version for simi-html-tags in simi-macros

## v0.1.0

Released 2018-10-01.

### Added

* Simi
    * Support for enum as app state
    * Todo example
    * No alias for element events

### Fixed

* Simi
    * Fixed `#[simi_app(element-id)]`
