#![feature(proc_macro_hygiene)]
extern crate js_sys as js;
extern crate simi;
extern crate wasm_bindgen;

use simi::interop::real_dom::window;
use simi::prelude::*;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

#[derive(Clone)]
struct ClockComponent;

impl<A: SimiApp> Component<A> for ClockComponent {
    type Properties = i32;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        component! {
            fieldset {
                legend {"component: " b { "Clock" } }
                div { "Current value: " props }
            }
        }
    }
}

enum Msg {
    Up,
    SomeRandValue,
}

struct Context {
    clock: UbuComponent<ClockApp, ClockComponent>,
    _closure: Closure<Fn()>,
}

impl Context {
    fn new(main: WeakMain<ClockApp>) -> Self {
        let clock = UbuComponent::new(main.clone(), ClockComponent);
        let closure = simi::interop::closure::no_arg(main.clone(), || Msg::Up);
        match window() {
            Some(window) => {
                let rs = window.set_interval_with_callback_and_timeout_and_arguments_0(
                    closure.as_ref().unchecked_ref(),
                    1000,
                );
                simi::error::log_result_error(rs);
            }
            None => simi::error::log_error_message("No window?"),
        }
        Self {
            clock,
            _closure: closure,
        }
    }
}

#[simi_app]
struct ClockApp {
    count: i32,
}

impl SimiApp for ClockApp {
    type Message = Msg;
    type Context = Context;

    fn init(main: WeakMain<Self>) -> (Self, Self::Context) {
        (Self { count: 2018 }, Context::new(main))
    }

    fn update(&mut self, m: Self::Message, context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::Up => {
                self.count += 1;
                context.clock.update(&self.count);
                false
            }
            Msg::SomeRandValue => {
                self.count = (js::Math::random() * 2018_f64) as i32;
                context.clock.update(&self.count);
                true
            }
        }
    }

    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            //@debug
            h2 { "Clock" }
            p { "inside an " b {"update-by-user component"} }
            hr
            p { "The initial value when the app start is " #self.count " (this value will never change)" }
            p { "The initial value of the current count is " self.count " (if you modify the " code{"update"} " method to return " code{"true"} " for " code{ "Msg::Up"}" then this value will also be updated every second)" }
            context_ubu_component {
                container: div ()
                context_field: clock
                props: &self.count
            }
            button (onclick=#Msg::SomeRandValue) { "Restart at a random value" }
        }
    }
}
