#![feature(proc_macro_hygiene)]
extern crate simi;
extern crate wasm_bindgen;

use simi::prelude::*;
use wasm_bindgen::prelude::*;

enum Msg {
    Up,
    Down,
}

#[simi_app]
struct Counter {
    count: i32,
}

impl SimiApp for Counter {
    type Message = Msg;
    type Context = ();
    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (Self { count: 2018 }, ())
    }
    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::Up => self.count += 1,
            Msg::Down => self.count -= 1,
        }
        true
    }
    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            //@debug
            h2 { "Counter" }
            p { "the " b {"simplest"} " version" }
            hr
            div { "Current value: " self.count }
            button (onclick=#Msg::Down) { "Down" }
            button (onclick=#Msg::Up) { "Up" }
        }
    }
}
