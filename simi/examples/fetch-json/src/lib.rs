#![feature(proc_macro_hygiene)]
extern crate serde;
extern crate simi;
extern crate wasm_bindgen;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate log;
extern crate wasm_logger;

use simi::fetch::*;
use simi::prelude::*;
use wasm_bindgen::prelude::*;

#[derive(Deserialize)]
struct Commit {
    id: String,
    short_id: String,
    title: String,
    created_at: String,
    //parent_ids: Vec<String>,
    message: String,
    author_name: String,
    author_email: String,
    authored_date: String,
    //committer_email: String,
    //committer_name: String,
    //committed_date: String,
}

#[derive(Deserialize)]
struct Branch {
    name: String,
    //merged: bool,
    //protected: bool,
    //developers_can_push: bool,
    //developers_can_merge: bool,
    //can_push: bool,
    //default: bool,
    commit: Commit,
}

enum Msg {
    BranchInfoLoaded(Result<Branch, SimiError>),
}

pub struct Context {
    main: WeakMain<AppState>,
    _fetcher: Option<Promise>,
}

impl Context {
    fn get_simi_master_branch_info(&mut self) {
        let url = "https://gitlab.com/api/v4/projects/7493299/repository/branches/master";
        let cb = ::simi::callback::with_arg(self.main.clone(), Msg::BranchInfoLoaded);
        let f = FetchBuilder::new(url);
        match f.fetch_json(cb) {
            Ok(p) => self._fetcher = Some(p),
            Err(e) => error!("Error on start fetching {}: {}", url, e),
        }
        info!("Sent fetch for simi-project's master branch info");
    }
}

#[simi_app]
struct AppState {
    error_string: Option<String>,
    master_branch_info: Option<Branch>,
}

impl SimiApp for AppState {
    type Message = Msg;
    type Context = Context;

    fn init(main: WeakMain<Self>) -> (Self, Context) {
        wasm_logger::init(wasm_logger::Config::new(log::Level::Info));
        (
            Self {
                error_string: None,
                master_branch_info: None,
            },
            Context {
                main,
                _fetcher: None,
            },
        )
    }

    fn update(&mut self, m: Self::Message, _c: &mut Self::Context) -> UpdateView {
        match m {
            Msg::BranchInfoLoaded(rs) => {
                info!("simi-project's master branch info is now available");
                match rs {
                    Ok(rs) => {
                        self.master_branch_info = Some(rs);
                        self.error_string = None;
                    }
                    Err(e) => self.error_string = Some(e.into()),
                }
            }
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            //@debug
            h2 { "Simi latest commit info" }
            if let Some(ref bi) = self.master_branch_info {
                table {
                    tr {
                        th { "Branch" }
                        td { bi.name }
                    }
                    tr {
                        th { "Commit id" }
                        td { bi.commit.id }
                    }
                    tr {
                        th { "Commit short id" }
                        td { bi.commit.short_id }
                    }
                    tr {
                        th { "Title" }
                        td { bi.commit.title }
                    }
                    tr {
                        th { "Created at" }
                        td { bi.commit.created_at }
                    }
                    tr {
                        th { "Message" }
                        td { bi.commit.message }
                    }
                    tr {
                        th { "Author name" }
                        td { bi.commit.author_name }
                    }
                    tr {
                        th { "Author email" }
                        td { bi.commit.author_email }
                    }
                    tr {
                        th { "Authored data" }
                        td { bi.commit.authored_date }
                    }
                }
            } else {
                p { "No information yet, click `Fetch` to fetch the information from gitlab.com" }
                p { "Please wait for a few moments after clicking" }
                button (onclick=app_context:get_simi_master_branch_info()) { "Fetch" }
                p { self.error_string.as_ref().unwrap_or(&"".to_string()) }
            }
        }
    }
}
