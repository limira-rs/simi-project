Note that you must enable features `fetch` and `fetch_json` in `simi`

```toml
simi = { version="0.1", features=["fetch","fetch_json"] }
```