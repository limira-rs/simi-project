#![feature(proc_macro_hygiene)]
extern crate simi;
extern crate wasm_bindgen;

use simi::prelude::*;
use wasm_bindgen::prelude::*;

struct Counter<A: SimiApp> {
    title: Option<&'static str>,
    up: simi::element_events::ElementEvent<A>,
    down: simi::element_events::ElementEvent<A>,
}

impl<A: SimiApp> Component<A> for Counter<A> {
    type Properties = i32;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        component! {
            //@debug
            div { #self.title props }
            button (onclick=#self.down) { "Down" }
            button (onclick=#self.up) { "Up" }
        }
    }
}

enum Msg {
    Value1Up,
    Value1Down,
    Value2Up,
    Value2Down,
}

#[simi_app]
struct Counters {
    value1: i32,
    value2: i32,
}

impl SimiApp for Counters {
    type Message = Msg;
    type Context = ();
    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                value1: 2018,
                value2: 8,
            },
            (),
        )
    }
    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::Value1Up => self.value1 += 1,
            Msg::Value1Down => self.value1 -= 1,
            Msg::Value2Up => self.value2 += 1,
            Msg::Value2Down => self.value2 -= 1,
        }
        true
    }
    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            h2 { "Counters" }
            p { "using a " b {"simple component"} }
            hr
            Counter (self.value1) {
                title: "First value: " // `title` is the field name of the component struct => line 9 in this file
                up: #onclick=Msg::Value1Up // `up` ... => line 10 in this file
                down: #onclick=Msg::Value1Down // `down` ... => line 11 in this file
            }
            hr
            Counter (self.value2) {
                title: "Second value: "
                up: #onclick=Msg::Value2Up
                down: #onclick=Msg::Value2Down
            }
        }
    }
}
