#![feature(proc_macro_hygiene)]
extern crate simi;
extern crate wasm_bindgen;

use simi::prelude::*;
use wasm_bindgen::prelude::*;

struct SubApp {
    count: i32,
}

enum SubAppMsg {
    Up,
    Down,
}

impl SimiApp for SubApp {
    type Message = SubAppMsg;
    type Context = ();
    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (Self { count: 2018 }, ())
    }
    fn update(&mut self, m: SubAppMsg, _context: &mut Self::Context) -> UpdateView {
        match m {
            SubAppMsg::Up => self.count += 1,
            SubAppMsg::Down => self.count -= 1,
        }
        true
    }
    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            fieldset {
                legend { "This is a sub app that is created by simi" }
                p { "This type of sub app is unable to communicate with other apps" }
                div { "Current value: " self.count }
                button (onclick=#SubAppMsg::Down) { "Down" }
                button (onclick=#SubAppMsg::Up) { "Up" }
            }
        }
    }
}

#[simi_app]
struct MainApp;

impl SimiApp for MainApp {
    type Message = ();
    type Context = ();
    fn init(_: WeakMain<Self>) -> (Self, Self::Context) {
        (MainApp, ())
    }
    fn update(&mut self, _m: Self::Message, _context: &mut Self::Context) -> UpdateView {
        true
    }
    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            p {
                "This is in MainApp. There is no functionality in the main app."
                br
                "This example is just for demonstrating how to have a sub app without requiring a context."
            }
            sub_application {
                container: div()
                application_type: SubApp
            }
        }
    }
}
