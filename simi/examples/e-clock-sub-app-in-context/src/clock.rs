use simi;
use simi::interop::real_dom::window;
use simi::prelude::*;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

pub enum ClockMsg {
    Down,
}

pub struct ClockContext {
    _ticker: Closure<Fn()>,
    pub timeup: Option<::simi::callback::Callback>,
}

impl ClockContext {
    fn new(main: WeakMain<Clock>) -> Self {
        let closure = simi::interop::closure::no_arg(main.clone(), || ClockMsg::Down);
        match window() {
            Some(window) => {
                let rs = window.set_interval_with_callback_and_timeout_and_arguments_0(
                    closure.as_ref().unchecked_ref(),
                    1000,
                );
                simi::error::log_result_error(rs);
            }
            None => simi::error::log_error_message("No window?"),
        }
        Self {
            _ticker: closure,
            timeup: None,
        }
    }
}

pub struct Clock {
    interval: u32,
    current_value: u32,
}

impl SimiApp for Clock {
    type Message = ClockMsg;
    type Context = ClockContext;

    fn init(main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                interval: 4,
                current_value: 4,
            },
            ClockContext::new(main),
        )
    }

    fn update(&mut self, m: Self::Message, context: &mut Self::Context) -> UpdateView {
        match m {
            ClockMsg::Down => {
                self.current_value -= 1;
                if self.current_value == 0 {
                    self.current_value = self.interval;
                    if let Some(ref c) = context.timeup {
                        c();
                    }
                }
            }
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            fieldset {
                legend {"sub-app: " b { "Clock" } }
                p { "This sub app updates every 1 second" }
                p { "Current value: " self.current_value }
            }
        }
    }
}
