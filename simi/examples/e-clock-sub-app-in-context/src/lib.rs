#![feature(proc_macro_hygiene)]
extern crate simi;
extern crate wasm_bindgen;

use simi::prelude::*;
use wasm_bindgen::prelude::*;

mod clock;

use clock::Clock;

enum MainMsg {
    TimeUp,
}

struct MainContext {
    clock: SimiSubApp<Clock>,
}

impl MainContext {
    fn new(_main: WeakMain<MainApp>) -> Self {
        Self {
            clock: SimiSubApp::new(),
        }
    }
}

#[simi_app]
struct MainApp {
    timeup_count: u32,
}

impl SimiApp for MainApp {
    type Message = MainMsg;
    type Context = MainContext;

    fn init(main: WeakMain<Self>) -> (Self, Self::Context) {
        (Self { timeup_count: 0 }, MainContext::new(main))
    }

    fn mounted(&mut self, main: WeakMain<Self>, context: &mut Self::Context) {
        // We must set callback to the main app for the sub app in here.
        // Because the Context of every app is only available after the app is up.
        context // The main context
            .clock // The sub app of the main app
            .main_mut() // Get the main of the sub app
            .context_mut() // Get the context of the sub app
            .expect("No context") // Unwrap from the Result
            .timeup = Some(simi::callback::no_arg(main.clone(), || MainMsg::TimeUp));
    }

    fn update(&mut self, m: Self::Message, _context: &mut Self::Context) -> UpdateView {
        match m {
            MainMsg::TimeUp => {
                self.timeup_count += 1;
                true
            }
        }
    }

    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            h2 { "Clock" }
            p { "inside a " b {"sub app"} }
            hr
            context_sub_application {
                container: div ()
                context_field: clock
            }
            p { "Here, we are back in the main app. It updates only when the Clock is timed up!" }
            p { "Timeup count: " self.timeup_count }
        }
    }
}
