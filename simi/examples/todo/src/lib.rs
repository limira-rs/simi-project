#![feature(proc_macro_hygiene)]
extern crate simi;
extern crate wasm_bindgen;

use simi::interop::real_dom::{Event, FocusEvent, KeyboardEvent};
use simi::prelude::*;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

mod todolist;
use todolist::*;

#[simi_app]
struct TodoApp {
    data: TodoList,
    editing_index: Option<usize>,
}

enum Msg {
    New(Event),
    Toggle(usize),
    ToggleAll(bool),
    ClearCompleted,
    Remove(usize),
    StartEditing(usize),
    EndEditing(KeyboardEvent),
    BlurEditing(FocusEvent),
}

impl SimiApp for TodoApp {
    type Message = Msg;
    type Context = ();

    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                data: TodoList::new(),
                editing_index: None,
            },
            (),
        )
    }
    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        let mut update_view = true;
        match m {
            Msg::New(event) => self.add_new_todo(event),
            Msg::Toggle(index) => self.data.toggle(index),
            Msg::Remove(index) => self.data.remove(index),
            Msg::ToggleAll(checked) => update_view = self.data.toggle_all(checked),
            Msg::ClearCompleted => update_view = self.data.clear_completed(),
            Msg::StartEditing(index) => self.start_editing(index),
            Msg::EndEditing(event) => update_view = self.end_editing(event),
            Msg::BlurEditing(event) => self.blur_editing(event),
        }
        update_view
    }

    fn render(&self, context: AppRenderContext<Self>) {
        let completed_count = self.data.completed_count();
        let todo_count = self.data.list().len();
        let empty_list = todo_count == 0;
        let all_completed = completed_count == todo_count && !empty_list;
        let item_left = todo_count - completed_count;
        application!{
            section (class="todoapp") {
                header(class="header") {
                    h1 { "Simi todos" }
                    input (
                        class="new-todo"
                        placeholder="What needs to be done?"
                        autofocus=true
                        onchange=#Msg::New(?)
                    )
                }
                section ( class="main" "hidden"=empty_list ) {
                    input (
                        id="toggle-all"
                        class="toggle-all"
                        type="checkbox"
                        checked=all_completed
                        onchange=Msg::ToggleAll(!all_completed)
                    )
                    label ( for="toggle-all" ) { "Mark all as complete" }
                    ul ( class="todo-list" ) {
                        for (index,task) in self.data.list().iter().enumerate() {
                            li ( "completed"=task.completed "editing"=self.is_editing(index) ) {
                                div(class="view"){
                                    input(
                                        class="toggle"
                                        type="checkbox"
                                        checked=task.completed
                                        onchange=Msg::Toggle(index)
                                    )
                                    label (ondoubleclick=Msg::StartEditing(index)) { task.title }
                                    button ( class="destroy" onclick=Msg::Remove(index) )
                                }
                                input (
                                    class="edit"
                                    value=task.title
                                    onblur=Msg::BlurEditing(?)
                                    onkeypress=Msg::EndEditing(?)
                                )
                            }
                        }
                    }
                }
                footer ( class="footer" "hidden"=empty_list) {
                    span ( class="todo-count" ) {
                        strong {item_left} self.item_left_label(item_left)
                    }
                    ul ( class="filters" ) {
                        li { a ( class="selected" href="#/" ) {"All"} }
                        li { a ( href="#/active" ) {"Active"} }
                        li { a ( href="#/completed" ) {"Completed"} }
                    }
                    button (
                        class="clear-completed"
                        "hidden"=completed_count==0
                        onclick=#Msg::ClearCompleted
                    ) { "Clear completed" }
                }
            }
            footer ( class="info" ) {
                p { "Double-click to edit a todo" }
                p { "Created by Limira" }
                p { "Part of Simi Project" }
                p { "Part of " a (href="http://todomvc.com") { "TodoMVC" } }
            }
        }
    }
}

impl TodoApp {
    fn item_left_label(&self, item_left: usize) -> &'static str {
        match item_left {
            1 => " item left",
            _ => " items left",
        }
    }

    fn add_new_todo(&mut self, event: Event) {
        if let Some(target) = event.target() {
            let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
            let title = input.value();
            let title = title.trim();
            if title.is_empty() {
                return;
            }
            self.data.add(title);
            input.set_value("");
        }
    }

    fn start_editing(&mut self, index: usize) {
        self.editing_index = Some(index)
    }

    fn is_editing(&self, index: usize) -> bool {
        if let Some(editing_index) = self.editing_index {
            index == editing_index
        } else {
            false
        }
    }

    fn end_editing(&mut self, event: KeyboardEvent) -> bool {
        match event.key().as_str() {
            "Escape" => {
                self.editing_index = None;
                true
            }
            "Enter" => {
                if let Some(target) = (event.as_ref() as &Event).target() {
                    self.process_end_editing(target);
                }
                true
            }
            _ => false,
        }
    }

    fn process_end_editing(&mut self, target: simi::interop::real_dom::EventTarget) {
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        let title = input.value();
        let title = title.trim();
        if title.is_empty() {
            if let Some(index) = self.editing_index {
                self.data.remove(index);
                self.editing_index = None;
            }
        } else {
            if let Some(index) = self.editing_index {
                self.data.update_title(index, title.to_string());
                self.editing_index = None;
            }
        }
    }

    fn blur_editing(&mut self, event: FocusEvent) {
        if let Some(target) = (event.as_ref() as &Event).target() {
            self.process_end_editing(target);
        }
    }
}
