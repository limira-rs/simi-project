#![feature(proc_macro_hygiene)]
#![cfg(target_arch = "wasm32")]

extern crate simi;
extern crate wasm_bindgen;
extern crate wasm_bindgen_test;

use wasm_bindgen_test::wasm_bindgen_test_configure;
wasm_bindgen_test_configure!(run_in_browser);

use simi::interop::real_dom::Event;
use simi::prelude::*;
use simi::test_helper::TestHelper;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_test::*;

const CONTAINER_ELEMENT_ID: &'static str = "simi-app-container";

pub enum Msg {
    SetContentValue(String),

    Up,
    Down,

    ClassName(bool),
    LiteralClass(bool),

    CustomAttributeLevel(i32),
    CustomAttributeName(String),

    InputValue(Event),
    SetInputValue(String),

    CheckValue(Event),
    SetCheckValue(bool),

    RadioValue(&'static str),

    ControlledValueSelect(Event),
    SetControlledValueSelectValue(String),

    ControlledIndexSelect(Event),
    SetControlledIndexSelectIndex(usize),

    TextAreaValue(Event),
    SetTextAreaValue(String),

    UpdateViewFalse,

    SetUpdateOrNot(String),
}

pub struct TestApp {
    content_value: String,
    count: i32,
    class_name: bool,
    custom_attribute_level: i32,
    custom_attribute_name: String,
    literal_class: bool,
    input_value: String,
    check_value: bool,
    radio_value: &'static str,
    controlled_select_value: Option<String>,
    controlled_select_index: Option<usize>,
    textarea_value: String,
    update_or_not: String,
}

impl SimiApp for TestApp {
    type Message = Msg;
    type Context = ();

    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                content_value: "A text use as a content in an element".to_string(),
                count: 2018,
                class_name: true,
                literal_class: false,
                custom_attribute_level: 3,
                custom_attribute_name: "Ruster".to_string(),
                input_value: "Test input value".to_string(),
                check_value: false,
                radio_value: "DefaultValue",
                controlled_select_value: None,
                controlled_select_index: None,
                textarea_value: "Test textarea value".to_string(),
                update_or_not: "Default no update content".to_string(),
            },
            (),
        )
    }

    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::SetContentValue(value) => self.content_value = value,

            Msg::Up => self.count += 1,
            Msg::Down => self.count -= 1,

            Msg::ClassName(value) => self.class_name = value,
            Msg::LiteralClass(value) => self.literal_class = value,

            Msg::CustomAttributeLevel(value) => self.custom_attribute_level = value,
            Msg::CustomAttributeName(value) => self.custom_attribute_name = value,

            Msg::InputValue(event) => self.update_input_value(event),
            Msg::SetInputValue(value) => self.input_value = value,

            Msg::CheckValue(input) => self.update_check_value(input),
            Msg::SetCheckValue(value) => self.check_value = value,

            Msg::RadioValue(value) => self.radio_value = value,

            Msg::ControlledValueSelect(event) => self.update_controlled_select_value(event),
            Msg::SetControlledValueSelectValue(value) => self.controlled_select_value = Some(value),

            Msg::ControlledIndexSelect(event) => self.update_controlled_select_index(event),
            Msg::SetControlledIndexSelectIndex(index) => self.controlled_select_index = Some(index),

            Msg::TextAreaValue(event) => self.update_textarea_value(event),
            Msg::SetTextAreaValue(value) => self.textarea_value = value,

            Msg::UpdateViewFalse => {
                self.count = 2048;
                return false;
            }

            Msg::SetUpdateOrNot(value) => self.update_or_not = value,
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        //use simi::simi_dom::SimiCreator;
        let class_name = "class-by-expr";
        application! {
            //@debug
            h2 { "TestApp" }
            div {
                p (id="no-update-p") { #self.content_value }
                p (id="to-update-p") { self.content_value }
            }
            div {
                div (id="current-value") { "Current value: " self.count }
                button (id="down-button" onclick=Msg::Down) { "Down" }
                button (id="up-button" onclick=Msg::Up) { "Up" }
            }
            div {
                // class_name is attached when creating the DOM, ignore when updating
                div (class="class1 class2" class_name class="class3" id="no-update-classes")
                // class_name?=self.class_name
                //      => class_name will be on/off depend on self.class_name
                // "literal-class1/2" will be on/off depend on self.literal_class
                div (
                    class_name?=self.class_name
                    "literal-class1"=self.literal_class
                    "literal-class2"=!self.literal_class
                    id="conditional-classes"
                )
            }
            div {
                div (id="custom-attributes"
                    data-user-name-no-update=#self.custom_attribute_name
                    data-user-name=self.custom_attribute_name
                    data-user-level=self.custom_attribute_level
                )
            }
            div {
                input (id="literal-input" value="Constant value for input")
                input (id="no-update-input" value=#self.input_value)
                input (id="tracking-input" value=self.input_value onchange=Msg::InputValue(?))
            }
            div {
                input (id="literal-check-true" type="checkbox" name="test-check" value="literal-true" checked=true)
                input (id="literal-check-false" type="checkbox" name="test-check" value="literal-false" checked=false)
                input (id="no-update-check" type="checkbox" name="test-check" value="expr-value-no-update" checked=#!self.check_value)
                input (id="tracking-check" type="checkbox" name="test-check" value="expr-value" checked=self.check_value onchange=Msg::CheckValue(?))
            }
            div {
                input (id="radio1" type="radio" name="test-radio" value="radio-value1"
                    checked=self.radio_checked("RadioValue1")
                    onchange=Msg::RadioValue("RadioValue1"))
                input (id="radio2" type="radio" name="test-radio" value="radio-value2"
                    checked=self.radio_checked("RadioValue2")
                    onchange=Msg::RadioValue("RadioValue2"))
                input (id="radio3" type="radio" name="test-radio" value="radio-value3"
                    checked=self.radio_checked("RadioValue3")
                    onchange=Msg::RadioValue("RadioValue3"))
                input (id="radio4" type="radio" name="test-radio" value="radio-value4"
                    checked=self.radio_checked("RadioValue4")
                    onchange=Msg::RadioValue("RadioValue4"))
            }
            div {
                select (id="init-value-select" value=Some("second".to_string())) {
                    option (id="init-value-select-option1" value="first") { "First Option" }
                    option (id="init-value-select-option2" value="second") { "Second Option" }
                    option (id="init-value-select-option3" value="third") { "Third Option" }
                }

                select (id="controlled-by-value-select"
                    size=6
                    value=self.controlled_select_value
                    onchange=Msg::ControlledValueSelect(?)
                ) {
                    option (id="controlled-by-value-select-option1" value="first") { "First Option" }
                    option (id="controlled-by-value-select-option2" value="second") { "Second Option" }
                    option (id="controlled-by-value-select-option3" value="third") { "Third Option" }
                }
            }
            div {
                select (id="init-index-select" index=Some(1)){
                    option (id="init-index-select-option1" value="first") { "First Option" }
                    option (id="init-index-select-option2" value="second") { "Second Option" }
                    option (id="init-index-select-option3" value="third") { "Third Option" }
                }
                select (id="controlled-by-index-select"
                    size=6
                    index=self.controlled_select_index
                    onchange=Msg::ControlledIndexSelect(?)
                ) {
                    option (id="controlled-by-index-select-option1" value="first") { "First Option" }
                    option (id="controlled-by-index-select-option2" value="second") { "Second Option" }
                    option (id="controlled-by-index-select-option3" value="third") { "Third Option" }
                }
            }
            div {
                textarea (id="textarea-test" rows=8 cols=32 value=self.textarea_value onchange=Msg::TextAreaValue(?))
            }
            div {
                #div (id="no-update-level0") { self.update_or_not }
                #div {
                    div (id="no-update-level1") { self.update_or_not }
                }
                div (id="must-update") { self.update_or_not }
            }
        }
    }
}

impl TestApp {
    fn update_input_value(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        self.input_value = input.value();
    }

    fn update_textarea_value(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let ta: &simi::interop::real_dom::HtmlTextAreaElement = target.unchecked_ref();
        self.textarea_value = ta.value();
    }

    fn update_check_value(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        self.check_value = input.checked();
    }

    fn radio_checked(&self, value: &str) -> bool {
        self.radio_value == value
    }

    fn update_controlled_select_value(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let se: &simi::interop::real_dom::HtmlSelectElement = target.unchecked_ref();
        if se.selected_index() < 0 {
            self.controlled_select_value = None;
        } else {
            self.controlled_select_value = Some(se.value());
        }
    }

    fn update_controlled_select_index(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let se: &simi::interop::real_dom::HtmlSelectElement = target.unchecked_ref();
        if se.selected_index() < 0 {
            self.controlled_select_index = None;
        } else {
            self.controlled_select_index = Some(se.selected_index() as usize);
        }
    }
}

#[wasm_bindgen]
pub struct AppHandle {
    main: RcMain<TestApp>,
}

impl SimiHandle<TestApp> for AppHandle {
    fn main(&self) -> RcMain<TestApp> {
        self.main.clone()
    }
}

#[wasm_bindgen]
impl AppHandle {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self {
            main: SimiMain::new_rc_main(),
        }
    }

    pub fn create_container(&self) {
        if ::simi::interop::real_dom::document()
            .get_element_by_id(CONTAINER_ELEMENT_ID)
            .is_some()
        {
            return;
        }
        let div = ::simi::interop::real_dom::document()
            .create_element("div")
            .expect("Create div must succeed");
        div.set_id(CONTAINER_ELEMENT_ID);
        let body = ::simi::interop::real_dom::document()
            .body()
            .expect("Must have a body");
        let node: &::simi::interop::real_dom::Node = body.as_ref();
        ::simi::interop::display_error(node.append_child(div.as_ref()));
    }

    pub fn start(&mut self) {
        self.in_element_id(CONTAINER_ELEMENT_ID);
    }
}

impl AppHandle {
    pub fn start_from_rust(&mut self) {
        self.create_container();
        self.start();
    }
}

#[wasm_bindgen(module = "./tests/simi_app.js")]
extern "C" {
    fn run_app() -> AppHandle;
}

#[wasm_bindgen_test]
fn run_from_rust() {
    let mut app_handle = AppHandle::new();
    app_handle.start_from_rust();
    test_app(&mut app_handle);
}

#[wasm_bindgen_test]
fn run_from_js() {
    let mut app_handle = run_app();
    test_app(&mut app_handle);
}

fn test_app(app_handle: &mut AppHandle) {
    test_content(app_handle);
    test_click_and_no_update(app_handle);
    test_classes(app_handle);
    test_custom_attributes(app_handle);
    test_input_change(app_handle);
    test_check(app_handle);
    test_radio(app_handle);
    test_controlled_value_select(app_handle);
    test_controlled_index_select(app_handle);
    test_textarea(app_handle);
    test_no_update_element_and_subs(app_handle);
}

fn test_content(app_handle: &mut AppHandle) {
    assert_eq!(
        "A text use as a content in an element",
        app_handle.main.borrow().get_app().content_value
    );

    let no_update = TestHelper::id("no-update-p");
    let to_update = TestHelper::id("to-update-p");
    assert_eq!(
        "A text use as a content in an element",
        no_update
            .as_node()
            .text_content()
            .expect("Must have some content")
    );
    assert_eq!(
        "A text use as a content in an element",
        to_update
            .as_node()
            .text_content()
            .expect("Must have some content")
    );

    // Event the text
    app_handle.main.borrow_mut().update(Msg::SetContentValue(
        "New text for the content in an element".to_string(),
    ));

    assert_eq!(
        "A text use as a content in an element",
        no_update
            .as_node()
            .text_content()
            .expect("Must have some content")
    );
    assert_eq!(
        "New text for the content in an element",
        to_update
            .as_node()
            .text_content()
            .expect("Must have some content")
    );
}

fn test_click_and_no_update(app_handle: &mut AppHandle) {
    let div = TestHelper::id("current-value");

    assert_eq!(2018, app_handle.main.borrow().get_app().count);
    assert_eq!(
        "Current value: 2018",
        div.as_node()
            .text_content()
            .expect("Must have some content")
    );

    TestHelper::id("down-button").click();
    assert_eq!(2017, app_handle.main.borrow().get_app().count);
    assert_eq!(
        "Current value: 2017",
        div.as_node()
            .text_content()
            .expect("Must have some content")
    );

    let up = TestHelper::id("up-button");
    up.click();
    up.click();
    assert_eq!(2019, app_handle.main.borrow().get_app().count);
    assert_eq!(
        "Current value: 2019",
        div.as_node()
            .text_content()
            .expect("Must have some content")
    );

    app_handle.main.borrow_mut().update(Msg::UpdateViewFalse);
    assert_eq!(2048, app_handle.main.borrow().get_app().count);
    assert_eq!(
        "Current value: 2019",
        div.as_node()
            .text_content()
            .expect("Must have some content")
    );
}

fn test_classes(app_handle: &mut AppHandle) {
    let no_update = TestHelper::id("no-update-classes")
        .as_element()
        .class_list();
    let conditional = TestHelper::id("conditional-classes")
        .as_element()
        .class_list();

    assert_eq!(true, no_update.contains("class1"));
    assert_eq!(true, no_update.contains("class2"));
    assert_eq!(true, no_update.contains("class3"));
    assert_eq!(true, no_update.contains("class-by-expr"));

    assert_eq!(true, conditional.contains("class-by-expr"));
    assert_eq!(false, conditional.contains("literal-class1"));
    assert_eq!(true, conditional.contains("literal-class2"));

    app_handle.main.borrow_mut().update(Msg::ClassName(false));
    assert_eq!(true, no_update.contains("class1"));
    assert_eq!(true, no_update.contains("class2"));
    assert_eq!(true, no_update.contains("class3"));
    assert_eq!(true, no_update.contains("class-by-expr"));

    assert_eq!(false, conditional.contains("class-by-expr"));
    assert_eq!(false, conditional.contains("literal-class1"));
    assert_eq!(true, conditional.contains("literal-class2"));

    app_handle.main.borrow_mut().update(Msg::LiteralClass(true));
    assert_eq!(true, no_update.contains("class1"));
    assert_eq!(true, no_update.contains("class2"));
    assert_eq!(true, no_update.contains("class3"));
    assert_eq!(true, no_update.contains("class-by-expr"));

    assert_eq!(false, conditional.contains("class-by-expr"));
    assert_eq!(true, conditional.contains("literal-class1"));
    assert_eq!(false, conditional.contains("literal-class2"));
}

fn test_custom_attributes(app_handle: &mut AppHandle) {
    let tester = TestHelper::id("custom-attributes");
    assert_eq!(
        "Ruster",
        app_handle.main.borrow().get_app().custom_attribute_name
    );
    assert_eq!(
        "Ruster",
        tester
            .as_element()
            .get_attribute("data-user-name-no-update")
            .expect("Custom data name no update")
    );
    assert_eq!(
        "Ruster",
        tester
            .as_element()
            .get_attribute("data-user-name")
            .expect("Custom data name")
    );
    assert_eq!(
        "3",
        tester
            .as_element()
            .get_attribute("data-user-level")
            .expect("Custom data level")
    );

    app_handle
        .main
        .borrow_mut()
        .update(Msg::CustomAttributeLevel(4));
    assert_eq!(
        "4",
        tester
            .as_element()
            .get_attribute("data-user-level")
            .expect("Custom data level")
    );

    app_handle
        .main
        .borrow_mut()
        .update(Msg::CustomAttributeName("Rustest".to_string()));
    assert_eq!(
        "Ruster",
        tester
            .as_element()
            .get_attribute("data-user-name-no-update")
            .expect("Custom data name no update")
    );
    assert_eq!(
        "Rustest",
        tester
            .as_element()
            .get_attribute("data-user-name")
            .expect("Custom data name")
    );
}

fn test_input_change(app_handle: &mut AppHandle) {
    assert_eq!(
        "Test input value",
        app_handle.main.borrow().get_app().input_value
    );

    let i = TestHelper::id("tracking-input");
    assert_eq!("Test input value", i.as_input().value());

    // DOM => APP
    i.change_input("New test content");
    assert_eq!(
        "New test content",
        app_handle.main.borrow().get_app().input_value
    );

    // APP => DOM
    app_handle
        .main
        .borrow_mut()
        .update(Msg::SetInputValue("Content sets by app".to_string()));
    assert_eq!(
        "Content sets by app",
        app_handle.main.borrow().get_app().input_value
    );
    assert_eq!("Content sets by app", i.as_input().value());
}

fn test_check(app_handle: &mut AppHandle) {
    assert_eq!(false, app_handle.main.borrow().get_app().check_value);
    let no_update = TestHelper::id("no-update-check");
    let to_update = TestHelper::id("tracking-check");
    {
        let i = TestHelper::id("literal-check-true");
        assert_eq!(true, i.as_input().checked());

        let i = TestHelper::id("literal-check-false");
        assert_eq!(false, i.as_input().checked());

        assert_eq!(false, to_update.as_input().checked());
        assert_eq!(true, no_update.as_input().checked());
    }
    // DOM => APP
    // Check on
    {
        to_update.click();
        assert_eq!(true, app_handle.main.borrow().get_app().check_value);
        assert_eq!(true, to_update.as_input().checked());

        assert_eq!(true, no_update.as_input().checked());
    }
    // Check off
    {
        to_update.click();
        assert_eq!(false, app_handle.main.borrow().get_app().check_value);
        assert_eq!(false, to_update.as_input().checked());

        assert_eq!(true, no_update.as_input().checked());
    }
    // APP => DOM
    {
        app_handle
            .main
            .borrow_mut()
            .update(Msg::SetCheckValue(true));

        assert_eq!(true, app_handle.main.borrow().get_app().check_value);
        assert_eq!(true, to_update.as_input().checked());

        assert_eq!(true, no_update.as_input().checked());
    }
}

fn test_radio(app_handle: &mut AppHandle) {
    assert_eq!(
        "DefaultValue",
        app_handle.main.borrow().get_app().radio_value
    );
    let radio1 = TestHelper::id("radio1");
    let radio2 = TestHelper::id("radio2");
    let radio3 = TestHelper::id("radio3");
    let radio4 = TestHelper::id("radio4");
    // DOM => APP
    // Select 'radio1'
    {
        radio1.click();
        assert_eq!(
            "RadioValue1",
            app_handle.main.borrow().get_app().radio_value
        );
        assert_eq!(true, radio1.as_input().checked());
        assert_eq!(false, radio2.as_input().checked());
        assert_eq!(false, radio3.as_input().checked());
        assert_eq!(false, radio4.as_input().checked());
    }
    // Select 'radio2'
    {
        radio2.click();
        assert_eq!(
            "RadioValue2",
            app_handle.main.borrow().get_app().radio_value
        );
        assert_eq!(false, radio1.as_input().checked());
        assert_eq!(true, radio2.as_input().checked());
        assert_eq!(false, radio3.as_input().checked());
        assert_eq!(false, radio4.as_input().checked());
    }
    // APP => DOM
    {
        app_handle
            .main
            .borrow_mut()
            .update(Msg::RadioValue("RadioValue3"));
        assert_eq!(
            "RadioValue3",
            app_handle.main.borrow().get_app().radio_value
        );
        assert_eq!(false, radio1.as_input().checked());
        assert_eq!(false, radio2.as_input().checked());
        assert_eq!(true, radio3.as_input().checked());
        assert_eq!(false, radio4.as_input().checked());
    }
}

fn test_controlled_value_select(app_handle: &mut AppHandle) {
    // Defaults
    let select = {
        assert_eq!(
            None,
            app_handle.main.borrow().get_app().controlled_select_value
        );

        let s = TestHelper::id("init-value-select");
        assert_eq!("second", s.as_select().value());
        assert_eq!(1, s.as_select().selected_index());

        let s = TestHelper::id("controlled-by-value-select");
        assert_eq!(6, s.as_select().size());
        assert_eq!("", s.as_select().value());
        assert_eq!(-1, s.as_select().selected_index());
        s
    };
    // Event DOM => APP
    {
        select.change_select("first");
        assert_eq!("first", select.as_select().value());
        assert_eq!(0, select.as_select().selected_index());

        let o = TestHelper::id("controlled-by-value-select-option1");
        assert_eq!(true, o.as_option().selected());
        assert_eq!(0, o.as_option().index());

        assert_eq!(
            Some("first".to_string()),
            app_handle.main.borrow().get_app().controlled_select_value
        );
    }
    // Event APP => DOM
    {
        app_handle
            .main
            .borrow_mut()
            .update(Msg::SetControlledValueSelectValue("second".to_string()));
        assert_eq!(
            Some("second".to_string()),
            app_handle.main.borrow().get_app().controlled_select_value
        );

        assert_eq!("second", select.as_select().value());
        assert_eq!(1, select.as_select().selected_index());

        let o = TestHelper::id("controlled-by-value-select-option2");
        assert_eq!(true, o.as_option().selected());
        assert_eq!(1, o.as_option().index());
    }
    // DOM => APP again
    {
        select.change_select("third");

        assert_eq!("third", select.as_select().value());
        assert_eq!(2, select.as_select().selected_index());

        let o = TestHelper::id("controlled-by-value-select-option3");
        assert_eq!(true, o.as_option().selected());
        assert_eq!(2, o.as_option().index());

        assert_eq!(
            Some("third".to_string()),
            app_handle.main.borrow().get_app().controlled_select_value
        );
    }
}

fn test_controlled_index_select(app_handle: &mut AppHandle) {
    // Defaults
    let select = {
        assert_eq!(
            None,
            app_handle.main.borrow().get_app().controlled_select_index
        );

        let s = TestHelper::id("init-index-select");
        assert_eq!("second", s.as_select().value());
        assert_eq!(1, s.as_select().selected_index());

        let s = TestHelper::id("controlled-by-index-select");
        assert_eq!(6, s.as_select().size());
        assert_eq!("", s.as_select().value());
        assert_eq!(-1, s.as_select().selected_index());
        s
    };
    // Event DOM => APP
    {
        select.change_index(0);
        assert_eq!("first", select.as_select().value());
        assert_eq!(0, select.as_select().selected_index());

        let o = TestHelper::id("controlled-by-index-select-option1");
        assert_eq!(true, o.as_option().selected());
        assert_eq!(0, o.as_option().index());

        assert_eq!(
            Some(0),
            app_handle.main.borrow().get_app().controlled_select_index
        );
    }
    // Event APP => DOM
    {
        app_handle
            .main
            .borrow_mut()
            .update(Msg::SetControlledIndexSelectIndex(1));
        assert_eq!(
            Some(1),
            app_handle.main.borrow().get_app().controlled_select_index
        );

        assert_eq!("second", select.as_select().value());
        assert_eq!(1, select.as_select().selected_index());

        let o = TestHelper::id("controlled-by-index-select-option2");
        assert_eq!(true, o.as_option().selected());
        assert_eq!(1, o.as_option().index());
    }
    // DOM => APP again
    {
        select.change_index(2);

        assert_eq!("third", select.as_select().value());
        assert_eq!(2, select.as_select().selected_index());

        let o = TestHelper::id("controlled-by-index-select-option3");
        assert_eq!(true, o.as_option().selected());
        assert_eq!(2, o.as_option().index());

        assert_eq!(
            Some(2),
            app_handle.main.borrow().get_app().controlled_select_index
        );
    }
}

fn test_textarea(app_handle: &mut AppHandle) {
    assert_eq!(
        "Test textarea value",
        app_handle.main.borrow().get_app().textarea_value
    );
    let textarea = TestHelper::id("textarea-test");
    assert_eq!("Test textarea value", textarea.as_textarea().value());
    // DOM => APP
    {
        textarea.change_textarea("New value for textarea");
        assert_eq!("New value for textarea", textarea.as_textarea().value());
        assert_eq!(
            "New value for textarea",
            app_handle.main.borrow().get_app().textarea_value
        );
    }
    // APP => DOM
    {
        app_handle.main.borrow_mut().update(Msg::SetTextAreaValue(
            "TextArea value setted from app".to_string(),
        ));
        assert_eq!(
            "TextArea value setted from app",
            textarea.as_textarea().value()
        );
        assert_eq!(
            "TextArea value setted from app",
            app_handle.main.borrow().get_app().textarea_value
        );
    }
}

fn test_no_update_element_and_subs(app: &mut AppHandle) {
    let l0 = TestHelper::id("no-update-level0");
    let l1 = TestHelper::id("no-update-level1");
    let update = TestHelper::id("must-update");

    assert_eq!(
        "Default no update content",
        l0.as_node().text_content().expect("Must have some content")
    );
    assert_eq!(
        "Default no update content",
        l1.as_node().text_content().expect("Must have some content")
    );
    assert_eq!(
        "Default no update content",
        update
            .as_node()
            .text_content()
            .expect("Must have some content")
    );

    app.main
        .borrow_mut()
        .update(Msg::SetUpdateOrNot("Updated content".to_string()));
    assert_eq!(
        "Default no update content",
        l0.as_node().text_content().expect("Must have some content")
    );
    assert_eq!(
        "Default no update content",
        l1.as_node().text_content().expect("Must have some content")
    );
    assert_eq!(
        "Updated content",
        update
            .as_node()
            .text_content()
            .expect("Must have some content")
    );
}
