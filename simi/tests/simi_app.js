import { AppHandle } from '/wasm-bindgen-test';

export function run_app() {
    window.simi_app_handle = new AppHandle();
    window.simi_app_handle.create_container();
    window.simi_app_handle.start();
    return window.simi_app_handle;
}
