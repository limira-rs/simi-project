#![feature(proc_macro_hygiene)]
#![cfg(target_arch = "wasm32")]

extern crate simi;
extern crate wasm_bindgen;
extern crate wasm_bindgen_test;

use wasm_bindgen_test::wasm_bindgen_test_configure;
wasm_bindgen_test_configure!(run_in_browser);

use simi::prelude::*;
use simi::test_helper::TestHelper;
use wasm_bindgen::prelude::*;
use wasm_bindgen_test::*;

#[simi_app]
struct TestApp {
    value: Option<String>,
}

enum Msg {
    SetValue(String),
    ClearValue,
}

impl SimiApp for TestApp {
    type Message = Msg;
    type Context = ();

    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (Self { value: None }, ())
    }

    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::ClearValue => self.value = None,
            Msg::SetValue(v) => self.value = Some(v),
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        // Because of the Simi framework itself. Items at the root of the macro content
        // and items in other items are process slightly difference. This test tests if/match
        // at both root level and sub level
        application!{
            if let Some(ref value) = self.value {
                div (id="if-let-self.value-directly-at-root") { value }
            }else{
                div (id="if-let-self.value-is-none") { "None" }
            }
            match self.value {
                Some(ref value) => div (id="match-self.value-directly-at-root") { value },
                None => div (id="match-self.value-is-none") { "None" }
            }
            div (id="if-let-inside-a-div") {
                // Also item order
                "Text before if "
                if let Some(ref value) = self.value {
                    div (id="if-let-self.value-inside-div") { value }
                }else{
                    div (id="if-let-self.value-is-none-inside-div") { "None" }
                }
                ", and text after else"
            }
            div (id="match-inside-a-div") {
                "Text before match "
                match self.value {
                    Some(ref value) => div (id="match-self.value-inside-div") { value },
                    None => div (id="match-self.value-is-none-inside-div") { "None" }
                }
                ", and text after match"
            }
        }
    }
}

impl AppHandle {
    pub fn start_app_for_test(&mut self) {
        let test_element_id = "div-element-use-for-testing-my-app";
        let doc = simi::interop::real_dom::document();
        if doc.get_element_by_id(test_element_id).is_none() {
            let div = doc
                .create_element("div")
                .expect("Create div element for testing");
            div.set_id(test_element_id);
            let body = doc.body().expect("Get body of the Document");
            (body.as_ref() as &simi::interop::real_dom::RealNode)
                .append_child(div.as_ref())
                .expect("Add div element to the body element");
        }
        self.in_element_id(test_element_id);
    }
}

#[wasm_bindgen_test]
fn if_let_and_match_with_extraction() {
    let mut app = AppHandle::new();
    app.start_app_for_test();
    value_is_none();

    let value = "if-let-match-with-extraction";
    app.main
        .borrow_mut()
        .update(Msg::SetValue(value.to_string()));
    value_is(value);

    app.main.borrow_mut().update(Msg::ClearValue);
    value_is_none();
}

fn value_is_none() {
    assert!(TestHelper::no_id("if-let-self.value-directly-at-root"));
    assert!(TestHelper::no_id("match-self.value-directly-at-root"));
    assert!(TestHelper::no_id("if-let-self.value-inside-div"));
    assert!(TestHelper::no_id("match-self.value-inside-div"));

    let e = TestHelper::id("if-let-self.value-is-none");
    assert_eq!(
        "None",
        e.as_node().text_content().expect("Get node content")
    );

    let e = TestHelper::id("match-self.value-is-none");
    assert_eq!(
        "None",
        e.as_node().text_content().expect("Get node content")
    );

    let e = TestHelper::id("if-let-self.value-is-none-inside-div");
    assert_eq!(
        "None",
        e.as_node().text_content().expect("Get node content")
    );

    let e = TestHelper::id("match-self.value-is-none-inside-div");
    assert_eq!(
        "None",
        e.as_node().text_content().expect("Get node content")
    );

    let e = TestHelper::id("if-let-inside-a-div");
    assert_eq!(
        "Text before if None, and text after else",
        e.as_node().text_content().expect("Get node content")
    );
    let e = TestHelper::id("match-inside-a-div");
    assert_eq!(
        "Text before match None, and text after match",
        e.as_node().text_content().expect("Get node content")
    );
}

fn value_is(value: &str) {
    assert!(TestHelper::no_id("if-let-self.value-is-none"));
    assert!(TestHelper::no_id("match-self.value-is-none"));
    assert!(TestHelper::no_id("if-let-self.value-is-none-inside-div"));
    assert!(TestHelper::no_id("match-self.value-is-none-inside-div"));

    let e = TestHelper::id("if-let-self.value-directly-at-root");
    assert_eq!(value, e.as_node().text_content().expect("Get node content"));

    let e = TestHelper::id("match-self.value-directly-at-root");
    assert_eq!(value, e.as_node().text_content().expect("Get node content"));

    let e = TestHelper::id("if-let-self.value-inside-div");
    assert_eq!(value, e.as_node().text_content().expect("Get node content"));

    let e = TestHelper::id("match-self.value-inside-div");
    assert_eq!(value, e.as_node().text_content().expect("Get node content"));

    let e = TestHelper::id("if-let-inside-a-div");
    assert_eq!(
        format!("Text before if {}, and text after else", value),
        e.as_node().text_content().expect("Get node content")
    );
    let e = TestHelper::id("match-inside-a-div");
    assert_eq!(
        format!("Text before match {}, and text after match", value),
        e.as_node().text_content().expect("Get node content")
    );
}
