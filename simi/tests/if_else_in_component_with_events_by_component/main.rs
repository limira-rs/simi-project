#![feature(proc_macro_hygiene)]
#![cfg(target_arch = "wasm32")]

extern crate simi;
extern crate wasm_bindgen;
extern crate wasm_bindgen_test;

use wasm_bindgen_test::wasm_bindgen_test_configure;
wasm_bindgen_test_configure!(run_in_browser);

use simi::interop::real_dom::Event;
use simi::prelude::*;
use simi::test_helper::TestHelper;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_test::*;

const CONTAINER_ELEMENT_ID: &'static str = "simi-app-container";

mod component;
use component::TestSnippet;

#[derive(PartialEq)]
pub enum FlowerType {
    Aquatic,
    Terrestrial,
    Orchidaceae,
}

impl FlowerType {
    fn flower_list(&self) -> [&str; 4] {
        match self {
            FlowerType::Aquatic => ["Iris", "Lotus", "Water Lily", "Water Hyacinth"],
            FlowerType::Terrestrial => ["Rose", "Daisy", "Lavender", "Royal Poinciana"],
            FlowerType::Orchidaceae => ["Cattleya", "Cymbidium", "Vanda", "Laelia"],
        }
    }
}

pub struct TestApp {
    flower_type: FlowerType,
    name: Option<String>,
}

pub enum Msg {
    FlowerTypeChange(FlowerType),
    NameChange(Event),
    SetName(String),
}

impl SimiApp for TestApp {
    type Message = Msg;
    type Context = ();

    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                flower_type: FlowerType::Terrestrial,
                name: Some("Rose".to_string()),
            },
            (),
        )
    }

    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::FlowerTypeChange(flower_type) => self.flower_type = flower_type,
            Msg::NameChange(event) => self.update_name(event),
            Msg::SetName(value) => self.name = Some(value),
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            //@debug
            TestSnippet (self)
        }
    }
}

impl TestApp {
    fn update_name(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let se: &simi::interop::real_dom::HtmlSelectElement = target.unchecked_ref();
        if se.selected_index() < 0 {
            self.name = None;
        } else {
            self.name = Some(se.value());
        }
    }
}

#[wasm_bindgen]
pub struct AppHandle {
    main: RcMain<TestApp>,
}

impl SimiHandle<TestApp> for AppHandle {
    fn main(&self) -> RcMain<TestApp> {
        self.main.clone()
    }
}

#[wasm_bindgen]
impl AppHandle {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self {
            main: SimiMain::new_rc_main(),
        }
    }

    pub fn create_container(&self) {
        if ::simi::interop::real_dom::document()
            .get_element_by_id(CONTAINER_ELEMENT_ID)
            .is_some()
        {
            return;
        }
        let div = ::simi::interop::real_dom::document()
            .create_element("div")
            .expect("Create div must succeed");
        div.set_id(CONTAINER_ELEMENT_ID);
        let body = ::simi::interop::real_dom::document()
            .body()
            .expect("Must have a body");
        let node: &::simi::interop::real_dom::Node = body.as_ref();
        ::simi::interop::display_error(node.append_child(div.as_ref()));
    }

    pub fn start(&mut self) {
        self.in_element_id(CONTAINER_ELEMENT_ID);
    }
}

impl AppHandle {
    pub fn start_from_rust(&mut self) {
        self.create_container();
        self.start();
    }
}

#[wasm_bindgen(module = "./tests/simi_app.js")]
extern "C" {
    fn run_app() -> AppHandle;
}

#[wasm_bindgen_test]
fn test_if_else() {
    let mut app_handle = run_app();
    test_if_with_a_final_else(&mut app_handle);
    test_if_without_a_final_else(&mut app_handle);
    test_order_of_content(&mut app_handle);
}

fn test_if_with_a_final_else(app: &mut AppHandle) {
    let radio_aquatic = TestHelper::id("radio-aquatic");
    let radio_terrestrial = TestHelper::id("radio-terrestrial");
    let radio_orchids = TestHelper::id("radio-orchids");

    assert_eq!(false, radio_aquatic.as_input().checked());
    assert_eq!(true, radio_terrestrial.as_input().checked());
    assert_eq!(false, radio_orchids.as_input().checked());

    assert!(TestHelper::no_id("div-aquatic"));
    assert!(TestHelper::no_id("div-orchid"));

    let div_terrestrial = TestHelper::id("div-terrestrial");
    assert_eq!(
        "terrestrial flowers",
        div_terrestrial
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    let select_terrestrial = TestHelper::id("select-terrestrial");
    assert_eq!("Rose", select_terrestrial.as_select().value());
    select_terrestrial.change_select("Lavender");
    assert_eq!("Lavender", select_terrestrial.as_select().value());
    assert_eq!(
        Some("Lavender".to_string()),
        app.main.borrow().get_app().name
    );

    app.main
        .borrow_mut()
        .update(Msg::SetName("Daisy".to_string()));
    assert_eq!("Daisy", select_terrestrial.as_select().value());

    // Event to aquatic
    radio_aquatic.click();

    assert!(TestHelper::no_id("div-terrestrial"));
    assert!(TestHelper::no_id("div-orchid"));

    let div_aquatic = TestHelper::id("div-aquatic");
    assert_eq!(
        "aquatic flowers",
        div_aquatic
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    let select_aquatic = TestHelper::id("select-aquatic");
    assert_eq!("", select_aquatic.as_select().value());
    assert_eq!(-1, select_aquatic.as_select().selected_index());

    select_aquatic.change_select("Water Lily");
    assert_eq!("Water Lily", select_aquatic.as_select().value());
    assert_eq!(
        Some("Water Lily".to_string()),
        app.main.borrow().get_app().name
    );

    app.main
        .borrow_mut()
        .update(Msg::SetName("Lotus".to_string()));
    assert_eq!("Lotus", select_aquatic.as_select().value());

    // Event to orchids
    radio_orchids.click();

    assert!(TestHelper::no_id("div-terrestrial"));
    assert!(TestHelper::no_id("div-aquatic"));

    let div_aquatic = TestHelper::id("div-orchid");
    assert_eq!(
        "orchid flowers",
        div_aquatic
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    let select_orchid = TestHelper::id("select-orchid");
    assert_eq!("", select_orchid.as_select().value());
    assert_eq!(-1, select_orchid.as_select().selected_index());

    select_orchid.change_select("Vanda");
    assert_eq!("Vanda", select_orchid.as_select().value());
    assert_eq!(Some("Vanda".to_string()), app.main.borrow().get_app().name);

    app.main
        .borrow_mut()
        .update(Msg::SetName("Cattleya".to_string()));
    assert_eq!("Cattleya", select_orchid.as_select().value());
}

fn test_if_without_a_final_else(app: &mut AppHandle) {
    // Note: the state of this test is the result of previous test
    let p = TestHelper::id("its-an-orchid");
    assert_eq!(
        "Neither an aquatic flower nor terrestrial flower? May be we call its an orchid?",
        p.as_node().text_content().expect("Must have a content")
    );

    app.main
        .borrow_mut()
        .update(Msg::FlowerTypeChange(FlowerType::Aquatic));

    let span = TestHelper::selector("span.simi-empty-span-element-use-in-place-of-omitted-else");
    assert_eq!("SPAN", span.as_element().tag_name());
}

fn test_order_of_content(app: &mut AppHandle) {
    // Note: the state of this test is the result of previous test
    let something = TestHelper::id("with-something-after-if-else");
    let nothing = TestHelper::id("nothing-after-if-else");

    assert_eq!(
        "It is an aquatic flower",
        nothing
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "It is an aquatic flower",
        something
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    app.main
        .borrow_mut()
        .update(Msg::FlowerTypeChange(FlowerType::Orchidaceae));

    assert_eq!(
        "It is an orchid",
        nothing
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "It is an orchid flower",
        something
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
}
