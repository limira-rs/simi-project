#![feature(proc_macro_hygiene)]
#![cfg(target_arch = "wasm32")]

extern crate simi;
extern crate wasm_bindgen;
extern crate wasm_bindgen_test;

use component::TestSnippet;
use simi::interop::real_dom::Event;
use simi::prelude::*;
use simi::test_helper::TestHelper;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_test::*;

use wasm_bindgen_test::wasm_bindgen_test_configure;
wasm_bindgen_test_configure!(run_in_browser);

pub mod component;

const CONTAINER_ELEMENT_ID: &'static str = "simi-app-container";

#[wasm_bindgen(module = "./tests/simi_app.js")]
extern "C" {
    fn run_app() -> AppHandle;
}

struct TestSnippetApp {
    count: u32,
    text: String,
    class1: String,
    class2: String,
    class_on: bool,
    checked: bool,
    input_value: String,
    input_alt: String,
    autofocus: bool,
    custom_attribute: String,
    select_value: Option<String>,
    select_index: Option<usize>,
}

enum Msg {
    Up,
    Down,
    SetText(String),
    CheckChanged,
    ClassOn(bool),
    InputValue(String),
    InputChange(Event),
    InputAlt(String),
    Autofocus(bool),
    CustomAttribute(String),
    SelectValueChange(Event),
    SelectIndexChange(Event),
    SetSelectValue(String),
    SetSelectIndex(usize),
}

impl SimiApp for TestSnippetApp {
    type Message = Msg;
    type Context = ();

    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                count: 2018,
                text: "Default text".to_string(),
                class1: "main-class1".to_string(),
                class2: "main-class2".to_string(),
                class_on: true,
                checked: true,
                input_value: "Test input value".to_string(),
                input_alt: "CheckedOrNot".to_string(),
                autofocus: false,
                custom_attribute: "some user data".to_string(),
                select_value: None,
                select_index: None,
            },
            (),
        )
    }

    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::Up => self.count += 1,
            Msg::Down => self.count -= 1,
            Msg::SetText(value) => self.text = value,
            Msg::CheckChanged => self.checked = !self.checked,
            Msg::ClassOn(value) => self.class_on = value,
            Msg::InputValue(value) => self.input_value = value,
            Msg::InputChange(event) => self.update_input_value(event),
            Msg::InputAlt(value) => self.input_alt = value,
            Msg::Autofocus(value) => self.autofocus = value,
            Msg::CustomAttribute(value) => self.custom_attribute = value,
            Msg::SelectValueChange(event) => self.update_select_value(event),
            Msg::SelectIndexChange(event) => self.update_select_index(event),
            Msg::SetSelectValue(value) => self.select_value = Some(value),
            Msg::SetSelectIndex(value) => self.select_index = Some(value),
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        application! {
            TestSnippet (self.count){
                title: &self.text
                no_update_title: #&self.text
                props: #self.count
                up: onclick=Msg::Up
                down: onclick=Msg::Down

                class1: &self.class1
                class2: &self.class2
                class_on: self.class_on

                input_value: &self.input_value
                input_changed: onchange=Msg::InputChange(?)
                checked: self.checked
                no_update_checked: #self.checked
                input_alt: &self.input_alt
                no_update_input_alt: #&self.input_alt
                check_changed: onchange=Msg::CheckChanged

                autofocus: self.autofocus,
                custom_attribute: &self.custom_attribute

                select_value: &self.select_value,
                select_index: &self.select_index,

                select_value_changed: onchange=Msg::SelectValueChange(?)
                select_index_changed: onchange=Msg::SelectIndexChange(?)
            }
        }
    }
}

impl TestSnippetApp {
    fn update_input_value(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        self.input_value = input.value();
    }

    fn update_select_value(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let se: &simi::interop::real_dom::HtmlSelectElement = target.unchecked_ref();
        if se.selected_index() < 0 {
            self.select_value = None;
        } else {
            self.select_value = Some(se.value());
        }
    }

    fn update_select_index(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let se: &simi::interop::real_dom::HtmlSelectElement = target.unchecked_ref();
        if se.selected_index() < 0 {
            self.select_index = None;
        } else {
            self.select_index = Some(se.selected_index() as usize);
        }
    }
}

#[wasm_bindgen]
pub struct AppHandle {
    main: RcMain<TestSnippetApp>,
}

impl SimiHandle<TestSnippetApp> for AppHandle {
    fn main(&self) -> RcMain<TestSnippetApp> {
        self.main.clone()
    }
}

#[wasm_bindgen]
impl AppHandle {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self {
            main: SimiMain::new_rc_main(),
        }
    }

    pub fn create_container(&self) {
        if ::simi::interop::real_dom::document()
            .get_element_by_id(CONTAINER_ELEMENT_ID)
            .is_some()
        {
            return;
        }
        let div = ::simi::interop::real_dom::document()
            .create_element("div")
            .expect("Create div must succeed");
        div.set_id(CONTAINER_ELEMENT_ID);
        let body = ::simi::interop::real_dom::document()
            .body()
            .expect("Must have a body");
        let node: &::simi::interop::real_dom::Node = body.as_ref();
        ::simi::interop::display_error(node.append_child(div.as_ref()));
    }

    pub fn start(&mut self) {
        self.in_element_id(CONTAINER_ELEMENT_ID);
    }
}

#[wasm_bindgen_test]
fn test_snippets() {
    let mut app_handle = run_app();
    test_event_and_content(&mut app_handle);
    test_classes(&mut app_handle);
    test_input_value_or_checked(&mut app_handle);
    test_attribute(&mut app_handle);
    test_custom_attribute(&mut app_handle);
    test_select_value(&mut app_handle);
    test_select_index(&mut app_handle);
}

fn test_event_and_content(app: &mut AppHandle) {
    let no_update_by_snippet = TestHelper::id("no-update-by-snippet");
    let no_update_by_caller = TestHelper::id("no-update-by-caller");
    let current_value = TestHelper::id("content-div");

    assert_eq!(2018, app.main.borrow().get_app().count);
    assert_eq!(
        "Default text 2018",
        no_update_by_snippet
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2018",
        no_update_by_caller
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2018",
        current_value
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    // Clicks
    TestHelper::id("down-button").click();
    assert_eq!(2017, app.main.borrow().get_app().count);
    assert_eq!(
        "Default text 2018",
        no_update_by_snippet
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2018",
        no_update_by_caller
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2017",
        current_value
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    let up = TestHelper::id("up-button");
    up.click();
    up.click();
    assert_eq!(2019, app.main.borrow().get_app().count);
    assert_eq!(
        "Default text 2018",
        no_update_by_snippet
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2018",
        no_update_by_caller
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2019",
        current_value
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    // APP => DOM
    app.main.borrow_mut().update(Msg::Down);
    app.main.borrow_mut().update(Msg::Down);
    assert_eq!(2017, app.main.borrow().get_app().count);
    assert_eq!(
        "Default text 2018",
        no_update_by_snippet
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2018",
        no_update_by_caller
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2017",
        current_value
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    app.main.borrow_mut().update(Msg::Up);
    app.main.borrow_mut().update(Msg::Up);
    assert_eq!(2019, app.main.borrow().get_app().count);
    assert_eq!(
        "Default text 2018",
        no_update_by_snippet
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2018",
        no_update_by_caller
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2019",
        current_value
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    app.main
        .borrow_mut()
        .update(Msg::SetText("Changed text".to_string()));
    assert_eq!(
        "Default text 2018",
        no_update_by_snippet
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Default text 2018",
        no_update_by_caller
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Changed text 2019",
        current_value
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
}

fn test_classes(app: &mut AppHandle) {
    let future = app.main.borrow().get_app().count > 2018;
    let class = TestHelper::id("classes").as_element().class_list();

    assert_eq!(true, app.main.borrow().get_app().class_on);
    assert_eq!(true, future);

    assert_eq!(true, class.contains("snippet-class"));
    assert_eq!(true, class.contains("future-literal"));
    assert_eq!(true, class.contains("main-class1"));
    assert_eq!(true, class.contains("main-class2"));
    assert_eq!(true, class.contains("future-expr1"));
    assert_eq!(true, class.contains("future-expr2"));

    app.main.borrow_mut().update(Msg::ClassOn(false));

    assert_eq!(false, class.contains("snippet-class"));
    assert_eq!(true, class.contains("future-literal"));
    assert_eq!(false, class.contains("main-class1"));
    assert_eq!(true, class.contains("main-class2"));
    assert_eq!(false, class.contains("future-expr1"));
    assert_eq!(true, class.contains("future-expr2"));

    app.main.borrow_mut().update(Msg::Down);
    app.main.borrow_mut().update(Msg::Down);
    let future = app.main.borrow().get_app().count > 2018;
    assert_eq!(false, future);
    assert_eq!(false, class.contains("snippet-class"));
    assert_eq!(false, class.contains("future-literal"));
    assert_eq!(false, class.contains("main-class1"));
    assert_eq!(false, class.contains("main-class2"));
    assert_eq!(false, class.contains("future-expr1"));
    assert_eq!(false, class.contains("future-expr2"));
}

fn test_input_value_or_checked(app: &mut AppHandle) {
    // Value
    let input_no_update = TestHelper::id("input-text-value-no-update");
    let input_to_update = TestHelper::id("input-text-value");

    assert_eq!("Test input value", input_no_update.as_input().value());
    assert_eq!("Test input value", input_to_update.as_input().value());

    // DOM => APP
    input_to_update.change_input("New input value from dom");
    assert_eq!("Test input value", input_no_update.as_input().value());
    assert_eq!(
        "New input value from dom",
        input_to_update.as_input().value()
    );
    assert_eq!(
        "New input value from dom",
        app.main.borrow().get_app().input_value
    );
    // APP => DOM
    app.main
        .borrow_mut()
        .update(Msg::InputValue("Text changed by app".to_string()));
    assert_eq!("Test input value", input_no_update.as_input().value());
    assert_eq!("Text changed by app", input_to_update.as_input().value());
    assert_eq!(
        "Text changed by app",
        app.main.borrow().get_app().input_value
    );

    //==========
    // Checked

    let checkbox_no_update_by_snippet = TestHelper::id("no-update-attribute-by-snippet");
    let checkbox_no_update_by_caller = TestHelper::id("no-update-attribute-by-caller");
    let checkbox = TestHelper::id("current-checked");

    assert_eq!(true, app.main.borrow().get_app().checked);
    assert_eq!(true, checkbox_no_update_by_snippet.as_input().checked());
    assert_eq!(true, checkbox_no_update_by_caller.as_input().checked());
    assert_eq!(true, checkbox.as_input().checked());

    // DOM => APP
    checkbox.click();
    assert_eq!(false, app.main.borrow().get_app().checked);
    assert_eq!(true, checkbox_no_update_by_snippet.as_input().checked());
    assert_eq!(true, checkbox_no_update_by_caller.as_input().checked());
    assert_eq!(false, checkbox.as_input().checked());

    // APP => DOM
    app.main.borrow_mut().update(Msg::CheckChanged);
    assert_eq!(true, app.main.borrow().get_app().checked);
    assert_eq!(true, checkbox_no_update_by_snippet.as_input().checked());
    assert_eq!(true, checkbox_no_update_by_caller.as_input().checked());
    assert_eq!(true, checkbox.as_input().checked());
}

fn test_attribute(app: &mut AppHandle) {
    let checkbox_no_update_by_snippet = TestHelper::id("no-update-attribute-by-snippet");
    let checkbox_no_update_by_caller = TestHelper::id("no-update-attribute-by-caller");
    let checkbox = TestHelper::id("current-checked");

    assert_eq!(
        Some("CheckedOrNot".to_string()),
        checkbox_no_update_by_snippet
            .as_element()
            .get_attribute("alt")
    );
    assert_eq!(
        Some("CheckedOrNot".to_string()),
        checkbox_no_update_by_caller
            .as_element()
            .get_attribute("alt")
    );
    assert_eq!(
        Some("CheckedOrNot".to_string()),
        checkbox.as_element().get_attribute("alt")
    );

    app.main
        .borrow_mut()
        .update(Msg::InputAlt("Select your option".to_string()));

    assert_eq!(
        Some("CheckedOrNot".to_string()),
        checkbox_no_update_by_snippet
            .as_element()
            .get_attribute("alt")
    );
    assert_eq!(
        Some("CheckedOrNot".to_string()),
        checkbox_no_update_by_caller
            .as_element()
            .get_attribute("alt")
    );
    assert_eq!(
        Some("Select your option".to_string()),
        checkbox.as_element().get_attribute("alt")
    );

    //autofocus
    assert_eq!(false, app.main.borrow().get_app().autofocus);
    assert_eq!(
        Some("false".to_string()),
        checkbox.as_element().get_attribute("autofocus")
    );

    app.main.borrow_mut().update(Msg::Autofocus(true));
    assert_eq!(true, app.main.borrow().get_app().autofocus);
    assert_eq!(
        Some("true".to_string()),
        checkbox.as_element().get_attribute("autofocus")
    );
}

fn test_custom_attribute(app: &mut AppHandle) {
    let span = TestHelper::id("custom-attribute");

    assert_eq!(
        "some user data",
        app.main.borrow().get_app().custom_attribute
    );
    assert_eq!(
        Some("some user data".to_string()),
        span.as_element().get_attribute("data-no-update")
    );
    assert_eq!(
        Some("some user data".to_string()),
        span.as_element().get_attribute("data-custom-data")
    );

    app.main
        .borrow_mut()
        .update(Msg::CustomAttribute("user has new data now".to_string()));

    assert_eq!(
        "user has new data now",
        app.main.borrow().get_app().custom_attribute
    );
    assert_eq!(
        Some("some user data".to_string()),
        span.as_element().get_attribute("data-no-update")
    );
    assert_eq!(
        Some("user has new data now".to_string()),
        span.as_element().get_attribute("data-custom-data")
    );
}

fn test_select_value(app: &mut AppHandle) {
    let tester = TestHelper::id("select-value");

    assert_eq!(None, app.main.borrow().get_app().select_value);
    assert_eq!("", tester.as_select().value());
    assert_eq!(-1, tester.as_select().selected_index());

    // DOM => APP
    tester.change_select("first");
    assert_eq!(
        Some("first".to_string()),
        app.main.borrow().get_app().select_value
    );
    assert_eq!("first", tester.as_select().value());
    assert_eq!(0, tester.as_select().selected_index());

    // APP => DOM
    app.main
        .borrow_mut()
        .update(Msg::SetSelectValue("second".to_string()));
    assert_eq!(
        Some("second".to_string()),
        app.main.borrow().get_app().select_value
    );
    assert_eq!("second", tester.as_select().value());
    assert_eq!(1, tester.as_select().selected_index());
}

fn test_select_index(app: &mut AppHandle) {
    let tester = TestHelper::id("select-index");

    assert_eq!(None, app.main.borrow().get_app().select_index);
    assert_eq!("", tester.as_select().value());
    assert_eq!(-1, tester.as_select().selected_index());

    // DOM => APP
    tester.change_select("first");
    assert_eq!(Some(0), app.main.borrow().get_app().select_index);
    assert_eq!("first", tester.as_select().value());
    assert_eq!(0, tester.as_select().selected_index());

    // APP => DOM
    app.main.borrow_mut().update(Msg::SetSelectIndex(1));
    assert_eq!(Some(1), app.main.borrow().get_app().select_index);
    assert_eq!("second", tester.as_select().value());
    assert_eq!(1, tester.as_select().selected_index());
}
