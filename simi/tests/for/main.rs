#![feature(proc_macro_hygiene)]
#![cfg(target_arch = "wasm32")]

extern crate simi;
extern crate wasm_bindgen;
extern crate wasm_bindgen_test;

use wasm_bindgen_test::wasm_bindgen_test_configure;
wasm_bindgen_test_configure!(run_in_browser);

use simi::interop::real_dom::Event;
use simi::prelude::*;
use simi::test_helper::TestHelper;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_test::*;

const CONTAINER_ELEMENT_ID: &'static str = "simi-app-container";

struct TodoItem {
    _id: usize,
    title: String,
    completed: bool,
}

impl TodoItem {
    fn toggle(&mut self) {
        self.completed = !self.completed;
    }
}

struct TestApp {
    tasks: Vec<TodoItem>,
    one_task: Vec<TodoItem>,
    two_tasks: Vec<TodoItem>,
    item_left: usize,
}

enum Msg {
    New(Event),
    Completed(usize),
    Remove(usize),
}

impl SimiApp for TestApp {
    type Message = Msg;
    type Context = ();

    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                tasks: Vec::new(),
                one_task: vec![TodoItem {
                    _id: 0,
                    title: "Test the creation of the `for` loop".to_string(),
                    completed: false,
                }],
                two_tasks: vec![
                    TodoItem {
                        _id: 0,
                        title: "Implement update-phase for the `for` loop".to_string(),
                        completed: false,
                    },
                    TodoItem {
                        _id: 0,
                        title: "Test update-phase of the `for` loop".to_string(),
                        completed: false,
                    },
                ],
                item_left: 0,
            },
            (),
        )
    }
    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::New(event) => self.add_new_todo(event),
            Msg::Completed(index) => self.tasks[index].toggle(),
            Msg::Remove(index) => {
                self.tasks.remove(index);
            }
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        application!{
            //@debug
            section{
                "Test pre-exists list of items"
                ul (id="one-task"){
                    for task in &self.one_task{
                        li (id="one-task-li"){task.title}
                    }
                }
                ul (id="two-tasks"){
                    for task in &self.two_tasks{
                        li {task.title}
                    }
                }
            }
            section (class="todoapp") {
                header(class="header"){
                    h1{"Simi's todos example"}
                    // Use onchange event here, because at the time of implementing this test
                    // `simi` is still use its own `real_dom` binding to browser DOM. I dont want
                    // to expand simi's `real_dom` now because it is very likely will be replace by
                    // wasm-bindgen's web-sys crate
                    input(id="new-todo" class="new-todo" placeholder="What needs to be done?" autofocus=true onchange=Msg::New(?))
                }
                section(class="main"){
                    input (id="toggle-all" class="toggle-all" type="checkbox")
                    label (for="toggle-all") {"Mark all as complete"}
                    ul (class="todo-list") {
                        for (index,task) in self.tasks.iter().enumerate(){
                            li ("completed"=task.completed) {
                                div(class="view"){
                                    input(class="toggle" type="checkbox" checked=task.completed onchange=Msg::Completed(index))
                                    label {task.title}
                                    button(class="destroy" onclick=Msg::Remove(index))
                                }
                                input (class="edit" value=task.title)
                            }
                        }
                    }
                }
                footer(class="footer"){
                    span(class="todo-count"){strong{self.item_left} self.item_left_label()  }
                    ul(class="filters"){
                        li{ a(class="selected" href="#/") {"All"} }
                        li{ a(href="#/active") {"Active"} }
                        li{ a(href="#/completed") {"Completed"} }
                    }
                    button(class="clear-completed"){"Clear completed"}
                }
            }
            footer(class="info"){
                p{"Double-click to edit a todo"}
                p{"Created by Limira-rs"}
                p{"Part of Simi Project"}
            }
        }
    }
}

impl TestApp {
    fn item_left_label(&self) -> String {
        match self.item_left {
            0 => "0 items left".to_string(),
            1 => "1 item left".to_string(),
            _ => format!("{} items left", self.item_left),
        }
    }
    fn add_new_todo(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        let todo = TodoItem {
            _id: self.tasks.len(),
            title: input.value(),
            completed: false,
        };
        self.tasks.push(todo);
    }
}
#[wasm_bindgen]
pub struct AppHandle {
    main: RcMain<TestApp>,
}

impl SimiHandle<TestApp> for AppHandle {
    fn main(&self) -> RcMain<TestApp> {
        self.main.clone()
    }
}

#[wasm_bindgen]
impl AppHandle {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self {
            main: SimiMain::new_rc_main(),
        }
    }

    pub fn create_container(&self) {
        if ::simi::interop::real_dom::document()
            .get_element_by_id(CONTAINER_ELEMENT_ID)
            .is_some()
        {
            return;
        }
        let div = ::simi::interop::real_dom::document()
            .create_element("div")
            .expect("Create div must succeed");
        div.set_id(CONTAINER_ELEMENT_ID);
        let body = ::simi::interop::real_dom::document()
            .body()
            .expect("Must have a body");
        let node: &::simi::interop::real_dom::Node = body.as_ref();
        ::simi::interop::display_error(node.append_child(div.as_ref()));
    }

    pub fn start(&mut self) {
        self.in_element_id(CONTAINER_ELEMENT_ID);
    }
}

impl AppHandle {
    pub fn start_from_rust(&mut self) {
        self.create_container();
        self.start();
    }
}

#[wasm_bindgen(module = "./tests/simi_app.js")]
extern "C" {
    fn run_app() -> AppHandle;
}

#[wasm_bindgen_test]
fn test() {
    let mut app_handle = run_app();
    test_creation_of_for_loop();
    test_updating_of_for_loop(&mut app_handle);
}

fn test_creation_of_for_loop() {
    let ul = TestHelper::id("one-task");
    assert_eq!(1, ul.as_node().child_nodes().length());

    let li = TestHelper::id("one-task-li");
    assert_eq!(
        "Test the creation of the `for` loop",
        li.as_node().text_content().expect("Must have a content")
    );

    let ul = TestHelper::id("two-tasks");
    assert_eq!(2, ul.as_node().child_nodes().length());

    let ul = TestHelper::selector("ul.todo-list");
    assert_eq!(0, ul.as_node().child_nodes().length());
}

fn test_updating_of_for_loop(app: &mut AppHandle) {
    let ul = TestHelper::selector("ul.todo-list");
    assert_eq!(0, ul.as_node().child_nodes().length());

    let new = TestHelper::id("new-todo");
    // Add the first todo
    new.change_input("Test adding the first todo item");
    assert_eq!(1, ul.as_node().child_nodes().length());

    let label = TestHelper::selector("div.view label");
    assert_eq!(
        "Test adding the first todo item",
        label.as_node().text_content().expect("Must have a content")
    );

    // Add a second todo
    new.change_input("Test adding the second todo item");
    assert_eq!(2, ul.as_node().child_nodes().length());

    let label = TestHelper::selector("li:last-child label");
    assert_eq!(
        "Test adding the second todo item",
        label.as_node().text_content().expect("Must have a content")
    );

    let toggle = TestHelper::selector("li:last-child input.toggle");
    toggle.click();
    assert_eq!(false, app.main.borrow().get_app().tasks[0].completed);
    assert_eq!(true, app.main.borrow().get_app().tasks[1].completed);

    // After swapping, the browser is out of sync with app state
    app.main.borrow_mut().get_app_mut().tasks.swap(0, 1);
    assert_eq!(true, app.main.borrow().get_app().tasks[0].completed);
    assert_eq!(false, app.main.borrow().get_app().tasks[1].completed);

    // Add a third todo
    new.change_input("Test update the events");
    assert_eq!(3, ul.as_node().child_nodes().length());

    let label = TestHelper::selector("li:last-child label");
    assert_eq!(
        "Test update the events",
        label.as_node().text_content().expect("Must have a content")
    );

    let toggle = TestHelper::selector("ul.todo-list li:nth-child(2) input.toggle");
    assert_eq!(false, toggle.as_input().checked());
    toggle.click();
    assert_eq!(true, app.main.borrow().get_app().tasks[0].completed);
    assert_eq!(true, app.main.borrow().get_app().tasks[1].completed);
    assert_eq!(false, app.main.borrow().get_app().tasks[2].completed);

    // Remove the second (in current list) todo item
    let destroy = TestHelper::selector("ul.todo-list li:nth-child(2) button.destroy");
    destroy.click();
    assert_eq!(2, ul.as_node().child_nodes().length());
    // The first must be the second added item
    let label = TestHelper::selector("li:first-child label");
    assert_eq!(
        "Test adding the second todo item",
        label.as_node().text_content().expect("Must have a content")
    );
    assert_eq!(true, app.main.borrow().get_app().tasks[0].completed);
    assert_eq!(false, app.main.borrow().get_app().tasks[1].completed);

    let label = TestHelper::selector("li:last-child label");
    assert_eq!(
        "Test update the events",
        label.as_node().text_content().expect("Must have a content")
    );
}
