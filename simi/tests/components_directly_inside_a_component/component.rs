use simi::prelude::*;

use {Msg, Number, YearMonth};

pub struct NumberUi<A: SimiApp> {
    pub snippet_id: Option<&'static str>,
    pub up: ElementEvent<A>,
    pub down: ElementEvent<A>,
    pub name_editor: ComponentChild<A>,
}
impl<A: SimiApp> Component<A> for NumberUi<A> {
    type Properties = Number;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        component!{
            //@debug
            div {
                p (
                    id=#format!("{}-content", self.snippet_id.unwrap())
                ) {
                    props.title ": " props.count
                }
                button (
                    id=#format!("{}-down-button", self.snippet_id.unwrap())
                    onclick=self.down
                ) {
                    "Down"
                }
                button (
                    id=#format!("{}-up-button", self.snippet_id.unwrap())
                    onclick=self.up
                ) {
                    "Up"
                }
            }
            $ self.name_editor
        }
    }
}

pub struct NumberNameUi<A: SimiApp> {
    pub input_id: Option<&'static str>,
    pub onchange: ElementEvent<A>,
}

impl<A: SimiApp> Component<A> for NumberNameUi<A> {
    type Properties = Number;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        component!{
            input (id=#self.input_id value=props.title onchange=self.onchange)
        }
    }
}

pub struct YearMonthUi;

impl<A> Component<A> for YearMonthUi
where
    A: SimiApp<Message = Msg> + 'static,
{
    type Properties = YearMonth;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        component!{
            //@debug
            NumberUi (&props.year) {
                snippet_id: "year"
                up: onclick=Msg::YearUp
                down: onclick=Msg::YearDown
                name_editor: NumberNameUi (&props.year) {
                    input_id: "year-name"
                    onchange: onchange=Msg::YearTitleChange(?)
                }
            }
            NumberUi (&props.month) {
                snippet_id: "month"
                up: onclick=Msg::MonthUp
                down: onclick=Msg::MonthDown
                name_editor: NumberNameUi (&props.month) {
                    input_id: "month-name"
                    onchange: onchange=Msg::MonthTitleChange(?)
                }
            }
        }
    }
}
