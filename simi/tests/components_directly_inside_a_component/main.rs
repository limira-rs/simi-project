#![feature(proc_macro_hygiene)]
#![cfg(target_arch = "wasm32")]

extern crate simi;
extern crate wasm_bindgen;
extern crate wasm_bindgen_test;

use component::*;
use simi::interop::real_dom::Event;
use simi::prelude::*;
use simi::test_helper::TestHelper;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_test::*;

use wasm_bindgen_test::wasm_bindgen_test_configure;
wasm_bindgen_test_configure!(run_in_browser);

pub mod component;

const CONTAINER_ELEMENT_ID: &'static str = "simi-app-container";

#[wasm_bindgen(module = "./tests/simi_app.js")]
extern "C" {
    fn run_app() -> AppHandle;
}

pub struct Number {
    pub count: i32,
    pub title: String,
}

pub struct YearMonth {
    pub year: Number,
    pub month: Number,
}

struct TestApp {
    year_month: YearMonth,
}

pub enum Msg {
    YearUp,
    YearDown,
    YearTitleChange(Event),
    YearSetTitle(String),
    MonthUp,
    MonthDown,
    MonthTitleChange(Event),
    MonthSetTitle(String),
}

impl SimiApp for TestApp {
    type Message = Msg;
    type Context = ();

    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                year_month: YearMonth {
                    year: Number {
                        count: 2018,
                        title: "year".to_string(),
                    },
                    month: Number {
                        count: 9,
                        title: "month".to_string(),
                    },
                },
            },
            (),
        )
    }

    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::YearUp => self.year_month.year.count += 1,
            Msg::YearDown => self.year_month.year.count -= 1,
            Msg::YearTitleChange(event) => self.update_year_title(event),
            Msg::YearSetTitle(value) => self.year_month.year.title = value,
            Msg::MonthUp => self.year_month.month.count += 1,
            Msg::MonthDown => self.year_month.month.count -= 1,
            Msg::MonthTitleChange(event) => self.update_month_title(event),
            Msg::MonthSetTitle(value) => self.year_month.month.title = value,
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        application!{
            div {
                YearMonthUi (&self.year_month)
            }
        }
    }
}

impl TestApp {
    fn update_year_title(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        self.year_month.year.title = input.value();
    }
    fn update_month_title(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        self.year_month.month.title = input.value();
    }
}

#[wasm_bindgen]
pub struct AppHandle {
    main: RcMain<TestApp>,
}

impl SimiHandle<TestApp> for AppHandle {
    fn main(&self) -> RcMain<TestApp> {
        self.main.clone()
    }
}

#[wasm_bindgen]
impl AppHandle {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self {
            main: SimiMain::new_rc_main(),
        }
    }

    pub fn create_container(&self) {
        if ::simi::interop::real_dom::document()
            .get_element_by_id(CONTAINER_ELEMENT_ID)
            .is_some()
        {
            return;
        }
        let div = ::simi::interop::real_dom::document()
            .create_element("div")
            .expect("Create div must succeed");
        div.set_id(CONTAINER_ELEMENT_ID);
        let body = ::simi::interop::real_dom::document()
            .body()
            .expect("Must have a body");
        let node: &::simi::interop::real_dom::Node = body.as_ref();
        ::simi::interop::display_error(node.append_child(div.as_ref()));
    }

    pub fn start(&mut self) {
        self.in_element_id(CONTAINER_ELEMENT_ID);
    }
}

impl AppHandle {
    pub fn start_from_rust(&mut self) {
        self.create_container();
        self.start();
    }
}

#[wasm_bindgen_test]
fn test_nested_snippets() {
    let mut app_handle = run_app();
    test_year(&mut app_handle);
    test_month(&mut app_handle);
}

fn test_year(app: &mut AppHandle) {
    let up = TestHelper::id("year-up-button");
    let down = TestHelper::id("year-down-button");
    let content = TestHelper::id("year-content");

    assert_eq!(
        "year: 2018",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    down.click();
    assert_eq!(
        "year: 2017",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    up.click();
    up.click();
    assert_eq!(
        "year: 2019",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    let input = TestHelper::id("year-name");
    input.change_input("Year");
    assert_eq!(
        "Year: 2019",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    app.main
        .borrow_mut()
        .update(Msg::YearSetTitle("Current year".to_string()));
    assert_eq!(
        "Current year: 2019",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
}

fn test_month(app: &mut AppHandle) {
    let up = TestHelper::id("month-up-button");
    let down = TestHelper::id("month-down-button");
    let content = TestHelper::id("month-content");

    assert_eq!(
        "month: 9",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    down.click();
    assert_eq!(
        "month: 8",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    up.click();
    up.click();
    assert_eq!(
        "month: 10",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    let input = TestHelper::id("month-name");
    input.change_input("Month");
    assert_eq!(
        "Month: 10",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    app.main
        .borrow_mut()
        .update(Msg::MonthSetTitle("Current month".to_string()));
    assert_eq!(
        "Current month: 10",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
}
