use simi::prelude::*;

use {FlowerType, Msg, TestApp};

pub struct TestSnippet<A: SimiApp> {
    pub aquatic_check: ElementEvent<A>,
    pub terrestrial_check: ElementEvent<A>,
    pub orchid_check: ElementEvent<A>,
}

impl<A: SimiApp<Message = Msg> + 'static> Component<A> for TestSnippet<A> {
    type Properties = TestApp;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        let flower_list = props.flower_type.flower_list();
        component!{
            //@debug
            // Move this into if else body to test attaching events (from self.field) inside if else arm
            // div{
            //     input (id="radio-aquatic" type="radio" name="type" checked=props.flower_type==FlowerType::Aquatic onchange=self.aquatic_check)
            //     input (id="radio-terrestrial" type="radio" name="type" checked=props.flower_type==FlowerType::Terrestrial onchange=self.terrestrial_check)
            //     input (id="radio-orchids" type="radio" name="type" checked=props.flower_type==FlowerType::Orchidaceae onchange=self.orchid_check)
            // }
            if props.flower_type == FlowerType::Aquatic {
                div{
                    input (id="radio-aquatic" type="radio" name="type" checked=props.flower_type==FlowerType::Aquatic onchange=self.aquatic_check)
                    input (id="radio-terrestrial" type="radio" name="type" checked=props.flower_type==FlowerType::Terrestrial onchange=self.terrestrial_check)
                    input (id="radio-orchids" type="radio" name="type" checked=props.flower_type==FlowerType::Orchidaceae onchange=self.orchid_check)
                }
                div (id="div-aquatic") {"aquatic flowers"}
                select (id="select-aquatic" value=props.name onchange=Msg::NameChange(?)) {
                    option (id="a-iris" value=flower_list[0]) { #flower_list[0] }
                    option (id="a-lotus" value=flower_list[1]) { #flower_list[1] }
                    option (id="a-water-lily" value=flower_list[2]) { #flower_list[2] }
                }
            }else if props.flower_type == FlowerType::Terrestrial {
                div{
                    input (id="radio-aquatic" type="radio" name="type" checked=props.flower_type==FlowerType::Aquatic onchange=self.aquatic_check)
                    input (id="radio-terrestrial" type="radio" name="type" checked=props.flower_type==FlowerType::Terrestrial onchange=self.terrestrial_check)
                    input (id="radio-orchids" type="radio" name="type" checked=props.flower_type==FlowerType::Orchidaceae onchange=self.orchid_check)
                }
                div (id="div-terrestrial") {"terrestrial flowers"}
                select (id="select-terrestrial" value=props.name onchange=Msg::NameChange(?)) {
                    option (id="a-rose" value=flower_list[0]) { #flower_list[0] }
                    option (id="a-daisy" value=flower_list[1]) { #flower_list[1] }
                    option (id="a-lavender" value=flower_list[2]) { #flower_list[2] }
                }
            }else{
                div{
                    input (id="radio-aquatic" type="radio" name="type" checked=props.flower_type==FlowerType::Aquatic onchange=self.aquatic_check)
                    input (id="radio-terrestrial" type="radio" name="type" checked=props.flower_type==FlowerType::Terrestrial onchange=self.terrestrial_check)
                    input (id="radio-orchids" type="radio" name="type" checked=props.flower_type==FlowerType::Orchidaceae onchange=self.orchid_check)
                }
                div (id="div-orchid") {"orchid flowers"}
                select (id="select-orchid" value=props.name onchange=Msg::NameChange(?)) {
                    option (id="a-cattleya" value=flower_list[0]) { #flower_list[0] }
                    option (id="a-cymbidium" value=flower_list[1]) { #flower_list[1] }
                    option (id="a-vanda" value=flower_list[2]) { #flower_list[2] }
                }
            }
            // Test complex condition expression and phantom else
            if props.flower_type != FlowerType::Aquatic && props.flower_type != FlowerType::Terrestrial {
                p (id="its-an-orchid") { "Neither an aquatic flower nor terrestrial flower? May be we call its an orchid?" }
            }
            // Test order
            div (id="nothing-after-if-else") {
                "It is "
                if props.flower_type == FlowerType::Aquatic{
                    "an aquatic flower"
                }else if props.flower_type == FlowerType::Terrestrial{
                    "a terrestrial flower"
                }else{
                    "an orchid"
                }
            }
            // Test order
            div (id="with-something-after-if-else") {
                "It is "
                if props.flower_type == FlowerType::Aquatic{
                    "an aquatic"
                }else if props.flower_type == FlowerType::Terrestrial{
                    "a terrestrial"
                }else{
                    "an orchid"
                }
                " flower"
            }
        }
    }
}
