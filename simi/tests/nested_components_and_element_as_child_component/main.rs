#![feature(proc_macro_hygiene)]
#![cfg(target_arch = "wasm32")]

extern crate simi;
extern crate wasm_bindgen;
extern crate wasm_bindgen_test;

use component::*;
use simi::interop::real_dom::Event;
use simi::prelude::*;
use simi::test_helper::TestHelper;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_test::*;

use wasm_bindgen_test::wasm_bindgen_test_configure;
wasm_bindgen_test_configure!(run_in_browser);

pub mod component;

const CONTAINER_ELEMENT_ID: &'static str = "simi-app-container";

#[wasm_bindgen(module = "./tests/simi_app.js")]
extern "C" {
    fn run_app() -> AppHandle;
}

pub struct SnippetProps {
    pub count: i32,
    pub title: String,
}

struct TestApp {
    year1: SnippetProps,
    year2: SnippetProps,
    month2: SnippetProps,
    year3: SnippetProps,
}

enum Msg {
    Year1Up,
    Year1Down,
    Year1TitleChange(Event),
    Year1SetTitle(String),
    Year2Up,
    Year2Down,
    Year2TitleChange(Event),
    Year2SetTitle(String),
    Month2Up,
    Month2Down,
    Month2TitleChange(Event),
    Month2SetTitle(String),
    Year3Up,
    Year3Down,
    Year3TitleChange(Event),
    Year3SetTitle(String),
}

impl SimiApp for TestApp {
    type Message = Msg;
    type Context = ();

    fn init(_main: WeakMain<Self>) -> (Self, Self::Context) {
        (
            Self {
                year1: SnippetProps {
                    count: 2018,
                    title: "Birth year".to_string(),
                },
                year2: SnippetProps {
                    count: 3018,
                    title: "Thousand years later".to_string(),
                },
                month2: SnippetProps {
                    count: 9,
                    title: "Month".to_string(),
                },
                year3: SnippetProps {
                    count: 2015,
                    title: "Rust's year of birth".to_string(),
                },
            },
            (),
        )
    }

    fn update(&mut self, m: Msg, _context: &mut Self::Context) -> UpdateView {
        match m {
            Msg::Year1Up => self.year1.count += 1,
            Msg::Year1Down => self.year1.count -= 1,
            Msg::Year1TitleChange(event) => self.update_year1_title(event),
            Msg::Year1SetTitle(value) => self.year1.title = value,
            Msg::Year2Up => self.year2.count += 1,
            Msg::Year2Down => self.year2.count -= 1,
            Msg::Year2TitleChange(event) => self.update_year2_title(event),
            Msg::Year2SetTitle(value) => self.year2.title = value,
            Msg::Month2Up => self.month2.count += 1,
            Msg::Month2Down => self.month2.count -= 1,
            Msg::Month2TitleChange(event) => self.update_month2_title(event),
            Msg::Month2SetTitle(value) => self.month2.title = value,
            Msg::Year3Up => self.year3.count += 1,
            Msg::Year3Down => self.year3.count -= 1,
            Msg::Year3TitleChange(event) => self.update_year3_title(event),
            Msg::Year3SetTitle(value) => self.year3.title = value,
        }
        true
    }

    fn render(&self, context: AppRenderContext<Self>) {
        //use simi::simi_dom::SimiCreator;
        application!{
            //@debug
            div {
                TestSnippet (&self.year1) {
                    snippet_id: "year1"
                    up: onclick=Msg::Year1Up
                    down: onclick=Msg::Year1Down
                    child: TestSnippetChild (&self.year1) {
                        input_id: "new-title-for-year1"
                        onchange: onchange=Msg::Year1TitleChange(?)
                    }
                }
            }
            div {
                ForNestedThreeLevel { // Child level 1
                    child1: TestSnippet (&self.year2) { // Child level 2
                        snippet_id: "year2"
                        up: onclick=Msg::Year2Up
                        down: onclick=Msg::Year2Down
                        child: TestSnippetChild (&self.year2) { // Child level 3
                            input_id: "new-title-for-year2"
                            onchange: onchange=Msg::Year2TitleChange(?)
                        }
                    }
                    child2: TestSnippet (&self.month2) {
                        snippet_id: "month2"
                        up: onclick=Msg::Month2Up
                        down: onclick=Msg::Month2Down
                        child: TestSnippetChild (&self.month2) {
                            input_id: "new-title-for-month2"
                            onchange: onchange=Msg::Month2TitleChange(?)
                        }
                    }
                }
            }
            div {
                TestSnippet (&self.year3) {
                    snippet_id: "year3"
                    up: onclick=Msg::Year3Up
                    down: onclick=Msg::Year3Down
                    child: div {
                        input (id="new-title-for-year3" value=self.year3.title onchange=Msg::Year3TitleChange(?))
                        p (id="rust-year-of-birth1") { #self.year3.title " is always " #self.year3.count }
                        #p (id="rust-year-of-birth2") { self.year3.title " is always " self.year3.count }
                    }
                }
            }
        }
    }
}

impl TestApp {
    fn update_year1_title(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        self.year1.title = input.value();
    }
    fn update_year2_title(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        self.year2.title = input.value();
    }
    fn update_month2_title(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        self.month2.title = input.value();
    }
    fn update_year3_title(&mut self, event: Event) {
        let target = event.target().expect("Must have a target");
        let input: &simi::interop::real_dom::HtmlInputElement = target.unchecked_ref();
        self.year3.title = input.value();
    }
}

#[wasm_bindgen]
pub struct AppHandle {
    main: RcMain<TestApp>,
}

impl SimiHandle<TestApp> for AppHandle {
    fn main(&self) -> RcMain<TestApp> {
        self.main.clone()
    }
}

#[wasm_bindgen]
impl AppHandle {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self {
            main: SimiMain::new_rc_main(),
        }
    }

    pub fn create_container(&self) {
        if ::simi::interop::real_dom::document()
            .get_element_by_id(CONTAINER_ELEMENT_ID)
            .is_some()
        {
            return;
        }
        let div = ::simi::interop::real_dom::document()
            .create_element("div")
            .expect("Create div must succeed");
        div.set_id(CONTAINER_ELEMENT_ID);
        let body = ::simi::interop::real_dom::document()
            .body()
            .expect("Must have a body");
        let node: &::simi::interop::real_dom::Node = body.as_ref();
        ::simi::interop::display_error(node.append_child(div.as_ref()));
    }

    pub fn start(&mut self) {
        self.in_element_id(CONTAINER_ELEMENT_ID);
    }
}

impl AppHandle {
    pub fn start_from_rust(&mut self) {
        self.create_container();
        self.start();
    }
}

#[wasm_bindgen_test]
fn test_nested_snippets() {
    let mut app_handle = run_app();
    test_nested_2_levels(&mut app_handle);
    test_nested_3_levels(&mut app_handle);
    test_element_as_child_snippet(&mut app_handle);
}

fn test_nested_2_levels(app: &mut AppHandle) {
    let up = TestHelper::id("year1-up-button");
    let down = TestHelper::id("year1-down-button");
    let content = TestHelper::id("year1-content");

    assert_eq!(
        "Birth year: 2018",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    down.click();
    assert_eq!(
        "Birth year: 2017",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    up.click();
    up.click();
    assert_eq!(
        "Birth year: 2019",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    let input = TestHelper::id("new-title-for-year1");
    input.change_input("Year of birth");
    assert_eq!(
        "Year of birth: 2019",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    app.main
        .borrow_mut()
        .update(Msg::Year1SetTitle("Current year".to_string()));
    assert_eq!(
        "Current year: 2019",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
}

fn test_nested_3_levels(app: &mut AppHandle) {
    let up = TestHelper::id("year2-up-button");
    let down = TestHelper::id("year2-down-button");
    let content = TestHelper::id("year2-content");

    assert_eq!(
        "Thousand years later: 3018",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    down.click();
    assert_eq!(
        "Thousand years later: 3017",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    up.click();
    up.click();
    assert_eq!(
        "Thousand years later: 3019",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    let input = TestHelper::id("new-title-for-year2");
    input.change_input("A thousand years later");
    assert_eq!(
        "A thousand years later: 3019",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    app.main
        .borrow_mut()
        .update(Msg::Year2SetTitle("One thousand years later".to_string()));
    assert_eq!(
        "One thousand years later: 3019",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    // month2
    let up = TestHelper::id("month2-up-button");
    let down = TestHelper::id("month2-down-button");
    let content = TestHelper::id("month2-content");

    assert_eq!(
        "Month: 9",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    down.click();
    assert_eq!(
        "Month: 8",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    up.click();
    up.click();
    assert_eq!(
        "Month: 10",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    let input = TestHelper::id("new-title-for-month2");
    input.change_input("Expected month");
    assert_eq!(
        "Expected month: 10",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    app.main
        .borrow_mut()
        .update(Msg::Month2SetTitle("Month again".to_string()));
    assert_eq!(
        "Month again: 10",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
}

fn test_element_as_child_snippet(app: &mut AppHandle) {
    let up = TestHelper::id("year3-up-button");
    let down = TestHelper::id("year3-down-button");
    let content = TestHelper::id("year3-content");

    let const1 = TestHelper::id("rust-year-of-birth1");
    let const2 = TestHelper::id("rust-year-of-birth2");

    assert_eq!(
        "Rust's year of birth is always 2015",
        const1
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Rust's year of birth is always 2015",
        const2
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    assert_eq!(
        "Rust's year of birth: 2015",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    down.click();
    assert_eq!(
        "Rust's year of birth: 2014",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    up.click();
    up.click();
    assert_eq!(
        "Rust's year of birth: 2016",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    let input = TestHelper::id("new-title-for-year3");
    input.change_input("Rust is one year old in");
    assert_eq!(
        "Rust is one year old in: 2016",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    app.main
        .borrow_mut()
        .update(Msg::Year3SetTitle("Rust is two years old in".to_string()));
    up.click();
    assert_eq!(
        "Rust is two years old in: 2017",
        content
            .as_node()
            .text_content()
            .expect("Must have a content")
    );

    assert_eq!(
        "Rust's year of birth is always 2015",
        const1
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
    assert_eq!(
        "Rust's year of birth is always 2015",
        const2
            .as_node()
            .text_content()
            .expect("Must have a content")
    );
}
