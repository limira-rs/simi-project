use simi::prelude::*;

use SnippetProps;

pub struct TestSnippet<A: SimiApp> {
    pub up: ElementEvent<A>,
    pub down: ElementEvent<A>,

    pub input_changed: ElementEvent<A>,
    pub check_changed: ElementEvent<A>,

    pub select_value_changed: ElementEvent<A>,
    pub select_index_changed: ElementEvent<A>,
}

impl<A: SimiApp> Component<A> for TestSnippet<A> {
    type Properties = SnippetProps;
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>) {
        let future: bool = props.count > 2018;
        let class_name1 = "future-expr1";
        let class_name2 = "future-expr2";
        component! {
            //@debug
            button (id="down-button" onclick=self.down) { "Down" }
            button (id="up-button" onclick=self.up) { "Up" }

            div (id="no-update-by-snippet") { #props.text " " #props.count }
            div (id="content-div") { props.text " " props.count }

            div (id="classes"
                "snippet-class"=props.class_on
                "future-literal"=future
                props.class1?=props.class_on
                props.class2?=future
                class_name1?=props.class_on
                class_name2?=future
            )

            input (id="input-text-value-no-update" value=#props.input_value)
            input (id="input-text-value" value=props.input_value onchange=self.input_changed)

            input (id="no-update-attribute-by-snippet" type="checkbox" checked=#props.checked alt=#props.input_alt)
            input (id="current-checked" type="checkbox" autofocus=props.autofocus checked=props.checked onchange=self.check_changed alt=props.input_alt)

            span (id="custom-attribute" data-no-update=#props.custom_attribute data-custom-data=props.custom_attribute) {"..?.."}

            select (id="select-value" value=props.select_value onchange=self.select_value_changed) {
                option (id="select-value-option1" value="first") { "First option" }
                option (id="select-value-option2" value="second") { "Second option" }
                option (id="select-value-option3" value="third") { "Third option" }
            }

            select (id="select-index" index=props.select_index onchange=self.select_index_changed) {
                option (id="select-index-option1" value="first") { "First option" }
                option (id="select-index-option2" value="second") { "Second option" }
                option (id="select-index-option3" value="third") { "Third option" }
            }
        }
    }
}
