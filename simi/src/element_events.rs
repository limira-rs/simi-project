//! This module implement structs to manage every events emit by an html element.

// We need to create a struct for an event because we want to put all of them into
// a vec by using a trait. It is possible to implement one struct for all events
// by using generic, but they cannot be putted inside a single vec because each
// event requires a different parameter type.
use wasm_bindgen::closure::Closure;
use wasm_bindgen::JsCast;

use app::{SimiApp, WeakMain};
use interop::real_dom::RealElement;
use HashMap;

/// Holders will be used as a handle to store (to prevent it from being dropped)
/// the actual listener that is attached to an element.
pub trait ListenerHolder {
    /// Remove the listener from element
    fn remove(&self, element: &RealElement);
}
/// A list of events that is currently attached to relevant element.
//pub type ListenerHolders = Vec<Box<ListenerHolder>>;
pub type ListenerHolders = HashMap<&'static str, Box<ListenerHolder>>;

/// Trait for events supported by simi
pub trait SimiElementEvent<A: SimiApp> {
    /// Add the listener to element, then store its holder to prevent dropping.
    /// After this, self will be dropped.
    // We need to use `&mut self` here because later we access this via a Box<SimiElementEvent>
    // If we use `self` to move itself, the compiler complains about unknown size at compile time.
    fn add_listener_to(
        &mut self,
        element: &RealElement,
        main: WeakMain<A>,
        holders: &mut ListenerHolders,
    );
    /// Update the listener for the element, then store its holder to prevent dropping.
    /// After this, self will be dropped.
    // We need to use `&mut self` here because later we access this via a Box<SimiElementEvent>
    // If we use `self` to move itself, the compiler complains about unknown size at compile time.
    fn update_listener_for(
        &mut self,
        element: &RealElement,
        main: WeakMain<A>,
        holders: &mut ListenerHolders,
    );
}
/// Element event
pub type ElementEvent<A> = ::std::cell::Cell<Option<Box<SimiElementEvent<A>>>>;
/// Events for an element
pub type Events<A> = Vec<Box<SimiElementEvent<A>>>;

// Learn from yew
macro_rules! impl_event {
    ( $(($Type:ident, $ListenerHolder:ident))+ ) => {
        $(
        /// Holder for $event listener
        pub(crate) struct $ListenerHolder {
            event_name: &'static str,
            closure: Closure<Fn(super::interop::real_dom::$Type)>,
        }
        impl ListenerHolder for $ListenerHolder {
            fn remove(&self, element: &RealElement) {
                let element: &::interop::real_dom::EventTarget = element.as_ref();
                ::interop::display_error(element.remove_event_listener_with_callback(self.event_name, self.closure.as_ref().unchecked_ref()));
            }
        }

        /// $event event
        pub struct $Type<F> {
            event_name: &'static str,
            // We need to put F in an Option here, because later we access it via trait SimiElementEvent.
            // Using Option here allow we to take out the value later easily.
            // See explanation from trait SimiElementEvent for more info.
            handler: Option<F>,
        }

        impl<F> $Type<F> {
            /// Create a new $event event from a closure handler
            pub fn new(event_name: &'static str, handler: F) -> Self {
                Self {
                    event_name,
                    handler: Some(handler),
                }
            }
        }

        impl<F, A> SimiElementEvent<A> for $Type<F>
        where
            A: SimiApp + 'static,
            F: Fn(super::interop::real_dom::$Type) -> A::Message + 'static,
        {
            /// Add the listener to element, then store its holder to prevent dropping.
            /// After this, self will be dropped.
            fn add_listener_to(
                &mut self,
                element: &RealElement,
                main: WeakMain<A>,
                holders: &mut ListenerHolders,
            ) {
                let handler = self.handler.take().unwrap();
                let handler = move |val: super::interop::real_dom::$Type| match main.upgrade() {
                    Some(main) => match main.try_borrow_mut() {
                        Ok(mut a) => {
                            a.update(handler(val))
                        },
                        Err(e) => error!("{}", e)
                    },
                    None => ::error::log_upgrade_main_failed(),
                };
                let closure = Closure::wrap(Box::new(handler) as Box<Fn(super::interop::real_dom::$Type)>);
                let element: &::interop::real_dom::EventTarget = element.as_ref();
                ::interop::display_error(element.add_event_listener_with_callback(self.event_name, closure.as_ref().unchecked_ref()));
                holders.insert(self.event_name, Box::new($ListenerHolder {event_name: self.event_name, closure: closure }));
            }

            /// Update the listener for the element, then store its holder to prevent dropping.
            /// After this, self will be dropped.
            fn update_listener_for(
                &mut self,
                element: &RealElement,
                main: WeakMain<A>,
                holders: &mut ListenerHolders,
            ) {
                if let Some(listener) = holders.get(self.event_name) {
                    listener.remove(element);
                }
                self.add_listener_to(element, main, holders);
            }
        }
        )+
    };
}

impl_event!{
    (FocusEvent, FocusEventHolder)
    (MouseEvent, MouseEventHolder)
    (WheelEvent, WheelEventHolder)
    (UiEvent, UiEventHolder)
    (InputEvent, InputEventHolder)
    (KeyboardEvent, KeyboardEventHolder)
    (Event, EventHolder)
}

macro_rules! impl_element_events_that_call_a_context_method {
    ( $(($DomEventType:ident, $ContextType:ident, $ListenerHolder:ident))+ ) => {
        $(
        /// Holder for $event listener
        pub(crate) struct $ListenerHolder {
            event_name: &'static str,
            closure: Closure<Fn(super::interop::real_dom::$DomEventType)>,
        }
        impl ListenerHolder for $ListenerHolder {
            fn remove(&self, element: &RealElement) {
                let element: &::interop::real_dom::EventTarget = element.as_ref();
                ::interop::display_error(element.remove_event_listener_with_callback(self.event_name, self.closure.as_ref().unchecked_ref()));
            }
        }

        /// $event event
        pub struct $ContextType<F> {
            event_name: &'static str,
            // We need to put F in an Option here, because later we access it via trait SimiElementEvent.
            // Using Option here allow we to take out the value later easily.
            // See explanation from trait SimiElementEvent for more info.
            handler: Option<F>,
        }

        impl<F> $ContextType<F> {
            /// Create a new $event event from a closure handler
            pub fn new(event_name: &'static str, handler: F) -> Self {
                Self {
                    event_name,
                    handler: Some(handler),
                }
            }
        }

        impl<F,A> SimiElementEvent<A> for $ContextType<F>
        where A: SimiApp + 'static,
            F: Fn(&mut A::Context) + 'static
        {
            fn add_listener_to(
                &mut self,
                element: &RealElement,
                main: WeakMain<A>,
                holders: &mut ListenerHolders,
            ) {
                let handler = self.handler.take().unwrap();
                let handler = move |_val: super::interop::real_dom::$DomEventType| match main.upgrade() {
                    Some(main) => match main.try_borrow_mut() {
                        Ok(mut a) => {
                            handler(a.get_app_context_mut());
                        },
                        Err(e) => error!("{}", e)
                    },
                    None => ::error::log_upgrade_main_failed(),
                };
                let closure = Closure::wrap(Box::new(handler) as Box<Fn(super::interop::real_dom::$DomEventType)>);
                let element: &::interop::real_dom::EventTarget = element.as_ref();
                ::interop::display_error(element.add_event_listener_with_callback(self.event_name, closure.as_ref().unchecked_ref()));
                holders.insert(self.event_name, Box::new($ListenerHolder {event_name: self.event_name, closure: closure }));
            }
            fn update_listener_for(
                &mut self,
                element: &RealElement,
                main: WeakMain<A>,
                holders: &mut ListenerHolders,
            ) {
                if let Some(listener) = holders.get(self.event_name) {
                    listener.remove(element);
                }
                self.add_listener_to(element, main, holders);
            }
        }
        )+
    };
}

impl_element_events_that_call_a_context_method!{
    (FocusEvent, ContextFocusEvent, ContextFocusEventHolder)
    (MouseEvent, ContextMouseEvent, ContextMouseEventHolder)
    (WheelEvent, ContextWheelEvent, ContextWheelEventHolder)
    (UiEvent, ContextUiEvent, ContextUiEventHolder)
    (InputEvent, ContextInputEvent, ContextInputEventHolder)
    (KeyboardEvent, ContextKeyboardEvent, ContextKeyboardEventHolder)
    (Event, ContextEvent, ContextEventHolder)
}
