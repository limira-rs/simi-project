//! Abstracting over fetch API for Simi framework
use error::ErrorString;
use futures::{future, Future};
pub use js_sys::Promise;
use serde::{Deserialize, Serialize};
use wasm_bindgen::{JsCast, JsValue};
use web_sys::{self, Request, RequestInit, Response};

pub use wasm_bindgen_futures::{future_to_promise, JsFuture};
pub use web_sys::RequestMode;

use callback::CallbackArg;

/// fetch builder
pub struct FetchBuilder {
    method: String,
    url: String,
    content_type: Option<String>,
    accept_type: Option<String>,
    request_init: RequestInit,
}

impl FetchBuilder {
    /// New builder with default arguments:
    /// * method: GET
    pub fn new(url: &str) -> Self {
        Self {
            method: "GET".to_string(),
            content_type: None,
            accept_type: None,
            url: url.to_string(),
            request_init: RequestInit::new(),
        }
    }

    fn init_fetch(self) -> Result<Promise, ErrorString> {
        let FetchBuilder {
            method,
            url,
            content_type,
            accept_type,
            mut request_init,
        } = self;
        request_init.method(&method);
        let req = Request::new_with_str_and_init(&url, &request_init)?;
        if let Some(content_type) = content_type {
            req.headers().set("Content-Type", &content_type)?;
        }
        if let Some(accept_type) = accept_type {
            req.headers().set("Accept", &accept_type)?;
        }

        let win = web_sys::window().ok_or(ErrorString::from("There is no web_sys::window"))?;
        Ok(win.fetch_with_request(&req))
    }

    #[cfg(feature = "fetch_json")]
    /// Set body content in JSON with provided data, method will setted to POST
    pub fn body_json<S: Serialize>(&mut self, data: &S) -> Result<(), ErrorString> {
        self.method = "POST".to_string();
        self.content_type = Some("application/json".to_string());

        let data = ::serde_json::to_string(data)?;
        let data = JsValue::from(data);
        self.request_init.body(Some(&data));
        Ok(())
    }

    #[cfg(feature = "fetch_json")]
    /// Fetch and parse data as JSON format
    pub fn fetch_json<R>(
        mut self,
        cb: CallbackArg<Result<R, ErrorString>>,
    ) -> Result<Promise, ErrorString>
    where
        for<'a> R: Deserialize<'a> + 'static,
    {
        self.accept_type = Some("application/json".to_string());

        let cb1 = cb.clone();
        let p = self.init_fetch()?;
        let rs = JsFuture::from(p)
            .and_then(|res| match res.dyn_into::<Response>() {
                Ok(res) => {
                    if res.ok() {
                        res.text()
                    } else {
                        Err(JsValue::from(res.status_text()))
                    }
                }
                Err(e) => Err(JsValue::from(e)),
            })
            .and_then(|promise| JsFuture::from(promise))
            .and_then(move |text| {
                match text.as_string() {
                    Some(text) => match ::serde_json::from_str(&text) {
                        Ok(r) => cb(Ok(r)),
                        Err(e) => cb(Err(ErrorString::from(e))),
                    },
                    None => cb(Err(ErrorString::from("Response value is not a text?"))),
                }
                future::ok(JsValue::null())
            })
            .or_else(move |e| {
                cb1(Err(ErrorString::new(
                    e.as_string()
                        .unwrap_or("No message for the error".to_string()),
                )));
                future::ok::<JsValue, JsValue>(JsValue::null())
            });
        Ok(future_to_promise(rs))
    }
}
