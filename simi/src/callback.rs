//! Create callbacks that send messages back to a simi app
use std::rc::Rc;

use app::{SimiApp, WeakMain};

/// A callback that send a message without data
pub type Callback = Rc<Fn()>;
/// A callback that send a message with data
pub type CallbackArg<T> = Rc<Fn(T)>;

/// Create callback for a handler that requires no argument
pub fn no_arg<A, H>(main: WeakMain<A>, handler: H) -> Callback
where
    A: SimiApp + 'static,
    H: Fn() -> A::Message + 'static,
{
    Rc::new(move || match main.upgrade() {
        None => ::error::log_upgrade_main_failed(),
        Some(main) => main.borrow_mut().update(handler()),
    })
}

/// Create callback for a handler that requires an argument
pub fn with_arg<A, T, H>(main: WeakMain<A>, handler: H) -> CallbackArg<T>
where
    A: SimiApp + 'static,
    H: Fn(T) -> A::Message + 'static,
{
    Rc::new(move |value: T| match main.upgrade() {
        None => ::error::log_upgrade_main_failed(),
        Some(main) => main.borrow_mut().update(handler(value)),
    })
}
