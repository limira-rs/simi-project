//! Provide structs and functions to help testing simi apps.
//! Maybe move into its own crate?
use interop::real_dom;
use wasm_bindgen::JsCast;

/// Element tester
pub struct TestHelper {
    /// The element in real dom to test
    element: real_dom::Element,
}

impl TestHelper {
    /// Return true if there is no element with id=element_id
    pub fn no_id(element_id: &str) -> bool {
        real_dom::document()
            .get_element_by_id(element_id)
            .map_or(true, |_| false)
    }

    /// Create a new TestHelper
    pub fn element(element: real_dom::Element) -> TestHelper {
        TestHelper { element }
    }

    /// Create a new TestHelper from element_id
    pub fn id(element_id: &str) -> TestHelper {
        TestHelper {
            element: real_dom::document()
                .get_element_by_id(element_id)
                .unwrap_or_else(|| panic!("No element with id = '{}'", element_id)),
        }
    }

    /// Query an element by a selector string, panic if none
    pub fn selector(s: &str) -> TestHelper {
        TestHelper {
            element: real_dom::document().query_selector(s).expect(s).expect(s),
        }
    }

    /// As a reference to real_dom::EventTarget
    pub fn as_event_target(&self) -> &real_dom::EventTarget {
        self.element.as_ref()
    }

    /// As a reference to real_dom::Node
    pub fn as_node(&self) -> &real_dom::Node {
        self.element.as_ref()
    }

    /// Reference to the element
    pub fn as_element(&self) -> &real_dom::Element {
        &self.element
    }

    /// As a reference to real_dom::HtmlInputElement
    pub fn as_input(&self) -> &real_dom::HtmlInputElement {
        self.element.unchecked_ref()
    }

    /// As a reference to real_dom::HtmlSelectElement
    pub fn as_select(&self) -> &real_dom::HtmlSelectElement {
        self.element.unchecked_ref()
    }

    /// As a reference to real_dom::HtmlOptionElement
    pub fn as_option(&self) -> &real_dom::HtmlOptionElement {
        self.element.unchecked_ref()
    }

    /// As a reference to real_dom::HtmlTextAreaElement
    pub fn as_textarea(&self) -> &real_dom::HtmlTextAreaElement {
        self.element.unchecked_ref()
    }

    /// Simulate a click on the element
    pub fn click(&self) {
        let clickable: &real_dom::HtmlElement = self.element.unchecked_ref();
        clickable.click();
    }

    /// Set input element value then trigger an input event
    pub fn input_input(&self, value: &str) {
        self.as_input().set_value(value);
        ::interop::display_error(
            self.as_event_target().dispatch_event(
                real_dom::InputEvent::new("input")
                    .expect("Create input event must succeed")
                    .as_ref(),
            ),
        );
    }

    /// Set input element value then trigger a change event
    pub fn change_input(&self, value: &str) {
        self.as_input().set_value(value);
        ::interop::display_error(
            self.as_event_target().dispatch_event(
                real_dom::InputEvent::new("change")
                    .expect("Create change event must succeed")
                    .as_ref(),
            ),
        );
    }

    /// Set select element value then trigger a change event
    pub fn change_select(&self, value: &str) {
        self.as_select().set_value(value);
        ::interop::display_error(self.as_event_target().dispatch_event(
            &real_dom::Event::new("change").expect("Create change event must succeed"),
        ));
    }

    /// Set select element index then trigger a change event
    pub fn change_index(&self, index: usize) {
        self.as_select().set_selected_index(index as i32);
        ::interop::display_error(self.as_event_target().dispatch_event(
            &real_dom::Event::new("change").expect("Create change event must succeed"),
        ));
    }

    /// Set textarea element value then trigger a change event
    pub fn change_textarea(&self, value: &str) {
        self.as_textarea().set_value(value);
        ::interop::display_error(self.as_event_target().dispatch_event(
            &real_dom::Event::new("change").expect("Create change event must succeed"),
        ));
    }
}
