//! A simple framework for building front-end apps in Rust. Inspires by [Yew](https://github.com/DenisKolodin/yew).
//!
//! If this is the first time you use Simi, you may want to have a look at the [README](https://gitlab.com/limira-rs/simi-project/blob/master/simi/README.md).
//! Otherwise, you may want to consult the [CHANGELOG](https://gitlab.com/limira-rs/simi-project/blob/master/simi/CHANGELOG.md) to find out what's new in recent versions.
//!
//! ## Important:
//! `simi` only works with a set of supported html tags. If `simi` found out that
//! an identifier is not a supported html tag, (and if its name is not in CamelCase)
//! it will automatically be treated it as an expression and render as it as a text node.
//! If you believe there are valid html tags that must be supported by `simi` please
//! [open an issue](https://gitlab.com/limira-rs/simi-project/issues).
//!
#![deny(missing_docs)]
extern crate fnv;
#[macro_use]
extern crate log;
extern crate js_sys;
extern crate wasm_bindgen;
extern crate web_sys;

#[cfg(feature = "fetch")]
extern crate futures;
#[cfg(feature = "fetch_json")]
extern crate serde;
#[cfg(feature = "fetch_json")]
extern crate serde_json;
#[cfg(feature = "fetch")]
extern crate wasm_bindgen_futures;

pub extern crate simi_html_tags as html_tags;
extern crate simi_macros;

#[cfg(test)]
extern crate wasm_bindgen_test;
#[cfg(test)]
use wasm_bindgen_test::wasm_bindgen_test_configure;
#[cfg(test)]
wasm_bindgen_test_configure!(run_in_browser);

type HashMap<K, V> = fnv::FnvHashMap<K, V>;

pub mod app;
pub mod callback;
pub mod element_events;
pub mod error;
#[cfg(feature = "fetch")]
pub mod fetch;
#[macro_use]
pub mod interop;
pub mod simi_dom;
pub mod test_helper;

pub mod prelude {
    //! Common use items for a simi app
    pub use app::*;
    pub use element_events::ElementEvent;
    pub use error::ErrorString as SimiError;
    pub use simi_dom::NodeList;
    pub use simi_macros::*;
}
