//! Framework for the Application

use error::*;
use interop::real_dom::{self, RealElement, RealNode};
use simi_dom::NodeList;
use std::cell::{Cell, RefCell, RefMut};
use std::rc::{Rc, Weak};

/// Indicate if we should update the ui or not
pub type UpdateView = bool;
/// Simpler name for weak link to a SimiMain
pub type WeakMain<A> = Weak<RefCell<SimiMain<A>>>;
/// Simpler name for rc link to a SimiMain
pub type RcMain<A> = Rc<RefCell<SimiMain<A>>>;
/// Component child
pub type ComponentChild<A> = Cell<Option<::simi_dom::WeakComponentChild<A>>>;

/// This trait will use your Struct/Enum as a Data State in a simi app
pub trait SimiApp: Sized {
    /// Message manage by the app
    type Message;
    /// Environment for the app
    type Context;
    /// Init the new app state and context with main
    fn init(main: WeakMain<Self>) -> (Self, Self::Context);
    /// Update the app state
    fn update(&mut self, m: Self::Message, context: &mut Self::Context) -> UpdateView;
    /// Render a simi app using self as the app state.
    /// Note that the parameter must be named `context`.
    /// You should not rename its name, otherwise, it will fail to compile.
    fn render(&self, context: AppRenderContext<Self>);
    /// Raise when mount done
    fn mounted(&mut self, _main: WeakMain<Self>, _context: &mut Self::Context) {
        // Do nothing in the default method
    }
}

/// Trait for a simi's component
pub trait Component<A: SimiApp>: Sized {
    /// Properties of a Component
    type Properties;
    /// Render self, a component, using the given properties.
    /// Note that the parameter must be named `context`.
    /// You should not rename its name, otherwise, it will fail to compile.
    fn render(&self, props: &Self::Properties, context: CompRenderContext<A>);
}

/// A container for ubu components in an app
pub struct UbuComponent<A: SimiApp, C: Clone + Component<A>> {
    root: RealElement,
    node_list: NodeList<A>,
    main: WeakMain<A>,
    component: C,
}

impl<A: SimiApp, C: Clone + Component<A>> UbuComponent<A, C> {
    /// Create a new ubu component
    pub fn new(main: WeakMain<A>, component: C) -> Self {
        Self {
            root: real_dom::document()
                .body()
                .expect("Must have a body")
                .into(),
            node_list: NodeList::new(),
            main,
            component,
        }
    }

    /// Update component with the given props
    pub fn update(&mut self, props: &C::Properties) {
        let comp = self.component.clone();
        let context =
            CompRenderContext::new(&mut self.node_list, &self.root, self.main.clone(), None);
        comp.render(props, context);
    }

    /// Render the component
    pub fn render(&mut self, real_element: &RealElement, props: &C::Properties) {
        self.root = real_element.clone();
        let comp = self.component.clone();
        let context =
            CompRenderContext::new(&mut self.node_list, &self.root, self.main.clone(), None);
        comp.render(props, context);
    }
}

struct SimiContext<A: SimiApp> {
    app: A,
    main: WeakMain<A>,
    app_context: A::Context,
}

/// Manage a Simi app
pub struct SimiMain<A: SimiApp> {
    // Where to mount `simi` app
    root: RealElement,
    // Virtual dom of the simi app
    root_list: NodeList<A>,
    // The user app, and a link to Self (SimiMain) via std::rc::Weak
    context: Option<SimiContext<A>>,
}

impl<A> SimiMain<A>
where
    A: SimiApp + 'static,
{
    /// Create the main struct for the app
    pub fn new_rc_main() -> RcMain<A> {
        Rc::new(RefCell::new(Self {
            root: real_dom::document()
                .body()
                .expect("Must have a body")
                .into(),
            root_list: NodeList::new(),
            context: None,
        }))
    }
    pub(crate) fn init_app(&mut self, main: WeakMain<A>) {
        let (app, app_context) = A::init(main.clone());
        self.context = Some(SimiContext {
            app,
            main,
            app_context,
        });
    }
    /// Starts the app. The app content will be attached to the element with id=element_id
    pub fn start_in(&mut self, element: RealElement) {
        self.root = element;
        self.start();
    }

    /// Starts the app. The app content will be attached to `document.body`
    fn start(&mut self) {
        // What is the best way to clear the content of an html element?
        self.root.set_inner_html("");
        self.first_render();
    }

    /// Render the app without attaching to a real element
    fn first_render(&mut self) {
        let SimiMain {
            ref mut root_list,
            ref mut context,
            root,
        } = self;
        match context {
            None => {
                root.set_inner_html(
                    "Unable to start the app: <code>self.weak_main</code> is not setted!",
                );
                return;
            }
            Some(simi_context) => {
                {
                    let render_context = AppRenderContext::new(
                        root_list,
                        root,
                        &mut simi_context.app_context,
                        simi_context.main.clone(),
                    );
                    simi_context.app.render(render_context);
                }
                simi_context
                    .app
                    .mounted(simi_context.main.clone(), &mut simi_context.app_context);
            }
        };
    }

    /// Update the app state, then UpdateView if it return UpdateView = true
    pub fn update(&mut self, m: A::Message) {
        let SimiMain {
            ref mut root_list,
            ref mut context,
            root,
        } = self;
        match context {
            None => {
                root.set_inner_html(
                    "Unable to start the app: <code>self.weak_main</code> is not setted!",
                );
                return;
            }
            Some(simi_context) => {
                if !simi_context.app.update(m, &mut simi_context.app_context) {
                    return;
                }
                let render_context = AppRenderContext::new(
                    root_list,
                    root,
                    &mut simi_context.app_context,
                    simi_context.main.clone(),
                );
                simi_context.app.render(render_context);
            }
        };
    }

    /// Get a reference to the app instance, use for testing
    pub fn get_app(&self) -> &A {
        match self.context {
            None => panic!("App not stated yet"),
            Some(ref context) => &context.app,
        }
    }

    /// Get a mut reference to the app instance, use for testing
    pub fn get_app_mut(&mut self) -> &mut A {
        match self.context {
            None => panic!("App not stated yet"),
            Some(ref mut context) => &mut context.app,
        }
    }

    /// Get a mutable reference to the app's context instance
    pub fn get_app_context_mut(&mut self) -> &mut A::Context {
        match self.context {
            None => panic!("App not started yet"),
            Some(ref mut context) => &mut context.app_context,
        }
    }
}

/// Context use for render!
pub struct CompRenderContext<'a, A: 'a + SimiApp> {
    /// List to store root-elements of render!
    pub root_list: Cell<Option<&'a mut NodeList<A>>>,
    /// Real element
    pub real_element: &'a RealElement,
    /// Apply for component child: Its child must be insert before this
    insert_before: Option<RealElement>,
    /// Link to the SimiMain
    pub main: WeakMain<A>,
}

impl<'a, A: 'a + SimiApp> CompRenderContext<'a, A> {
    /// New context
    pub fn new(
        root_list: &'a mut NodeList<A>,
        real_element: &'a RealElement,
        main: WeakMain<A>,
        insert_before: Option<RealElement>,
    ) -> Self {
        Self {
            root_list: Cell::new(Some(root_list)),
            real_element,
            main,
            insert_before,
        }
    }

    /// Insert before
    pub fn insert_before_node(&self) -> Option<&RealNode> {
        self.insert_before.as_ref().map(|ib| ib.as_ref())
    }

    /// Take the root_list
    pub fn take_root_list(&self) -> &'a mut NodeList<A> {
        self.root_list
            .replace(None)
            .expect("Root list is already taken")
    }
}

/// Context use for render!
pub struct AppRenderContext<'a, A: 'a + SimiApp> {
    /// List to store root-elements of render!
    pub root_list: Cell<Option<&'a mut NodeList<A>>>,
    /// Real element
    pub real_element: &'a RealElement,
    /// Link to the SimiMain
    pub main: WeakMain<A>,
    /// The app's context
    pub app_context: &'a mut A::Context,
}

impl<'a, A: 'a + SimiApp> AppRenderContext<'a, A> {
    /// New context
    pub fn new(
        root_list: &'a mut NodeList<A>,
        real_element: &'a RealElement,
        app_context: &'a mut A::Context,
        main: WeakMain<A>,
    ) -> Self {
        Self {
            root_list: Cell::new(Some(root_list)),
            real_element,
            main,
            app_context,
        }
    }

    /// Take the root_list
    pub fn take_root_list(&self) -> &'a mut NodeList<A> {
        self.root_list
            .replace(None)
            .expect("Root list is already taken")
    }
}

/// Trait for the AppHandle that is export to js
pub trait SimiHandle<A: SimiApp + 'static> {
    /// User's app handle must return RcMain
    fn main(&self) -> RcMain<A>;

    #[doc(hidden)]
    fn init_app(&self) {
        let main = self.main();
        let wmain = Rc::downgrade(&main);
        main.borrow_mut().init_app(wmain);
    }

    /// Start the app as the only child of the body
    fn in_body(&self) {
        self.init_app();
        let main = self.main();
        main.borrow_mut().start();
    }

    /// Start the app as the only child of element=element_id
    fn in_element_id(&self, element_id: &str) {
        self.init_app();
        let main = self.main();
        let element = real_dom::document()
            .get_element_by_id(element_id)
            .unwrap_or_else(|| panic!("No element with id = '{}'", element_id));
        main.borrow_mut().start_in(element);
    }

    /// Start the app as the only child of element
    fn in_element(&self, element: &RealElement) {
        self.init_app();
        let main = self.main();
        main.borrow_mut().start_in(element.clone());
    }
}

impl<A> SimiMain<A>
where
    A: SimiApp + 'static,
{
    /// Context mut
    pub fn context_mut(&mut self) -> Result<&mut A::Context, ErrorString> {
        match self.context {
            Some(ref mut context) => Ok(&mut context.app_context),
            None => Err(ErrorString::from(
                "No context. The app must be started before getting its context",
            )),
        }
    }
}

/// Sub app
pub struct SimiSubApp<A: SimiApp + 'static>(RcMain<A>);

impl<A: SimiApp + 'static> Default for SimiSubApp<A> {
    fn default() -> Self {
        Self::new()
    }
}

impl<A: SimiApp + 'static> SimiHandle<A> for SimiSubApp<A> {
    fn main(&self) -> RcMain<A> {
        self.0.clone()
    }
}

impl<A: SimiApp> SimiSubApp<A> {
    /// New sub app
    pub fn new() -> Self {
        SimiSubApp(SimiMain::new_rc_main())
    }

    /// Set and attach to the real element
    pub fn set_real_parent_element(&self, real_element: &RealElement) {
        self.in_element(real_element);
    }

    /// Main mut
    pub fn main_mut(&self) -> RefMut<SimiMain<A>> {
        self.0.try_borrow_mut().expect("Unable to borrow mut")
    }
}
