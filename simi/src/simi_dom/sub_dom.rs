/// Type of a sub dom
/// This is used for context sub app or context ubu component
pub enum SubDomType {
    /// It is a component
    Component,
    /// It is a sub app
    SubApp,
}

/// Reference to an independent sub dom such as a Component or a SubApp
/// It is now just a placeholder, there nothing special. Should it be replaced
/// by a unit struct?? Or maybe completely removed??
pub struct SubDom {
    _sub_dom_type: SubDomType,
}

impl SubDom {
    /// New component node
    pub fn new(_sub_dom_type: SubDomType) -> Self {
        Self { _sub_dom_type }
    }
}
