use interop::real_dom::{self, RealElement};

/// Text
pub struct Text {
    text: String,
    real_element: real_dom::Text,
}

impl Text {
    /// New Text node
    pub fn new(text: String) -> Self {
        let real_element = real_dom::document().create_text_node(&text);
        Self { text, real_element }
    }

    /// Attach the first real element to the given parent
    pub fn append_to(&self, parent: &RealElement) {
        let parent: &::interop::real_dom::Node = parent.as_ref();
        ::interop::display_error(parent.append_child(self.real_element.as_ref()));
    }

    /// Attach the first real element to the given parent
    pub fn insert_to(&self, parent: &RealElement, before: Option<&real_dom::Node>) {
        let parent: &::interop::real_dom::Node = parent.as_ref();
        ::interop::display_error(parent.insert_before(self.real_element.as_ref(), before));
    }

    /// Update the text content if it is new
    pub fn update_text(&mut self, text: String) {
        if self.text != text {
            self.text = text;
            let real_node: &::interop::real_dom::Node = self.real_element.as_ref();
            real_node.set_text_content(Some(&self.text));
        }
    }

    // Remove real node from real dom and return the next sibling
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &real_dom::Node,
    ) -> Option<RealElement> {
        use wasm_bindgen::JsCast;
        let next_sibling: Option<RealElement> = {
            let node: &real_dom::Node = self.real_element.as_ref();
            match node.next_sibling() {
                None => None,
                Some(next) => Some(next.unchecked_into()),
            }
        };
        ::interop::display_error(real_parent.remove_child(self.real_element.as_ref()));
        next_sibling
    }

    // Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &real_dom::Node) {
        ::interop::display_error(real_parent.remove_child(self.real_element.as_ref()));
    }
}
