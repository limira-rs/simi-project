use super::{NodeList, SpecialAttributes};
use app::{SimiApp, WeakMain};
use element_events::{Events, ListenerHolders};
use html_tags::HtmlTag;
use interop::real_dom::{self, RealElement};
use HashMap;

/// Shared things for all elements
pub struct Element<A: SimiApp, M: Default + SpecialAttributes> {
    /// Element tag
    tag: &'static str,
    /// Equivalent tag in enum
    /// Should remove this?
    etag: HtmlTag,
    /// Attributes that may change in the future
    attributes: HashMap<&'static str, String>,
    /// Classes that are on/off depend on a condition
    classes: HashMap<&'static str, bool>,
    expr_classes: HashMap<String, bool>,
    /// Holders for attached events
    listeners: ListenerHolders,
    /// Real node in real dom
    real_element: RealElement,
    /// Child nodes of this element
    nodes: NodeList<A>,
    /// More attributes for some special elements such as `<select>`, `<input>`
    pub more: M,
}

impl<A: SimiApp, M: Default + SpecialAttributes> Element<A, M> {
    /// New element from html tag
    pub fn new(tag: &'static str, etag: HtmlTag) -> Self {
        let real_element = real_dom::document()
            .create_element(tag)
            .expect("Create element with tag checked by simi must succeed");
        Self {
            tag,
            etag,
            attributes: HashMap::default(),
            classes: HashMap::default(),
            expr_classes: HashMap::default(),
            listeners: HashMap::default(),
            real_element,
            nodes: NodeList::new(),
            more: M::default(),
        }
    }

    /// Html tag
    pub fn tag(&self) -> &'static str {
        self.tag
    }

    /// enum HtmlTag
    pub fn etag(&self) -> HtmlTag {
        self.etag
    }

    /// Reference to nodes of the list
    pub fn nodes(&self) -> &NodeList<A> {
        &self.nodes
    }

    /// Mutable reference to nodes of the list
    pub fn nodes_mut(&mut self) -> &mut NodeList<A> {
        &mut self.nodes
    }

    /// A mutable reference to attributes
    pub fn attributes_mut(&mut self) -> &mut HashMap<&'static str, String> {
        &mut self.attributes
    }

    /// Real element of this Element
    pub fn get_real_element(&self) -> &RealElement {
        &self.real_element
    }

    /// Mutable references to classes and expr_classes
    pub fn classes_expr_classes_mut(
        &mut self,
    ) -> (&mut HashMap<&'static str, bool>, &mut HashMap<String, bool>) {
        (&mut self.classes, &mut self.expr_classes)
    }

    /// Attach the real element to the given parent
    pub fn append_to(&self, parent: &RealElement) {
        let parent: &real_dom::Node = parent.as_ref();
        let rs = parent.append_child(self.real_element.as_ref());
        ::interop::display_error(rs);
    }

    /// Attach the real element to the given parent before the `before` node
    pub fn insert_to(&self, parent: &RealElement, before: Option<&real_dom::Node>) {
        let parent: &real_dom::Node = parent.as_ref();
        let rs = parent.insert_before(self.real_element.as_ref(), before);
        ::interop::display_error(rs);
    }
}

impl<A: SimiApp, M: Default + SpecialAttributes> Element<A, M> {
    /// Add events directly to the real element
    // `Self=Element` just stores a handle to the closures to keep it working
    // They will be dropped when `Self` is dropped
    pub fn add_real_events(&mut self, main: &WeakMain<A>, events: Events<A>) {
        let Element {
            ref real_element,
            ref mut listeners,
            ..
        } = self;
        for mut e in events.into_iter() {
            e.add_listener_to(real_element, main.clone(), listeners);
        }
        listeners.shrink_to_fit();
    }

    /// Update the given events
    pub fn update_real_events(&mut self, main: &WeakMain<A>, events: Events<A>) {
        let Element {
            ref real_element,
            ref mut listeners,
            ..
        } = self;
        for mut e in events.into_iter() {
            e.update_listener_for(real_element, main.clone(), listeners);
        }
    }

    /// Apply attributes to real element for the first time
    pub fn set_attributes_to_real_element(&self) {
        let element = self.get_real_element();
        for (name, value) in self.attributes.iter() {
            ::interop::display_error(element.set_attribute(name, value));
        }
        let class_list = element.class_list();
        for (class, on) in self.classes.iter() {
            if *on {
                ::interop::display_error(class_list.add_1(class));
            }
        }
        for (class, on) in self.expr_classes.iter() {
            if *on {
                ::interop::display_error(class_list.add_1(class));
            }
        }

        self.more.set_attributes_to_real_element(&element);
    }

    /// Update class for self and real element
    pub fn update_class(&mut self, class: &str, on: bool, class_list: &real_dom::DomTokenList) {
        if let Some(c) = self.classes.get_mut(class) {
            if *c != on {
                *c = on;
                if on {
                    ::interop::display_error(class_list.add_1(class));
                } else {
                    ::interop::display_error(class_list.remove_1(class));
                }
            }
        } else {
            warn!("Simi bug: Encounter a new class? in update-phase?");
        }
    }

    /// Update class for self and real element. Apply for a class that its name from a variable
    pub fn update_expr_class(
        &mut self,
        class: &str,
        on: bool,
        class_list: &real_dom::DomTokenList,
    ) {
        if let Some(c) = self.expr_classes.get_mut(class) {
            if *c != on {
                *c = on;
                if on {
                    ::interop::display_error(class_list.add_1(&class));
                } else {
                    ::interop::display_error(class_list.remove_1(&class));
                }
            }
        } else {
            // What is the best way to handle the case in which the variable changed its value?
            //class_list.add(&class);
            //self.expr_classes.insert(class, on);
            warn!("A bug?: Encounter a new expr_class? in update-phase? Do you change your class name during update?");
        }
    }

    /// Update tracked attribute if changed
    pub fn update_attribute(&mut self, name: &str, value: String, real_element: &RealElement) {
        if let Some(a) = self.attributes.get_mut(name) {
            if *a != value {
                ::interop::display_error(real_element.set_attribute(name, &value));
                *a = value;
            }
        } else {
            warn!("A bug?: Simi does not expect a new attribute during update!");
        }
    }

    // Remove real node from real dom and return the next sibling
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &real_dom::Node,
    ) -> Option<RealElement> {
        use wasm_bindgen::JsCast;
        let next_sibling: Option<RealElement> = {
            let node: &real_dom::Node = self.real_element.as_ref();
            match node.next_sibling() {
                None => None,
                Some(next) => Some(next.unchecked_into()),
            }
        };
        ::interop::display_error(real_parent.remove_child(self.real_element.as_ref()));
        next_sibling
    }

    // Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &real_dom::Node) {
        ::interop::display_error(real_parent.remove_child(self.real_element.as_ref()));
    }
}
