use super::NodeList;
use app::SimiApp;
use interop::real_dom::{self, RealElement};

/// An active arm of if-else or match
pub struct Arm<A: SimiApp> {
    // Some(index) of the active arm
    // or:
    // None if it is an `if {} else if {}` without a final `else` and no `if-condition` is true
    index: Option<usize>,
    // Nodes of current active arm
    nodes: NodeList<A>,
}

#[allow(clippy::new_without_default)]
impl<A: SimiApp> Arm<A> {
    /// New empty arm
    pub fn new() -> Self {
        Self {
            index: None,
            nodes: NodeList::new(),
        }
    }

    /// Check if the current value of index is the same as expected_index or not
    pub fn is_same_index(&self, expected_index: Option<usize>) -> bool {
        match (self.index, expected_index) {
            (Some(ci), Some(ei)) => ci == ei,
            (None, None) => true,
            _ => false,
        }
    }

    /// Update the index
    pub fn set_index(&mut self, index: Option<usize>) {
        self.index = index;
    }

    /// A mutable reference to the node list
    pub fn nodes_mut(&mut self) -> &mut NodeList<A> {
        &mut self.nodes
    }

    /// Remove all simi nodes, real nodes and return the real node after the last real node of this arm
    pub fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &real_dom::Node,
    ) -> Option<RealElement> {
        self.nodes.remove_and_get_next_sibling(real_parent)
    }

    /// Remove real nodes from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &real_dom::Node) {
        self.nodes.remove_real_node(real_parent)
    }
}
