use super::{
    Arm, Component, Element, Input, NoSpecial, NodeList, Select, SimiCreatedSubApp, SubDom, Text,
    TextArea, WeakComponentChild,
};
use app::SimiApp;
use interop::real_dom::{self, RealElement};

/// A simi virtual Node
pub enum Node<A: SimiApp> {
    /// A node that will be rendered as a Text node in real dom
    Text(Text),
    /// A node that will be rendered as an Html Element
    Element(Element<A, NoSpecial>),
    /// A node that will be rendered as an Html Select element
    Select(Element<A, Select>),
    /// A node that will be rendered as an Html Input element
    Input(Element<A, Input>),
    /// A node that will be rendered as an Html TextArea element
    TextArea(Element<A, TextArea>),
    /// A simi component
    Component(Component<A>),
    /// A child of a simi component
    ComponentChild(WeakComponentChild<A>),
    /// A simi context-component or a simi context-sub-app
    SubDom(SubDom),
    /// A sub app the created by simi
    /// A sub app that created by user and stored in user app context is in SubDom
    SimiCreatedSubApp(SimiCreatedSubApp),
    /// An arm of if/else or match
    Arm(Arm<A>),
    /// None keyed items of a for loop
    NoneKeyedFor(NodeList<A>),
}

impl<A: SimiApp> Node<A> {
    /// Update text of current node
    pub fn update_text(&mut self, text: String) {
        match self {
            Node::Text(ref mut item) => item.update_text(text),
            _ => warn!("`update_text can be onlly called on a Node::Text"),
        }
    }

    /// If it is a ComponentChild, set_need_rerender for it
    pub fn set_need_rerender(&self, value: bool) {
        match self {
            Node::ComponentChild(child) => match child.upgrade() {
                Some(child) => child.borrow_mut().set_need_rerender(value),
                None => error!("Unable to set need_rerender for a child component"),
            },
            _ => {
                error!(
                    "`Simi` bug: set_need_rerender can be only called on a Node::ComponentChild",
                );
            }
        }
    }

    // Remove real node from real dom and return the next sibling
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &real_dom::Node,
    ) -> Option<RealElement> {
        match self {
            Node::Text(item) => item.remove_and_get_next_sibling(real_parent),
            Node::Element(item) => item.remove_and_get_next_sibling(real_parent),
            Node::Select(item) => item.remove_and_get_next_sibling(real_parent),
            Node::Input(item) => item.remove_and_get_next_sibling(real_parent),
            Node::TextArea(item) => item.remove_and_get_next_sibling(real_parent),
            Node::Component(item) => item.remove_and_get_next_sibling(real_parent),
            Node::ComponentChild(item) => match item.upgrade() {
                Some(child_component) => match child_component.try_borrow_mut() {
                    Ok(mut component) => component.remove_and_get_next_sibling(real_parent),
                    Err(e) => {
                        error!("{}", e);
                        None
                    }
                },
                None => {
                    error!("Unable to remove and get next sibling from a child component");
                    None
                }
            },
            Node::SubDom(_) | Node::SimiCreatedSubApp(_) => {
                // Currently, a component or sub_app always require to be inside a container element
                // We will also never encounter this type of node here
                warn!(
                    "This will never be encountered: remove_and_get_next_sibling for a SubDom/SimiCreatedSubApp",
                );
                None
            }
            Node::Arm(ref mut item) => item.remove_and_get_next_sibling(real_parent),
            Node::NoneKeyedFor(ref mut item) => item.remove_and_get_next_sibling(real_parent),
        }
    }

    // Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &real_dom::Node) {
        match self {
            Node::Text(item) => item.remove_real_node(real_parent),
            Node::Element(item) => item.remove_real_node(real_parent),
            Node::Select(item) => item.remove_real_node(real_parent),
            Node::Input(item) => item.remove_real_node(real_parent),
            Node::TextArea(item) => item.remove_real_node(real_parent),
            Node::Component(item) => item.remove_real_node(real_parent),
            Node::ComponentChild(item) => match item.upgrade() {
                Some(child_component) => match child_component.try_borrow_mut() {
                    Ok(mut child_component) => child_component.remove_real_node(real_parent),
                    Err(e) => error!("{}", e),
                },
                None => {
                    error!("Unable to remove real node from a child component");
                }
            },
            Node::SubDom(_) | Node::SimiCreatedSubApp(_) => {
                // Currently, a component or sub_app always require to be inside a container element
                // We will also never encounter this type of node here
                warn!(
                    "This will never be encountered: remove_real_node for a SubDom/SimiCreatedSubApp",
                );
            }
            Node::Arm(ref mut item) => item.remove_real_node(real_parent),
            Node::NoneKeyedFor(ref mut item) => item.remove_real_node(real_parent),
        }
    }
}

impl<A: SimiApp> From<Text> for Node<A> {
    fn from(item: Text) -> Self {
        Node::Text(item)
    }
}

impl<A: SimiApp> From<Element<A, NoSpecial>> for Node<A> {
    fn from(item: Element<A, NoSpecial>) -> Self {
        Node::Element(item)
    }
}

impl<A: SimiApp> From<Element<A, Select>> for Node<A> {
    fn from(item: Element<A, Select>) -> Self {
        Node::Select(item)
    }
}

impl<A: SimiApp> From<Element<A, Input>> for Node<A> {
    fn from(item: Element<A, Input>) -> Self {
        Node::Input(item)
    }
}

impl<A: SimiApp> From<Element<A, TextArea>> for Node<A> {
    fn from(item: Element<A, TextArea>) -> Self {
        Node::TextArea(item)
    }
}

impl<A: SimiApp> From<Component<A>> for Node<A> {
    fn from(item: Component<A>) -> Self {
        Node::Component(item)
    }
}

impl<A: SimiApp> From<WeakComponentChild<A>> for Node<A> {
    fn from(item: WeakComponentChild<A>) -> Self {
        Node::ComponentChild(item)
    }
}

impl<A: SimiApp> From<SubDom> for Node<A> {
    fn from(item: SubDom) -> Self {
        Node::SubDom(item)
    }
}

impl<A: SimiApp> From<Arm<A>> for Node<A> {
    fn from(item: Arm<A>) -> Self {
        Node::Arm(item)
    }
}

impl<A: SimiApp> From<SimiCreatedSubApp> for Node<A> {
    fn from(item: SimiCreatedSubApp) -> Self {
        Node::SimiCreatedSubApp(item)
    }
}
