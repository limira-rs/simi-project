use interop::real_dom::RealElement;
use wasm_bindgen::JsCast;

/// Define how to init a special element
pub trait SpecialAttributes {
    /// Executed when the element is create
    fn set_attributes_to_real_element(&self, real_element: &RealElement);
}

/// For elements that have no special attributes
#[derive(Default)]
pub struct NoSpecial;

impl SpecialAttributes for NoSpecial {
    /// Nothing here
    fn set_attributes_to_real_element(&self, _real_element: &RealElement) {
        //
    }
}

/// Special attributes/values for HtmlSelectElement
pub struct Select {
    /// Select's value
    pub value: Option<String>,
    /// Select's selected index
    pub index: Option<usize>,
}

impl Default for Select {
    fn default() -> Self {
        Self {
            value: None,
            index: None,
        }
    }
}

impl SpecialAttributes for Select {
    /// Attach attribute in creating-phase
    fn set_attributes_to_real_element(&self, real_element: &RealElement) {
        let real_element: &::interop::real_dom::HtmlSelectElement = real_element.unchecked_ref();
        if let Some(ref value) = self.value {
            real_element.set_value(value);
        } else if let Some(index) = self.index {
            real_element.set_selected_index(index as i32);
        } else {
            real_element.set_selected_index(-1);
        }
    }
}

impl Select {
    /// Update select's value if changed
    pub fn update_value(&mut self, new_value: &Option<String>, real_element: &RealElement) {
        if self.value == *new_value {
            return;
        }
        self.value = new_value.clone();
        match self.value {
            Some(ref value) => {
                let real_element: &::interop::real_dom::HtmlSelectElement =
                    real_element.unchecked_ref();
                real_element.set_value(&value);
            }
            None => {
                let real_element: &::interop::real_dom::HtmlSelectElement =
                    real_element.unchecked_ref();
                real_element.set_selected_index(-1);
            }
        }
    }

    /// Update select's index if changed
    pub fn update_index(&mut self, new_index: &Option<usize>, real_element: &RealElement) {
        if self.index == *new_index {
            return;
        }
        self.index = *new_index;
        match self.index {
            Some(ref index) => {
                let real_element: &::interop::real_dom::HtmlSelectElement =
                    real_element.unchecked_ref();
                real_element.set_selected_index(*index as i32);
            }
            None => {
                let real_element: &::interop::real_dom::HtmlSelectElement =
                    real_element.unchecked_ref();
                real_element.set_selected_index(-1);
            }
        }
    }
}

/// Special attributes/values for HtmlInputElement
pub struct Input {
    /// Input's value
    pub value: Option<String>,
    /// Checkbox/Radio's checked
    pub checked: Option<bool>,
}

impl Default for Input {
    fn default() -> Self {
        Self {
            value: None,
            checked: None,
        }
    }
}

impl SpecialAttributes for Input {
    /// Attach attribute in creating-phase
    fn set_attributes_to_real_element(&self, real_element: &RealElement) {
        let real_element: &::interop::real_dom::HtmlInputElement = real_element.unchecked_ref();
        if let Some(ref value) = self.value {
            real_element.set_value(value);
        }
        if let Some(value) = self.checked {
            real_element.set_checked(value);
        }
    }
}

impl Input {
    /// Update input element's value if changed
    pub fn update_value(&mut self, new_value: &str, real_element: &RealElement) {
        match self.value {
            Some(ref mut old_value) => {
                if old_value != new_value {
                    let real_element: &::interop::real_dom::HtmlInputElement =
                        real_element.unchecked_ref();
                    real_element.set_value(new_value);
                    *old_value = new_value.to_string();
                }
            }
            None => {
                let real_element: &::interop::real_dom::HtmlInputElement =
                    real_element.unchecked_ref();
                real_element.set_value(&new_value);
                self.value = Some(new_value.to_string());
            }
        }
    }

    /// Update input checked property
    pub fn update_checked(&mut self, new_checked: bool, real_element: &RealElement) {
        match self.checked {
            Some(ref mut checked) => {
                if *checked != new_checked {
                    *checked = new_checked;
                    let real_element: &::interop::real_dom::HtmlInputElement =
                        real_element.unchecked_ref();
                    real_element.set_checked(new_checked);
                }
            }
            None => {
                let real_element: &::interop::real_dom::HtmlInputElement =
                    real_element.unchecked_ref();
                real_element.set_checked(new_checked);
                self.checked = Some(new_checked);
            }
        }
    }
}

// FIXME: How to avoid code repeate Input.value/TextArea.value??

/// Special attributes/values for HtmlTextAreaElement
pub struct TextArea {
    /// TextArea's value
    pub value: Option<String>,
}

impl Default for TextArea {
    fn default() -> Self {
        Self { value: None }
    }
}

impl SpecialAttributes for TextArea {
    /// Attach attribute in creating-phase
    fn set_attributes_to_real_element(&self, real_element: &RealElement) {
        let real_element: &::interop::real_dom::HtmlTextAreaElement = real_element.unchecked_ref();
        if let Some(ref value) = self.value {
            real_element.set_value(value);
        }
    }
}

impl TextArea {
    /// Update textarea element's value if changed
    pub fn update_value(&mut self, new_value: &str, real_element: &RealElement) {
        match self.value {
            Some(ref mut old_value) => {
                if old_value != new_value {
                    let real_element: &::interop::real_dom::HtmlTextAreaElement =
                        real_element.unchecked_ref();
                    real_element.set_value(new_value);
                    *old_value = new_value.to_string();
                }
            }
            None => {
                let real_element: &::interop::real_dom::HtmlTextAreaElement =
                    real_element.unchecked_ref();
                real_element.set_value(new_value);
                self.value = Some(new_value.to_string());
            }
        }
    }
}
