use std::any::{Any, TypeId};

use app::{RcMain, SimiApp, SimiMain};
use interop::real_dom::RealElement;

fn init_a_simi_app<B: SimiApp + 'static>(real_parent: &RealElement) -> RcMain<B> {
    let rc_app: RcMain<B> = SimiMain::new_rc_main();
    {
        let wm = ::std::rc::Rc::downgrade(&rc_app);
        let mut app = rc_app.borrow_mut();
        app.init_app(wm);
        app.start_in(real_parent.clone());
    }
    rc_app
}

/// For sub app that user manage it in user app's context, see sub_dom.rs
///
/// This is a sub app that is created and managed by simi.
/// Currently, this type of sub app dont have any communication with the main app
pub struct SimiCreatedSubApp {
    user_app_type_id: TypeId,
    _app_handle: Box<Any>,
}

impl SimiCreatedSubApp {
    /// Create a new SimiCreatedSubApp for app type B
    pub fn new<B: SimiApp + 'static>(real_parent: &RealElement) -> Self {
        let id = TypeId::of::<B>();
        let rc_app = init_a_simi_app::<B>(real_parent);
        Self {
            user_app_type_id: id,
            _app_handle: Box::new(rc_app),
        }
    }

    /// Use for matching OneOfSimiCreatedSubApp to check if
    /// it is the same app or not
    pub fn is_app_type<B: SimiApp + 'static>(&self) -> bool {
        self.user_app_type_id == TypeId::of::<B>()
    }

    /// Create a new app in this node.
    pub fn new_app<B: SimiApp + 'static>(&mut self, real_parent: &RealElement) {
        self.user_app_type_id = TypeId::of::<B>();
        self._app_handle = Box::new(init_a_simi_app::<B>(real_parent));
    }
}
