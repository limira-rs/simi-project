//! This module implements a virtual dom for simi app (inspired by Yew's virtual dom).
//! Simi does not create a new virtual dom for each render. Instead it try to update
//! the current virtual dom. Simi virtual dom just provides a very basic funtionality.
//! Most of the `magic` is done by procedural macros: application! and component!.

use app::SimiApp;
use std::cell::RefCell;
use std::rc::{Rc, Weak};

mod arm;
mod component;
mod element;
mod node;
mod node_list;
mod simi_created_sub_app;
mod special_elements;
mod sub_dom;
mod text;

pub use self::arm::*;
pub use self::component::*;
pub use self::element::*;
pub use self::node::*;
pub use self::node_list::*;
pub use self::simi_created_sub_app::*;
pub use self::special_elements::*;
pub use self::sub_dom::*;
pub use self::text::*;

type RcComponentChild<A> = Rc<RefCell<Component<A>>>;
/// A child of a component
pub type WeakComponentChild<A> = Weak<RefCell<Component<A>>>;

/// Trait to create sub items.  This is a workaround for situation where the compiler is unable
/// to infer type A.
pub trait SimiCreator<A: SimiApp> {
    /// Create a Simi Text node
    fn create_text<T: ToString>(&self, item: T) -> Text {
        Text::new(item.to_string())
    }

    /// Create a Simi Element node
    fn create_element<M: Default + SpecialAttributes>(
        &self,
        tag: &'static str,
        etag: ::html_tags::HtmlTag,
    ) -> Element<A, M> {
        Element::new(tag, etag)
    }

    /// Create a Simi Component node
    fn create_component(&self, child_count: usize) -> Component<A> {
        Component::new(child_count)
    }

    /// Create a Simi sub dom node
    fn create_sub_dom(&self, sub_dom_type: SubDomType) -> SubDom {
        SubDom::new(sub_dom_type)
    }

    /// Create an Simi Arm node for an active if or match arm
    fn create_arm(&self) -> Arm<A> {
        Arm::new()
    }

    /// Create an Simi NodeList, tobe use for Node such as Node::NoneKeyedFor
    fn create_node_list(&self) -> NodeList<A> {
        NodeList::new()
    }
}
