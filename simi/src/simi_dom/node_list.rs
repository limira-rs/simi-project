use super::{Node, SimiCreator};
use app::SimiApp;
use interop::real_dom::{self, RealElement};

/// A list of Nodes
pub struct NodeList<A: SimiApp> {
    nodes: Vec<Node<A>>,
}

impl<A: SimiApp> SimiCreator<A> for NodeList<A> {}

#[allow(clippy::new_without_default_derive)]
impl<A: SimiApp> NodeList<A> {
    /// Create an empty list
    pub fn new() -> Self {
        Self { nodes: Vec::new() }
    }

    /// Make sure that the list is able to contains a `capacity` number of items
    pub fn new_capacity(&mut self, capacity: usize) {
        if capacity > self.nodes.len() {
            let additional = capacity - self.nodes.len();
            self.nodes.reserve(additional);
        }
    }

    /// Number of nodes in the list
    pub fn count(&self) -> usize {
        self.nodes.len()
    }

    /// Mutable reference to node at the given index
    pub fn get_mut(&mut self, index: usize) -> Option<&mut Node<A>> {
        self.nodes.get_mut(index)
    }

    /// Mutable reference to nodes
    pub fn nodes_mut(&mut self) -> &mut Vec<Node<A>> {
        &mut self.nodes
    }

    /// Append a simi node to the end of the list
    pub fn append_child(&mut self, node: Node<A>) {
        self.nodes.push(node);
    }

    /// Remove real nodes from real dom and return the next real sibling of the last real node of this list
    pub(crate) fn remove_and_get_next_sibling(
        &mut self,
        real_parent: &real_dom::Node,
    ) -> Option<RealElement> {
        let mut last = self.nodes.pop().expect("Simi bug: Arm must not be empty");
        let next_sibling = last.remove_and_get_next_sibling(real_parent);
        self.remove_real_node(real_parent);
        next_sibling
    }

    /// Remove real node from real dom
    pub(crate) fn remove_real_node(&mut self, real_parent: &real_dom::Node) {
        while let Some(mut node) = self.nodes.pop() {
            node.remove_real_node(real_parent);
        }
    }

    /// Remove all nodes after the given size from both real and virtual dom
    pub fn truncate(&mut self, size_after_removal: usize, real_parent: &real_dom::Node) {
        while self.nodes.len() > size_after_removal {
            let mut node = self.nodes.pop().expect("No more node to remove");
            node.remove_real_node(real_parent);
        }
    }
}
