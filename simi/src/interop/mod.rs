//! This module supports interop with things provided by browsers
#[macro_use]
pub mod closure;

// TODO: Remove these two funtions, use the functions in ::error.rs instead

/// (Will be removed, use the function in error instead) Display error return by web-sys
pub fn display_error<R>(rs: Result<R, ::wasm_bindgen::JsValue>) {
    match rs {
        Ok(_) => {}
        Err(e) => error!(
            "{}",
            e.as_string()
                .unwrap_or_else(|| "(No error message?)".to_string())
        ),
    }
}

/// (Will be removed, use the function in error instead) Log error using macro from `log` crate
pub fn log_error(message: &str) {
    error!("{}", message);
}

/// Reexport web_sys and have some aliases
pub mod real_dom {
    pub use web_sys::*;

    /// Alias to interop::real_dom::Element
    pub type RealElement = Element;
    /// Alias to interop::real_dom::Node
    pub type RealNode = Node;

    /// Document
    pub fn document() -> Document {
        window()
            .expect("Must have a window")
            .document()
            .expect("Must have a document")
    }
}
