//! Create closures that send messages back to simi-app from JavaScript
//! The result closures are intented to pass to JavaScript side, this enable JavaScript
//! code to be able to send messages back to your simi-app.

use app::{SimiApp, WeakMain};
use wasm_bindgen::closure::{Closure, WasmClosure};

#[cfg(feature = "closure_serde")]
use serde::Deserialize;

/// Create a `wasm_bindgen::closure::Closure` for a handler that requires no argument.
///
/// # Example
///
/// This example will create a callback that will send `Msg::TimeOut` to
/// your simi-app. The callback closure then be passed to window.setInterval().
/// You must either store the closure somewhere or you must call its `forget` method.
///
/// Your app's Message type:
/// ```rust
/// use wasm_bindgen::JsCast;
/// enum Msg {
///     TimeOut
/// }
/// ```
/// In your app:
/// ```rust
/// impl SimiApp for AppState {
///     type Message = Msg;
///     type Context = ();
///     fn init(main: WeakMain<Self>) -> (Self, Self::Context) {
///         let callback_from_js = simi::interop::closure::no_arg(main, || Msg::TimeOut);
///         web_sys::window()
///             .unwrap()
///             .set_interval_with_callback_and_timeout_and_arguments_0(
///                 callback_from_js.as_ref().unchecked_ref(),
///                 1000
///             ).unwrap();
///         callback_from_js.forget();
///     }
/// }
/// ```
pub fn no_arg<A, H>(main: WeakMain<A>, handler: H) -> Closure<Fn()>
where
    A: SimiApp + 'static,
    H: Fn() -> A::Message + 'static,
{
    let handler = move || match main.upgrade() {
        None => ::error::log_upgrade_main_failed(),
        Some(main) => main.borrow_mut().update(handler()),
    };
    Closure::wrap(Box::new(handler) as Box<Fn()>)
}

/// Create a `wasm_bindgen::closure::Closure` for a handler that requires an argument.
///
/// # Example
///
/// ```rust
/// use wasm_bindgen::JsValue;
/// enum Msg {
///     FromJs(JsValue)
/// }
/// let callback_from_js = simi::interop::closure::no_arg(main, Msg::FromJs);
/// ```
pub fn with_arg<A, T, H>(main: WeakMain<A>, handler: H) -> Closure<Fn(T)>
where
    A: SimiApp + 'static,
    H: Fn(T) -> A::Message + 'static,
    Fn(T) + 'static: WasmClosure,
{
    let handler = move |value: T| match main.upgrade() {
        None => ::error::log_upgrade_main_failed(),
        Some(main) => {
            info!("Try to borrow the app");
            main.borrow_mut().update(handler(value));
            info!("Message sent");
        }
    };
    Closure::wrap(Box::new(handler) as Box<Fn(T)>)
}

/// Create a `wasm_bindgen::closure::Closure` for a handler that requires an argument.
/// The argument received from JavaScript is intepreted as a wasm_bindgen::JsValue. Then will
/// be deserialize into the Rust type T using serde
#[cfg(feature = "closure_serde")]
pub fn with_serde_arg<A, T, H>(
    main: WeakMain<A>,
    handler: H,
) -> Closure<Fn(::wasm_bindgen::JsValue)>
where
    A: SimiApp + 'static,
    H: Fn(T) -> A::Message + 'static,
    for<'a> T: Deserialize<'a> + 'static,
{
    let handler = move |value: ::wasm_bindgen::JsValue| match main.upgrade() {
        None => ::error::log_upgrade_main_failed(),
        Some(main) => {
            let value: Result<T, ::serde_json::Error> = value.into_serde();
            match value {
                Ok(value) => main.borrow_mut().update(handler(value)),
                Err(e) => error!("Deserialize error: {}", e),
            };
        }
    };
    Closure::wrap(Box::new(handler) as Box<Fn(::wasm_bindgen::JsValue)>)
}

/// Create a `wasm_bindgen::closure::Closure` that will call your app's context method.
///
/// # Example
///
/// Your app's Context type:
/// ```rust
/// #[macro_use]
/// extern crate simi;
///
/// struct AppContext;
/// impl AppContext {
///     fn do_something_with_app_context() {
///         // Some code here
///     }
/// }
/// ```
/// Create the context callback
/// ```rust
/// let context_callback = context_callback!(main, do_something_with_app_context);
/// ```
#[macro_export]
macro_rules! context_callback {
    ($weak_main:expr, $context_method_name:ident) => {{
        let main = $weak_main.clone();
        let handler = move || match main.upgrade() {
            None => $crate::error::log_upgrade_main_failed(),
            Some(mut main) => match main.try_borrow_mut() {
                Err(e) => $crate::error::log_error_message(&e.to_string()),
                Ok(mut main) => main.get_app_context_mut().$context_method_name(),
            },
        };
        wasm_bindgen::closure::Closure::wrap(Box::new(handler) as Box<Fn()>)
    }};
}
